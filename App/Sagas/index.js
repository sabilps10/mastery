import { takeLatest, all } from "redux-saga/effects";
import API from "../Services/Api";
import FixtureAPI from "../Services/FixtureApi";
import DebugConfig from "../Config/DebugConfig";

/* ------------- Types ------------- */

if (__DEV__) console.tron.log("masuk Index Sagas");

import { StartupTypes } from "../Redux/StartupRedux";
import { LoginTypes } from "../Redux/LoginRedux";
import { ForgetPasswordTypes } from "../Redux/ForgetPasswordRedux";
import { RegisterTypes } from "../Redux/RegisterRedux";
import { HomeTypes } from "../Redux/HomeRedux";
import { NewHomeTypes } from "../Redux/NewHomeRedux";
import { QuestTypes } from "../Redux/QuestRedux";
import { CoachingProgrammeTypes } from "../Redux/CoachingProgrammeRedux";
import { ShowProfileTypes } from "../Redux/ShowProfileRedux";
import { EditProfileTypes } from "../Redux/EditProfileRedux";
import { PostsTypes } from "../Redux/PostsRedux";
import { ChatsTypes } from "../Redux/ChatsRedux";
import { SendPostsTypes } from "../Redux/SendPostsRedux";
import { SendPhotoTypes } from "../Redux/SendPhotoRedux";
import { ProfilePhotoTypes } from "../Redux/ProfilePhotoRedux";
import { SendProofTypes } from "../Redux/SendProofRedux";
import { SendDocTypes } from "../Redux/SendDocRedux";
import { SendAudioTypes } from "../Redux/SendAudioRedux";
import { EvidenceTypes } from "../Redux/EvidenceRedux";
import { AppreciationTypes } from "../Redux/AppreciationRedux";
import { RedeemTypes } from "../Redux/RedeemRedux";
import { RewardTypes } from "../Redux/RewardRedux";
import { NewsletterTypes } from "../Redux/NewsletterRedux";
import { CloseQuestTypes } from "../Redux/CloseQuestRedux";
// import { RegisterTokenTypes } from "../Redux/RegisterTokenRedux";
import { UpdateFCMTypes } from "../Redux/UpdateFCMRedux";
//import { BugTypes } from "../Redux/BugRedux";

/* ------------- Sagas ------------- */

import { startup } from "./StartupSagas";
import { doLogin } from "./LoginSagas";
import { doForgetPassword } from "./ForgetPasswordSagas";
import { doRegister } from "./RegisterSagas";
import { getHome } from "./HomeSagas";
import { getNewHome } from "./NewHomeSagas";
import { getQuest, pickQuest } from "./QuestSagas";
import { getQuestDetail } from "./QuestSagas";
import { getCoach, pickCoach, subscribeCoach } from "./CoachingProgrammeSagas";
import { getCoachDetail } from "./CoachingProgrammeSagas";
import { getUpdateFCM } from "./UpdateFCMSagas";
import { getProfileDetail } from "./ShowProfileSagas";
import { getEditProfile } from "./EditProfileSagas";
import { getPosts } from "./PostsSagas";
import { getChats } from "./ChatsSagas";
import { getSendPosts } from "./SendPostsSagas";
import { getSendPhoto } from "./SendPhotoSagas";
import { getProfilePhoto } from "./ProfilePhotoSagas";
import { getSendProof } from "./SendProofSagas";
import { getSendDoc } from "./SendDocSagas";
import { getSendAudio } from "./SendAudioSagas";
import { getEvidence } from "./EvidenceSagas";
import { getAppreciation } from "./AppreciationSagas";
import { getNewsletter } from "./NewsletterSagas";
import { getReward, pickReward } from "./RewardSagas";
import { getRedeem } from "./RedeemSagas";
import { getCloseQuest } from "./CloseQuestSagas";


/* ------------- API ------------- */

// The API we use is only used from Sagas, so we create it here and pass along
// to the sagas which need it.
const api = DebugConfig.useFixtures ? FixtureAPI : API.create();

/* ------------- Connect Types To Sagas ------------- */

export default function* root() {
  yield all([
    // some sagas only receive an action
    // takeLatest(StartupTypes.STARTUP, startup),
    takeLatest(LoginTypes.LOGIN_REQUEST, doLogin, api),
    takeLatest(ForgetPasswordTypes.FORGETPASSWORD_REQUEST, doForgetPassword, api),
    takeLatest(RegisterTypes.REGISTER_REQUEST, doRegister, api),
    takeLatest(HomeTypes.HOME_REQUEST, getHome, api),
    takeLatest(NewHomeTypes.NEW_HOME_REQUEST, getNewHome, api),
    takeLatest(CoachingProgrammeTypes.COACHING_PROGRAMME_REQUEST, getCoach, api),
    takeLatest(CoachingProgrammeTypes.COACHING_PROGRAMME_DETAIL_REQUEST, getCoachDetail, api),
    takeLatest(CoachingProgrammeTypes.PICK_COACHING_PROGRAMME, pickCoach, api),
    takeLatest(CoachingProgrammeTypes.SUBSCRIBE_COACHING_PROGRAMME, subscribeCoach, api),
    takeLatest(QuestTypes.QUEST_REQUEST, getQuest, api),
    takeLatest(QuestTypes.QUEST_DETAIL_REQUEST, getQuestDetail, api),
    takeLatest(QuestTypes.PICK_QUEST, pickQuest, api),
    takeLatest(ShowProfileTypes.PROFILE_DETAIL_REQUEST, getProfileDetail, api),
    takeLatest(EditProfileTypes.EDIT_PROFILE_REQUEST, getEditProfile, api),
    takeLatest(PostsTypes.POSTS_REQUEST, getPosts, api),
    takeLatest(ChatsTypes.CHATS_REQUEST, getChats, api),
    takeLatest(SendPostsTypes.SEND_POSTS_REQUEST, getSendPosts, api),
    takeLatest(SendPhotoTypes.SEND_PHOTO_REQUEST, getSendPhoto, api),
    takeLatest(ProfilePhotoTypes.PROFILE_PHOTO_REQUEST, getProfilePhoto, api),
    takeLatest(SendProofTypes.SEND_PROOF_REQUEST, getSendProof, api),
    takeLatest(SendDocTypes.SEND_DOC_REQUEST, getSendDoc, api),
    takeLatest(UpdateFCMTypes.UPDATE_FCM_REQUEST, getUpdateFCM, api),
    takeLatest(SendAudioTypes.SEND_AUDIO_REQUEST, getSendAudio, api),
    takeLatest(EvidenceTypes.EVIDENCE_REQUEST, getEvidence, api),
    takeLatest(AppreciationTypes.APPRECIATION_REQUEST, getAppreciation, api),
    takeLatest(RewardTypes.REWARD_REQUEST, getReward, api),
    takeLatest(RewardTypes.PICK_REWARD, pickReward, api),
    takeLatest(RedeemTypes.REDEEM_REQUEST, getRedeem, api),
    takeLatest(NewsletterTypes.NEWSLETTER_REQUEST, getNewsletter, api),
    takeLatest(CloseQuestTypes.CLOSE_QUEST_REQUEST, getCloseQuest, api),
  ]);
}
