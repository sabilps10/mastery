/* ***********************************************************
* A short word on how to use this automagically generated file.
* We're often asked in the ignite gitter channel how to connect
* to a to a third party api, so we thought we'd demonstrate - but
* you should know you can use sagas for other flow control too.
*
* Other points:
*  - You'll need to add this saga to sagas/index.js
*  - This template uses the api declared in sagas/index.js, so
*    you'll need to define a constant in that file.
*************************************************************/

import { call, put, select } from 'redux-saga/effects'
import EvidenceActions from '../Redux/EvidenceRedux'
import {LoginSelectors} from '../Redux/LoginRedux'

// import { EvidenceSelectors } from '../Redux/EvidenceRedux'

export function * getEvidence (api, action) {
  const { data } = action
  // get current data from Store
  // const currentData = yield select(EvidenceSelectors.getData)
  // make the call to the api
  const access_token = yield select(LoginSelectors.getAccessToken);
  const response = yield call(api.getevidence, {data, access_token})

  // success?
  if (response.ok) {
    // You might need to change the response here - do this with a 'transform',
    // located in ../Transforms/. Otherwise, just pass the data back from the api.
    yield put(EvidenceActions.evidenceSuccess(response.data))
  } else {
    yield put(EvidenceActions.evidenceFailure())
  }
}
