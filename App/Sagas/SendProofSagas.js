/* ***********************************************************
* A short word on how to use this automagically generated file.
* We're often asked in the ignite gitter channel how to connect
* to a to a third party api, so we thought we'd demonstrate - but
* you should know you can use sagas for other flow control too.
*
* Other points:
*  - You'll need to add this saga to sagas/index.js
*  - This template uses the api declared in sagas/index.js, so
*    you'll need to define a constant in that file.
*************************************************************/

import { call, put, select } from 'redux-saga/effects'
import SendProofActions from '../Redux/SendProofRedux'
import {LoginSelectors} from '../Redux/LoginRedux'

// import { SendProofSelectors } from '../Redux/SendProofRedux'

export function * getSendProof (api, action) {
  const { data } = action
  // get current data from Store
  // const currentData = yield select(SendProofSelectors.getData)
  // make the call to the api
  const access_token = yield select(LoginSelectors.getAccessToken);
  
  const response = yield call(api.sendProof, {data, access_token})
  if(__DEV__) console.tron.log(response);
  // success?
  if (response.ok) {
    // You might need to change the response here - do this with a 'transform',
    // located in ../Transforms/. Otherwise, just pass the data back from the api.
    yield put(SendProofActions.sendProofSuccess(response.data))
  } else {
    yield put(SendProofActions.sendProofFailure())
  }
}
