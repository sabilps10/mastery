// /* ***********************************************************
//  * A short word on how to use this automagically generated file.
//  * We're often asked in the ignite gitter channel how to connect
//  * to a to a third party api, so we thought we'd demonstrate - but
//  * you should know you can use sagas for other flow control too.
//  *
//  * Other points:
//  *  - You'll need to add this saga to sagas/index.js
//  *  - This template uses the api declared in sagas/index.js, so
//  *    you'll need to define a constant in that file.
//  *************************************************************/

import { call, put, select } from "redux-saga/effects";
 import CoachingProgrammeActions from "../Redux/CoachingProgrammeRedux";
// // import { QuestSelectors } from '../Redux/QuestRedux'
 import {LoginSelectors} from '../Redux/LoginRedux'

 export function* getCoach(api, action) {
   const { data } = action;
  
//   // get current data from Store
   const access_token = yield select(LoginSelectors.getAccessToken);
//   // make the call to the api
   const response = yield call(api.getCoachs, { data, access_token });
   if(__DEV__) console.tron.log(response);
  
//   // success?
   if (response.ok) {
//     // You might need to change the response here - do this with a 'transform',
//     // located in ../Transforms/. Otherwise, just pass the data back from the api.
    yield put(CoachingProgrammeActions.coachingProgrammeSuccess(response.data.coach_programme));
   } else {
     yield put(CoachingProgrammeActions.coachingProgrammeFailure());
   }
 }

export function* getCoachDetail(api, action) {
   const { id } = action;
  
//   // get current data from Store
   const access_token = yield select(LoginSelectors.getAccessToken);
//   // make the call to the api
   const response = yield call(api.getCoachDetail, { id, access_token });
 
//   // success?
   if (response.ok) {
//     // You might need to change the response here - do this with a 'transform',
//     // located in ../Transforms/. Otherwise, just pass the data back from the api.
     yield put(CoachingProgrammeActions.coachingProgrammeDetailSuccess(response.data.coach_programme, response.data.programme_subscribers));
   } else {
     yield put(CoachingProgrammeActions.coachingProgrammeFailure());
   }
 }

 export function* pickCoach(api, action) {
   const { id } = action;
  
   // get current data from Store
   const access_token = yield select(LoginSelectors.getAccessToken);
//   // make the call to the api
   const response = yield call(api.pickCoach, { id, access_token });
 
   // success?
   if (response.ok) {
     // You might need to change the response here - do this with a 'transform',
     // located in ../Transforms/. Otherwise, just pass the data back from the api.
     yield put(CoachingProgrammeActions.pickCoachingProgrammeSuccess(response.data));
  } else {
     yield put(CoachingProgrammeActions.pickCoachingProgrammeFailure());
   }
 }

 export function* subscribeCoach(api, action) {
   const { id } = action;
  
   // get current data from Store
   const access_token = yield select(LoginSelectors.getAccessToken);
//   // make the call to the api
   const response = yield call(api.subscribeCoach, { id, access_token });
 
   // success?
   if (response.ok) {
     // You might need to change the response here - do this with a 'transform',
     // located in ../Transforms/. Otherwise, just pass the data back from the api.
     yield put(CoachingProgrammeActions.subscribeCoachingProgrammeSuccess(response.data.payment_url));
  } else {
     yield put(CoachingProgrammeActions.subscribeCoachingProgrammeFailure());
   }
 }