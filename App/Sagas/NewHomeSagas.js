// /* ***********************************************************
//  * A short word on how to use this automagically generated file.
//  * We're often asked in the ignite gitter channel how to connect
//  * to a to a third party api, so we thought we'd demonstrate - but
//  * you should know you can use sagas for other flow control too.
//  *
//  * Other points:
//  *  - You'll need to add this saga to sagas/index.js
//  *  - This template uses the api declared in sagas/index.js, so
//  *    you'll need to define a constant in that file.
//  *************************************************************/

import { call, put, select } from "redux-saga/effects";
import NewHomeActions from "../Redux/NewHomeRedux";
// // import { QuestSelectors } from '../Redux/QuestRedux'
import { LoginSelectors } from '../Redux/LoginRedux'

export function* getNewHome(api, action) {
   const { data } = action;

   //   // get current data from Store
   const access_token = yield select(LoginSelectors.getAccessToken);
   //   // make the call to the api
   const response = yield call(api.getNewHome, { data, access_token });
   if (__DEV__) console.tron.log(response);

   //   // success?
   if (response.ok) {
      //     // You might need to change the response here - do this with a 'transform',
      //     // located in ../Transforms/. Otherwise, just pass the data back from the api.
      yield put(NewHomeActions.newHomeSuccess(response.data.quests, response.data.programmes, response.data.banners, response.data.profile));
   } else {
      yield put(NewHomeActions.newHomeFailure());
   }
}
