/* ***********************************************************
 * A short word on how to use this automagically generated file.
 * We're often asked in the ignite gitter channel how to connect
 * to a to a third party api, so we thought we'd demonstrate - but
 * you should know you can use sagas for other flow control too.
 *
 * Other points:
 *  - You'll need to add this saga to sagas/index.js
 *  - This template uses the api declared in sagas/index.js, so
 *    you'll need to define a constant in that file.
 *************************************************************/

import { call, put, select } from "redux-saga/effects";
import HomeActions from "../Redux/HomeRedux";
import { LoginSelectors } from "../Redux/LoginRedux";
//import Bugfender from '@bugfender/rn-bugfender';
import Secrets from "react-native-config";

//Bugfender.init(Secrets.BUGFENDER_APIKEY);

export function* getHome(api, action) {
  const { data } = action;
  // get current data from Store
  // const currentData = yield select(HomeSelectors.getData)
  // make the call to the api
  const access_token = yield select(LoginSelectors.getAccessToken);
  if(__DEV__) console.tron.log('at:', access_token);
  const response = yield call(api.getHome, {
    access_token
  });
  //if(__DEV__) console.tron.log(response);
  // success?
  if (response.ok) {
    //if(__DEV__) console.tron.log('OK',response.data)
    //Bugfender.d("home",response.problem);
    // You might need to change the response here - do this with a 'transform',
    // located in ../Transforms/. Otherwise, just pass the data back from the api.
    yield put(HomeActions.homeSuccess(response.data));
  } else {
    if(__DEV__) console.tron.log('home-error', response);
    //Bugfender.e("home",response.problem);
    response.originalError
    yield put(HomeActions.homeFailure(response.status));
  }
}
