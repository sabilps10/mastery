import React, { Component } from "react";
import { View, Text, Image, Dimensions, ActivityIndicator } from "react-native";
import { connect } from "react-redux";

import styles from "./Styles/ShowProfileScreenStyle";
import { Images } from '../Themes'

import TouchableOSResponsive from "../Components/MaterialUi/TouchableOsResponsive";
import HeaderMastery from "../Components/HeaderMastery";

import { ScrollView } from "react-native-gesture-handler";

//halaman profile screen akan menarik data profil dari API

class ProfileScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      confirm_popup: false,
      confirm_data: null,
      close_progress: false
    };
  }

  componentDidMount() {
    const { login } = this.props; 
    if(__DEV__) console.tron.log("quest info mounting : ", this.props.login.id);
  }
  
  componentDidUpdate(_props, _states) {
    const { login } = this.props; 

  }

  render() {
    const { login } = this.props; 
    
    //render view
    return (
      <View style={styles.rewardsContainer}>
      <HeaderMastery
          navigation={this.props.navigation}
          title={'EDIT PROFILE'}
      />

      <View style={styles.loginContainer}>

          <View style={styles.background} />
          {/* <Image style={styles.avatarImage}>{this.props.loggedInUser.profile_pic}</Image> /> */}
          <View style={styles.container}>
              <TouchableOSResponsive style={styles.loginButton} onPress={() => this.props.navigation.navigate('UpdateProfileScreen')}>
                  <Text style={styles.loginText}>EDIT</Text>
                  <Image style={styles.menuImage} source={Images.edit}></Image>
              </TouchableOSResponsive>
          </View>

          <Image style={styles.usernameIcon} source={Images.user} />
          {/* <Text
                      style={styles.containerInputStyle}>{this.props.loggedInUser.name}</Text>
                  /> */}
          <Image style={styles.useremailIcon} source={Images.email} />
          {/* <Text
                      style={styles.containerInputStyle}>{this.props.loggedInUser.email}
                      </Text>/> */}
          <Image style={styles.phoneIcon} source={Images.phone} />
          {/* <Text
                      style={styles.containerInputStyle}>{this.props.loggedInUser.phone_number}
                      </Text>/> */}
      </View>
  </View >    );
  }
}

const mapStateToProps = state => {
  const { login } = state;
  return {
    login
  };
};

const mapDispatchToProps = dispatch => {
  return {

  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ProfileScreen);
