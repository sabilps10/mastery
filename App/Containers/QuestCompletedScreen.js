import React, { Component } from 'react'
import { View, ScrollView, Text, Image, FlatList, Dimensions } from 'react-native'
import { connect } from 'react-redux'

import styles from './Styles/QuestCompletedScreenStyle'
import { Images } from '../Themes'

import TouchableOSResponsive from '../Components/MaterialUi/TouchableOsResponsive'
import HeaderMastery from '../Components/HeaderMastery';

class QuestCompletedScreen extends Component {
  renderQuest (data, index) {
    // MOCK DATA
    const title = "BUG FIXINGS"
    const date = "08/07/2019"
    const points = 100

    return (
      <View style={styles.questComponentContainer} key={index}>
        <View style={styles.questIconContainer}>
          <Image style={styles.questIcon} source={Images.questIcon}/>
        </View>
        <View style={styles.questDescriptionContainer}>
          <Text style={styles.questTitleText}>{title}</Text>
          <Text style={styles.questDateText}>{date}</Text>
        </View>
        <Text style={styles.questPointsText}>{points.toString()}</Text>
      </View>
    )
  }

  render () {
    const questsData = [1, 2, 3, 4]

    return (
      <View style={styles.rewardsContainer}>
        <HeaderMastery
          navigation={this.props.navigation}
          title={"QUESTS"}  
        >
          <View style={styles.questButtonContainer}>
            <TouchableOSResponsive onPress={() => { this.props.navigation.navigate('QuestNewScreen') }}>
              <Text style={styles.questButtonTextDeactivated}>NEW QUEST</Text>
            </TouchableOSResponsive>
            <TouchableOSResponsive onPress={() => { this.props.navigation.navigate('QuestOngoingScreen') }}>
              <Text style={styles.questButtonTextDeactivated}>YOUR QUEST</Text>
            </TouchableOSResponsive>
            <Text style={styles.questButtonText}>COMPLETED</Text>
          </View>
        </HeaderMastery>
        <ScrollView style={styles.questsContainer}>
          {
            questsData.map((data, index) => {
              return this.renderQuest(data, index)
            })
          }
        </ScrollView>
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(QuestCompletedScreen)
