import React, { Component } from 'react'
import { View, ScrollView, Text, Image, ImageBackground } from 'react-native'
import { connect } from 'react-redux'

import styles from './Styles/AppreciationsScreenStyle'

import TouchableOSResponsive from '../Components/MaterialUi/TouchableOsResponsive'
import HeaderMastery from '../Components/HeaderMastery'
import RewardActions from '../Redux/RewardRedux';
import SnackBar from 'react-native-snackbar-component'
import { Col, Row, Grid } from "react-native-easy-grid";


class AppreciationsScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      snackbar: false,
      snackbarText: "",
    }
  }

  componentDidMount() {
    const { getRewards, login } = this.props;

    getRewards({
      company_id: login.loggedInUser.companies[0].id
    });
  }
  componentDidUpdate(oldProps, oldStates) {
    if (oldProps.reward.id !== this.props.reward.id) {
      if (this.props.reward.id === null)
        return this.props.navigation.navigate("LoginScreen");
    }
    if (oldProps.reward !== this.props.reward) {
      if (this.props.reward.status === 403) {
        this.props.logout();
        this.props.navigation.navigate("LoginScreen");
      } else {
        this.setState({ loaded: true })
      }
    }
  }

  onPressReward(idReward, points) {
    const point_left = this.props.home.payload.stats.points_left
    if (__DEV__) console.tron.log('point:', points);
    if (__DEV__) console.tron.log('point_left:', point_left);
    if (__DEV__) console.tron.log('reward id:', idReward);
    if (point_left > points || point_left === points) {
      this.props.pickReward(idReward);
      this.props.navigation.navigate("SuccessRedeemScreen");
    }
    else {
      this.setState({
        snackbar: true,
        snackbarText: "Sorry, you have insufficient points"
      })
    }
  }

  renderRewardData(data, index) {
    const title = data.name
    const desc = data.description
    const points = data.points
    const idReward = data.id
    const uri = data.picture

    return (
      <View style={{ ...styles.ticketComponentContainer }} key={index}>
        <Image style={styles.ticketImage} source={{ uri: uri }} />
        <View style={{ ...styles.ticketContainer }}>
          <Text style={styles.ticketTitle}>{title}</Text>
          <Text style={styles.ticketDescription}>{desc}</Text>
          <Row size={0.1}>
            <Row>
              <Text style={styles.pointText}>{points}</Text>
              <Text style={styles.pointText}>POINTS</Text>
            </Row>
            <Row style={{marginLeft: 30, marginTop: -10}}>
              <TouchableOSResponsive style={styles.reedemButtonContainer} onPress={() => this.onPressReward(idReward, points)}>
                <Text style={styles.reedemText}>REEDEM</Text>
              </TouchableOSResponsive>
            </Row>
          </Row>
          {/* <View style={styles.pointReedemContainer}>
            <Text style={styles.pointText}>{points}</Text>
            <Text style={styles.pointText}>POINTS</Text>
            <TouchableOSResponsive style={styles.reedemButtonContainer} onPress={() => this.onPressReward(idReward, points)}>
              <Text style={styles.reedemText}>REEDEM</Text>
            </TouchableOSResponsive>
          </View> */}
        </View>
      </View>
    )
  }

  render() {
    const { reward } = this.props;
    if (__DEV__) console.tron.log('reward props:', this.props);
    return (
      <View style={styles.rewardsContainer}>
				<View style={styles.headerContainer}/>
        <HeaderMastery
          navigation={this.props.navigation}
          title={'APPRECIATIONS'}
        />
        {
          reward.payload !== null ? (
            <ScrollView style={styles.ticketsContainer}>
              {
                reward.payload.rewards.map((data, index) => {
                  return this.renderRewardData(data, index)
                })
              }
            </ScrollView>
          ) :
            (
              <View style={styles.ticketComponent}>
                <Text style={styles.welcomeTextBold}>You don't have any appreciations to reedem right now!</Text>
              </View>
            )
        }
        <SnackBar visible={this.state.snackbar} textMessage={this.state.snackbarText} actionHandler={() => { this.setState({ snackbar: false }) }} actionText="Got it!" />
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  const { reward, home, login } = state;
  return {
    reward,
    home,
    login,

  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getRewards: (data) => dispatch(RewardActions.rewardRequest(data)),
    pickReward: id => dispatch(RewardActions.pickReward(id)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(AppreciationsScreen)
