import React, { Component } from 'react'
import { View, Text, Image, ActivityIndicator, ScrollView, KeyboardAvoidingView, Modal, ProgressBarAndroid } from 'react-native'
import { connect } from 'react-redux'
import styles from './Styles/EditProfileScreenStyle'
import { Images, Colors } from '../Themes'
import ImagePicker from "react-native-image-picker"
import MaterialEditText from '../Components/MaterialEditText'
import TouchableOSResponsive from '../Components/MaterialUi/TouchableOsResponsive'
import HeaderMastery from '../Components/HeaderMastery'

import ProfilePhotoActions from "../Redux/ProfilePhotoRedux"
import EditProfileActions from '../Redux/EditProfileRedux'
import NewHomeActions from "../Redux/NewHomeRedux"

class EditProfileScreen extends Component {
    constructor(props) {
        super(props);
        
        this.state = {
            progress: false,
            profile: false,
            photoSource: null,
            newPhotoURL: null,
            name: props.newhome ? props.newhome.profile.name ? props.newhome.profile.name : '' : '',
            phone_number: props.newhome ? props.newhome.profile.phone_number ? props.newhome.profile.phone_number : '' : '',
            profile_pic: props.newhome ? props.newhome.profile.avatar ? props.newhome.profile.avatar : '' : '',
            loading: false,
            menuOpen: false
        };
    }

    showImagePicker() {
        const options = {
            title: "Take Photo",
            takePhotoButtonTitle: "From Camera",
            mediaType: "photo",
            // customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
            storageOptions: {
                skipBackup: true,
                path: "mastery"
            }
        };
        ImagePicker.showImagePicker(options, response => {
            if (__DEV__) console.tron.log("Response = ", response);

            if (response.didCancel) {
                if (__DEV__) console.tron.log("User cancelled image picker");
            } else if (response.error) {
                if (__DEV__) console.tron.log("ImagePicker Error: ", response.error);
            } else if (response.customButton) {
                if (__DEV__) console.tron.log("User tapped custom button: ", response.customButton);
            } else {
                this.setState({
                    photoSource: response,
                    menuOpen: false,
                    loading: true
                });
                const uploadOpts = {
                    file: response,
                    body: {
                        //description: this.state.photoSource.fileName,
                        submit_type: "photo"
                    },
                };

                this.props.setProfilePhoto(uploadOpts);
            }
        });
    }

    componentDidMount() {
        this.loadList();
        const { newhome } = this.props;
        this.setState({
            profile: newhome.profile
        });
    }

    componentDidUpdate (prevProps, prevState) {
        const { edit_profile, profile_photo, getNewHome } = this.props

        if (prevProps.edit_profile.fetching && !edit_profile.fetching) {
            if (edit_profile.error === null) {
                this.props.navigation.navigate('SuccessEditProfileScreen')
                getNewHome()
            } else {
                alert('Failed edit profile');
            }
        }

        if (prevProps.profile_photo.fetching && !profile_photo.fetching) {
            if (profile_photo.error === null) {
                this.setState({
                    newPhotoURL: profile_photo.payload ? profile_photo.payload.file_url : ''
                })
            } else {
                alert('Failed upload photo');
            }
        }
    }

    loadList() {
        const { newhome } = this.props;
        this.setState({
            profile: newhome.profile
        });
        // this.props.loggedInUser.profile_pic;
    }

    onPressEditProfile() {
        const { name, phone_number, newPhotoURL } = this.state

        if (name.length === 0 || phone_number.length === 0) {
            alert("Please Fill The Form!");
        } else {
            if (newPhotoURL !== null) {
                this.props.doEditProfile({
                    name,
                    phone_number,
                    profile_pic: newPhotoURL
                })
            } else {
                this.props.doEditProfile({
                    name,
                    phone_number
                })
            }
        }
    }

    renderData(item) {
        return (
            <View
                key={item.id}
            >
                {item.submit_type === "photo" ? (
                    <Image
                        style={styles.avatarImage}
                        source={{
                            uri: item.path
                        }}
                    />
                ) : null}
            </View>
        );
    }

    renderFlatList() {
        const { payload } = this.props.newhome;
        if (payload === null) return null;
        return (
            <Image style={styles.avatarImage}
                renderItem={({ item }) => this.renderData(item)}></Image>);
    }

    render() {
        const { newhome, edit_profile, profile_photo } = this.props;
        const { profile, photoSource } = this.state;
        if (__DEV__) console.tron.log(newhome);
        if (__DEV__) console.tron.log("isi edit = ", this.props);
        return (
            <View style={styles.loginContainer}>
                <HeaderMastery
                    navigation={this.props.navigation}
                    title={'EDIT PROFILE'}
                />
                <KeyboardAvoidingView behavior='position'>
                    <View style={styles.photoContainer}>
                        {/* {this.props.edit_profile.fetching ? ( */}
                        <TouchableOSResponsive onPress={() => {
                            this.showImagePicker();
                        }}>
                            <Image style={styles.avatarImage} source={photoSource !== null ? photoSource : { uri: this.state.profile.avatar }}></Image>
                        </TouchableOSResponsive>
                        {/* ) : (
                                this.renderFlatList()
                            )} */}
                    </View>
                    <View style={styles.inputContainer}>
                        <Image style={styles.inputIcon} source={Images.user} />
                        <MaterialEditText
                            containerInputStyle={styles.containerInputStyle}
                            value={''}
                            isFocused={this.state.name.length > 0}
                            propsValue={this.state.name}
                            label={'Name'}
                            onChangeText={(name) => {
                                this.setState({ name })
                            }} />
                    </View>
                    <View style={styles.inputContainer}>
                        <Image style={styles.inputIcon} source={Images.phone} />
                        <MaterialEditText
                            containerInputStyle={styles.containerInputStyle}
                            value={''}
                            isFocused={this.state.phone_number.length > 0}
                            propsValue={this.state.phone_number}
                            label={'Phone'}
                            onChangeText={(phone_number) => {
                                this.setState({ phone_number })
                            }} />
                    </View>
                </KeyboardAvoidingView>
                <TouchableOSResponsive style={styles.passButton} onPress={() => this.props.navigation.navigate('EditPasswordScreen')}>
                    <Text style={styles.loginText}>EDIT PASSWORD</Text>
                </TouchableOSResponsive>
                <TouchableOSResponsive style={styles.loginButton} onPress={() => this.onPressEditProfile(profile.avatar)}>
                    <Text style={styles.loginText}>SAVE EDIT</Text>
                </TouchableOSResponsive>
                <Modal
                visible={edit_profile.fetching || profile_photo.fetching}
                animated={true}
                animationType={'fade'}
                transparent={true}
                style={styles.container}>
                    <View style={styles.backgroundLoading}>
                        <ProgressBarAndroid color={Colors.white} style={{width: 30, height: 30}} />
                    </View>
                </Modal>
            </View>

        )
    }
}

const mapStateToProps = (state) => {
    const { newhome, edit_profile, profile_photo } = state
    return {
        newhome,
        edit_profile,
        profile_photo
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        getNewHome: () => dispatch(NewHomeActions.newHomeRequest()),
        doEditProfile: (data) => dispatch(EditProfileActions.editProfileRequest(data)),
        setProfilePhoto: (data) => dispatch(ProfilePhotoActions.profilePhotoRequest(data))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(EditProfileScreen)
