import React, { Component } from 'react'
import { View, Text, Image, ActivityIndicator, Platform } from 'react-native'
import { connect } from 'react-redux'
import styles from './Styles/ForgetPasswordScreenStyle'
import { Images } from '../Themes'

import MaterialEditText from '../Components/MaterialEditText'
import TouchableOSResponsive from '../Components/MaterialUi/TouchableOsResponsive'
import SnackBar from 'react-native-snackbar-component'
import Secrets from "react-native-config";
import HeaderMastery from '../Components/HeaderMastery'


import ForgetPasswordActions from '../Redux/ForgetPasswordRedux';

class ForgetPasswordScreen extends Component {
  constructor(props) {
    super(props)

    this.state = {
      snackbar: false,
      snackbarText: "",
      email: "",
      auth: false,
      debugmsg: ""
    }
  }
  // componentDidMount() {
  //   if (this.props.login.logout) {
  //     this.props.login.signout()
  //   }
  // }
  //   componentDidUpdate(prevProps, oldStates) {
  //     //check if access_token still exists.
  //     //if exists, then we verify the token lifetime.
  //     //if token expired, we need to re-login.
  //     //but if token still alive, then we autologin
  //     if (prevProps.login.access_token !== this.props.login.access_token) {
  //       if (this.props.login.access_token !== null) {
  //         this.props.navigation.navigate('HomeScreen')
  //       }

  //     }
  //     if (prevProps.login !== this.props.login) {
  //       if (!this.state.auth) return;
  //       if(__DEV__) console.tron.log("login berubah:", this.props.login);

  //       if (this.props.login.payload !== null) {

  //         if (this.props.login.payload.status === 1) {
  //           if(__DEV__) console.tron.log("can login");
  //           if (this.state.auth) {
  //             this.setState({ auth: false })
  //             this.props.navigation.navigate('HomeScreen')
  //           }
  //         } else {
  //           if(__DEV__) console.tron.log("cannot login");

  //           this.setState({
  //             auth: false,
  //             snackbar: true,
  //             snackbarText: "Login Failed.."
  //           })
  //         }

  //       }
  //       if (this.props.login.error) {
  //         if(__DEV__) console.tron.log('login failed')

  //         this.setState({
  //           auth: false,
  //           snackbar: true,
  //           snackbarText: "Email dan Password anda salah !"
  //         })
  //       }
  //     }
  //  }

  onPressForgetPassword() {
    const { email, auth } = this.state
    if(__DEV__) console.tron.log("forget password nih");
    if (email.length > 0) {
      if(__DEV__) console.tron.log("do forget password");
      this.setState({ auth: true })
      this.props.forgetpasswordAction({
        email,
      })
    }
    this.props.navigation.navigate('SuccessResetPassScreen');
  }

  render() {

    return (
      <View style={styles.rewardsContainer}>
       <HeaderMastery
                    navigation={this.props.navigation}
                    title={'    FORGOT PASSWORD'}
                />
               
      <View style={styles.loginContainer}>
        <View style={styles.background} />
        <Image style={styles.emailIcon} source={Images.email} />
        <MaterialEditText
          containerInputStyle={styles.containerInputStyle}
          value={''}
          label={'E-MAIL'}
          onChangeText={(email) => {
            this.setState({ email })
          }} />
        {(this.state.auth === true) ? (
          <ActivityIndicator size={Platform.OS === "ios" ? 'large' : "large"} color="#ffffff" />
        ) : (
            <TouchableOSResponsive style={styles.loginButton} onPress={() => this.onPressForgetPassword()}>
              <Text style={styles.loginText}>RESET PASSWORD</Text>
            </TouchableOSResponsive>
          )}
        <SnackBar visible={this.state.snackbar} textMessage={this.state.snackbarText} actionHandler={() => { this.setState({ snackbar: false }) }} actionText="Got it!" />
      </View>
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    forgetpassword: state.forgetpassword
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    forgetpasswordAction: (input) => dispatch(ForgetPasswordActions.forgetpasswordRequest(input))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ForgetPasswordScreen)
