import React, { Component } from 'react'
import { View, ScrollView, Text, Image, ImageBackground } from 'react-native'
import { connect } from 'react-redux'
import { Col, Row, Grid } from "react-native-easy-grid";


import styles from './Styles/RewardsScreenStyle'
import { Images } from '../Themes'
import TouchableOSResponsive from '../Components/MaterialUi/TouchableOsResponsive'

import HeaderMastery from '../Components/HeaderMastery';
import RedeemActions from '../Redux/RedeemRedux';
class RewardsScreen extends Component {
  constructor(props) {
    super(props);
  }
  componentDidMount() {
    const { getRedeems } = this.props;

    getRedeems();
  }
  renderTicket(data, index) {
    // MOCK DATA
    const title = data.name
    const points = data.points
    const idReward = data.reward_id

    return (
      <View style={styles.ticketComponentContainer} key={index} >
        <Row style={styles.row1}>
          <Col style={{ width: "25%", borderRightColor: "#D3D3D3", borderRightWidth: 1 }}>
            <Col style={{ justifyContent: "center", alignItems: "center" }}>
              <Text style={styles.pointValueText}>{points}</Text>
            </Col>
            <Col style={{ justifyContent: "center", alignItems: "center" }}>
              <Text style={styles.pointTitleText}>POINTS</Text>
            </Col>
          </Col>
          <Row>
            <Text style={styles.descriptionTitleText}>{title}</Text>
          </Row>
        </Row>
      </View>
    )
  }

  render() {
    const { redeem } = this.props;
    if (__DEV__) console.tron.log('props redeem:', redeem);
    return (
      <View style={styles.rewardsContainer}>
				<View style={styles.headerContainer}/>
        <HeaderMastery
          navigation={this.props.navigation}
          title={'REWARDS'}
        />
        {
          redeem.payload !== null ? (
            <ScrollView style={styles.ticketsContainer}>
              {
                redeem.payload.redeems.map((data, index) => {
                  return this.renderTicket(data, index)
                })
              }
            </ScrollView>
          ) :
            (
              <View style={{ paddingTop: 60, paddingLeft: 30, paddingRight: 30 }}>
                <Text>Silahkan Tukar Point anda dengan Reward yang tersedia di menu Appreciations !</Text>
                <TouchableOSResponsive style={{ ...styles.reedemButtonContainer, marginTop: 60, paddingTop: 2 }} onPress={() => {
                  this.props.navigation.navigate('AppreciationsScreen')
                }}>
                  <Text style={{ ...styles.reedemText }}>REEDEM</Text>
                </TouchableOSResponsive>
              </View>
            )
        }

      </View>
    )
  }
}

const mapStateToProps = (state) => {
  const { redeem } = state;
  return {
    redeem,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getRedeems: (data) => dispatch(RedeemActions.redeemRequest(data)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(RewardsScreen)
