import React, { Component } from 'react'
import { View, Text, Image, ActivityIndicator, StyleSheet } from 'react-native'
import { connect } from 'react-redux'

import styles from './Styles/SuccessRedeemScreenStyle'
import { Images } from '../Themes'

import MaterialEditText from '../Components/MaterialEditText'
import TouchableOSResponsive from '../Components/MaterialUi/TouchableOsResponsive'
import SnackBar from 'react-native-snackbar-component'
import Secrets from "react-native-config";



class SuccessRedeemScreen extends Component {
    
  render() {

    return (
        <View style={styles.container}>
                <Image style={styles.menuImage} source={Images.checklist}></Image>
                <Text style={styles.welcomeText}>CONGRATULATION</Text>
                <Text style={styles.welcomeTextBold1}>YOUR REWARD HAS BEEN SUCCESSFULLY REDEEMED WITH YOUR EXISTING POINT(S)</Text>
                <Text style={styles.welcomeTextBold2}>THE MASTERY SYSTEM WILL PROCESS YOUR POINT REDEEM</Text>
                <TouchableOSResponsive style={styles.loginButton} onPress={() => this.props.navigation.navigate('RewardsScreen')}>
                    <Text style={styles.loginText}>CONTINUE</Text>
                </TouchableOSResponsive>
        </View>
    );
}
}
    
    
export default SuccessRedeemScreen




 
