import React, { Component } from 'react'
import { View, ScrollView, Text, Image, ImageBackground, ActivityIndicator, Dimensions } from 'react-native'
import { connect } from 'react-redux'

import styles from './Styles/MainTitleScreenStyle'
import { Images } from '../Themes'
import CoachingProgrammeActions from "../Redux/CoachingProgrammeRedux";
import TouchableOSResponsive from '../Components/MaterialUi/TouchableOsResponsive'
import HeaderMasteryCoaching from '../Components/HeaderMasteryCoaching'
// import VideoPlayerCoach from "../Components/VideoPlayerCoach";
import Video from "react-native-video";
import MediaControls, { PLAYER_STATES } from "react-native-media-controls";
import Icon from "react-native-vector-icons/FontAwesome";
import HTMLView from 'react-native-htmlview';
import YouTube from 'react-native-youtube';

class CoachingProgrammeMainTitleScreen extends Component {
    constructor(props) {
        super(props);

        let questHeight = []
        for (let index = 0; index < 50; index++) {
            questHeight.push(80)
        }

        this.state = {
            video_player: false,
            text_description: false,
            currentTime: 0,
            duration: 0,
            isFullScreen: true,
            isLoading: true,
            paused: false,
            playerState: PLAYER_STATES.PLAYING,
            screenType: "cover",
            id: props.navigation.state.params.id,
            payment_url: "",
            questHeight: questHeight,
            youtubePlayerReady: false,
            fullscreen: false,
            is_free:false,
        };

        this.renderQuest = this.renderQuest.bind(this)
    }

    componentDidMount() {
        this.props.getCoachId(this.props.navigation.state.params.id);

        setTimeout(() => {
            this.setState({
                youtubePlayerReady: true
            })
        }, 500)
    }
    componentDidUpdate(oldProps, oldStates) {
        if (__DEV__) console.tron.log("payment_url ", this.props.coach.payment_url);
        if (oldProps.coach.payment_url !== this.props.coach.payment_url) {
            if(this.state.is_free){
                //this.props.getCoachId(this.props.navigation.state.params.id);
                this.props.navigation.navigate('SuccessCoachingScreen')
               // this.setState({is_free:false})
            }else{
                this.props.navigation.navigate('MyWebScreen', { url: this.props.coach.payment_url })
                this.setState({
                    payment_url: false,
                    is_free:false
                })
            }
            
        }
        // else if (this.props.coach.payment_url === "") {
        //     this.props.navigation.navigate('SuccessCoachingScreen')
        // }
    }

    onSeek = seek => {
        this.videoPlayer.seek(seek);
    };
    onPaused = playerState => {
        this.setState({
            paused: !this.state.paused,
            playerState
        });
    };
    onReplay = () => {
        this.setState({ playerState: PLAYER_STATES.PLAYING });
        this.videoPlayer.seek(0);
    };
    onProgress = data => {
        const { isLoading, playerState } = this.state;
        // Video Player will continue progress even if the video already ended
        if (!isLoading && playerState !== PLAYER_STATES.ENDED) {
            this.setState({ currentTime: data.currentTime });
        }
    };
    onLoad = data => this.setState({ duration: data.duration, isLoading: false });
    onLoadStart = data => this.setState({ isLoading: true });
    onEnd = () => this.setState({ playerState: PLAYER_STATES.ENDED });
    onError = () => alert("Oh! ", error);
    exitFullScreen = () => {
        alert("Exit full screen");
    };
    enterFullScreen = () => { };
    onFullScreen = () => {
        if (this.state.screenType == "content")
            this.setState({ screenType: "cover" });
        else this.setState({ screenType: "content" });
    };

    onPressSubscribe(price) {
        this.props.subscribeCoach(this.props.navigation.state.params.id);

        if(parseInt(price)===0){
            this.setState({
                is_free:true
            })
        }else{
            this.setState({
                is_free:false
            })
        }
    }

    renderButton(data) {
        const price = data.price
        const complete_text = data.complete_text
        function currencyFormat(num) {
            return 'Rp.' + num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.') + ',-'
        }
        //this.props.coach.programme_subscribers.programme_status === 2;
        if (typeof this.props.coach.programme_subscribers.programme_status === 'undefined') {
            return (
                <View>
                    <View style={styles.contentDescriptionContainerTop}>
                        <Text style={styles.labelText}>Price</Text>
                        <Text style={styles.priceText}>{price > 0 ? currencyFormat(price) : 'Free'}</Text>
                    </View>
                    <TouchableOSResponsive style={styles.loginButton} onPress={() => {
                        
                        this.onPressSubscribe(price);
                        }}>
                        <Text style={styles.loginText}>SUBSCRIBE NOW</Text>
                    </TouchableOSResponsive>
                </View>
            )
        } else if (this.props.coach.programme_subscribers.programme_status === 0 && this.props.coach.coach_programme.progress === 1) {
            return (
                <View>
                    <View style={styles.contentDescriptionContainerTop}>
                        <Text style={styles.labelText}>Price</Text>
                        <Text style={styles.priceText}>{price > 0 ? currencyFormat(price) : 'Free'}</Text>
                    </View>
                    <TouchableOSResponsive style={styles.loginButton} onPress={() => this.onPressSubscribe(price)}>
                        <Text style={styles.loginText}>SUBSCRIBE NOW</Text>
                    </TouchableOSResponsive>
                </View>
            )
        } else if (this.props.coach.programme_subscribers.programme_status === 1 && this.props.coach.coach_programme.progress === 1) {
            return (
                <View style={styles.contentDescriptionContainerTop}>
                    <TouchableOSResponsive style={styles.login2Button} onPress={() => this.props.navigation.navigate('QuestNewScreen')}>
                        <Text style={styles.loginText}>VIEW RECENT QUEST</Text>
                    </TouchableOSResponsive>
                </View>
            )
        } else if (this.props.coach.programme_subscribers.programme_status === 2 && this.props.coach.coach_programme.progress === 1) {
            return (
                <View style={styles.ticketComponent}>
                    <Text style={styles.welcomeTextBoldBottom}>{this.props.coach.coach_programme.complete_text}</Text>
                </View>

            )
        } else {
            return (<View>
                <View style={styles.contentDescriptionContainerTop}>
                    <Text style={styles.labelText}>Price</Text>
                    <Text style={styles.priceText}>{price > 0 ? currencyFormat(price) : 'Free'}</Text>
                </View>
                <TouchableOSResponsive style={styles.loginButton} onPress={() => this.onPressSubscribe(price)}>
                    <Text style={styles.loginText}>EXTEND SUBSCRIPTION</Text>
                </TouchableOSResponsive>
            </View>);
        }
    }

    renderQuest(data, index) {
        let quests = this.props.coach.coach_programme.quests
        const quest_name = data.name
        const quest_desc = data.description
        if (typeof quests === 'undefined' || quests === null || quests.length === 0) {
            return (
                <View key={index}>
                    <View>
                        <Text style={{ fontSize: 20, fontWeight: "bold", color: "red", marginLeft: 30, marginBottom: 20 }}>You don't have any quests right now!</Text>
                    </View>
                </View>
            )
        }
        else {
            const extraHeight = (quest_desc.split("<p>").length - 1)
            return (
                <View style={{ ...styles.contentDescriptionContainer, height: this.state.questHeight[index] + (extraHeight * 40) }} >
                    <View style={{ marginTop: -5, marginLeft: -10 }}
                        onLayout={(event) => {
                            var { x, y, width, height } = event.nativeEvent.layout;
                            // let questHeight = this.state.questHeight
                            // questHeight[index] = height

                            // console.tron.log(questHeight[index])

                            // this.setState({
                            //     questHeight: questHeight
                            // })
                        }}
                    >
                        <Text style={styles.questText}>{quest_name}</Text>
                        <HTMLView style={styles.descriptionText} value={quest_desc} />
                        {/* <Text style={styles.descriptionText}>{quest_desc}</Text> */}
                    </View>
                </View>
            )
        }
    }

    renderRewardData(data) {

        const coach_photo = data.coach_main_photo
        const coach_name = data.coach_name
        const profession = data.profession
        const description = data.description
        const video_url = data.video_url
        const youtube_id = data.youtube_id
        const thumbnail = data.thumbnail_url ? data.thumbnail_url : false
        const isYoutube = data.is_youtube_video ? data.is_youtube_video : false
        const name = data.name

        console.tron.log({data})
        
        return (
            <ScrollView style={styles.ticketsContainer}>
                <View style={styles.ticketComponentContainer}>
                    <ImageBackground style={styles.ticketImage} source={{ uri: coach_photo }}>
                        <Image style={styles.blur} source={Images.blur} />
                        <Text style={styles.welcomeTextBold1}>{coach_name.charAt(0).toUpperCase() + coach_name.slice(1)}</Text>
                        <HTMLView style={styles.welcomeTextBold2} value={profession} />
                        <Text style={styles.welcomeTextBold1}>{}</Text>
                    </ImageBackground>
                </View>
                {video_url !== null && video_url !== undefined && video_url.length > 0 ?
                    isYoutube ? this.state.youtubePlayerReady ?
                    <View style={styles.youtubeContainer}>
                        <YouTube
                        apiKey={'AIzaSyDRB9xT9wbJ9671RjH79AV-_eUTbo-KBkY'} // <= Todo: Pindah ke env
                        videoId={youtube_id} // <= Youtube Video ID, Contoh: wOcCu31oi7Q
                        play={false}
                        fullscreen={this.state.fullscreen}
                        onReady={e => this.setState({ isReady: true })}
                        onChangeState={e => {
                            if (e.state === 'playing' || e.state === 'buffering') {
                            this.setState({ fullscreen: true }) 
                            } else if (e.state === 'ended') {
                            this.setState({ fullscreen: false });
                            }
                        }}
                        onChangeQuality={e => this.setState({ quality: e.quality })}
                        onError={e => this.setState({ error: e.error })}
                        style={{ height: Dimensions.get('screen').width / 1.75, width: Dimensions.get('screen').width * 0.85 }}
                        />
                    </View>
                    : null
                    :
                    <View style={styles.contentDescriptionContainerVideo}>
                        <TouchableOSResponsive onPress={() => this.props.navigation.navigate('VideoPlayerCoach')}>
                            <View style={styles.videobox}>
                            { thumbnail && <Image style={styles.thumbnail} source={{uri: thumbnail}}/> }
                                <Icon
                                    name="play"
                                    size={50}
                                    color="#cccccc"
                                    style={{
                                        position: 'absolute',
                                        alignContent: "center",
                                        textAlign: "center",
                                        justifyContent: "center",
                                        alignSelf: 'center',
                                        marginTop: 40
                                    }}
                                />
                                {!thumbnail &&
                                <Text
                                    style={{
                                        position: 'absolute',
                                        alignContent: "center",
                                        textAlign: "center",
                                        justifyContent: "center",
                                        alignSelf: 'center',
                                        marginTop: 95,
                                    }}
                                >
                                    Click Here To Play Video
                                </Text>
                                }
                            </View>
                        </TouchableOSResponsive>

                    </View>
                    : null}
                <View style={styles.contentDescriptionContainerBottom}>
                    <Text style={{ ...styles.welcomeText, marginTop: -45, marginBottom: 30 }}>{name}</Text>
                    <HTMLView style={{ ...styles.welcomeTextBold, marginTop: -25, marginBottom: 50 }} value={description} />
                </View>
            </ScrollView>
        )
    }




    render() {
        const { coach, navigation } = this.props;
        if (__DEV__) console.tron.log("isi coach ", coach);

        return (
            <View style={styles.rewardsContainer}>
								<View style={styles.headerContainer}/>
                <TouchableOSResponsive style={styles.buttonBackContainer} onPress={() => {
                    navigation.goBack()
                }}>
                    <Image style={styles.buttonBack} source={Images.buttonBackBorder} />
                </TouchableOSResponsive>
                <ScrollView style={styles.ticketsContainer}>

                    {
                        coach.coach_programme !== null ? (
                            <ScrollView style={styles.ticketsContainer}>
                                {this.renderRewardData(this.props.coach.coach_programme)}
                                <Text style={{ ...styles.questTitleText, marginBottom: 20 }}>Quest List</Text>
                                {
                                    coach.coach_programme.quests.map((data, index) => {
                                        return this.renderQuest(data, index)
                                    })
                                }
                                <Text style={{ ...styles.questTitleText, marginVertical: 20 }}>About the Coach</Text>
                                <HTMLView style={{ ...styles.welcomeTextBold, marginBottom: 50 }} value={coach.coach_programme.coach_desc} />
                                {this.renderButton(this.props.coach.coach_programme)}
                            </ScrollView>
                        ) :
                            (
                                <View style={styles.ticketComponent}>
                                    <Text style={styles.welcomeTextBoldBottom}>You don't have any coaching programme right now!</Text>
                                </View>
                            )
                    }
                </ScrollView>
            </View>
        );
    }
}

const mapStateToProps = (state) => {
    const { coach } = state;
    return {
        coach
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        getCoachs: data => dispatch(CoachingProgrammeActions.coachingProgrammeRequest(data)),
        getCoachId: id => dispatch(CoachingProgrammeActions.coachingProgrammeDetailRequest(id)),
        pickCoach: id => dispatch(CoachingProgrammeActions.pickCoach(id)),
        subscribeCoach: id => dispatch(CoachingProgrammeActions.subscribeCoachingProgramme(id))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CoachingProgrammeMainTitleScreen)
