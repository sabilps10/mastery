import { StyleSheet, Dimensions } from 'react-native'
import { ApplicationStyles, Fonts, Colors, Metrics } from '../../Themes'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  rewardsContainer: {
    flex: 1
  },
  ticketsContainer: {
    marginTop: 260 * (Dimensions.get('screen').width / 768) / 4 - 6.5
	},
	headerContainer: {
		paddingTop: Metrics.navBarHeight,
		backgroundColor: Colors.masteryBlue
	},
  welcomeTextBold: {
    ...Fonts.style.title.h2,
    color: Colors.masteryBlue,
    fontWeight: 'bold',
    fontSize: 13,
    marginTop:30,
    textAlign:"justify",
    // fontFamily: "Montserrat-Black",
  },
  ticketComponentContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: -Metrics.baseMargin * 3,
    marginBottom: -Metrics.doubleBaseMargin * 3
  },
  ticketComponent: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 330,
  },
  ticketImage: {
    resizeMode: 'contain',
    width: Metrics.countWidthBased(Metrics.countHeightScala(175)),
    height: Metrics.countHeightBased(Metrics.countHeightScala(175)),
    borderRadius: 8
  },
  ticketContainer: {
    marginLeft: Metrics.doubleBaseMargin
  },
  ticketTitle: {
    ...Fonts.style.title.h5,
    width: Dimensions.get('screen').width / 2,
    fontWeight: 'bold',
    color: Colors.coal,
    marginBottom: Metrics.baseMargin
  },
  ticketDescription: {
    ...Fonts.style.body.p7,
    width: Dimensions.get('screen').width / 2,
    fontWeight: 'bold',
    color: Colors.charcoal,
    marginBottom: Metrics.baseMargin
  },
  pointReedemContainer: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  pointText: {
    ...Fonts.style.title.h5,
    textAlign: 'center',
    color: Colors.masteryOrange,
    fontWeight: 'bold',
    marginRight: Metrics.baseMargin / 2
  },
  reedemButtonContainer: {
    alignSelf: 'center',
    alignItems: 'center',
    width: Metrics.countWidthBased(Metrics.countWidthScala(75)),
    height: Metrics.countHeightBased(Metrics.countHeightScala(25)),
    borderWidth: 1,
    borderRadius: 15,
    paddingTop:6,
    borderColor: Colors.masteryOrange,
    backgroundColor: Colors.masteryOrange,
    marginLeft: Metrics.baseMargin
  },
  reedemText: {
    ...Fonts.style.title.h8,
    color: Colors.white,
    alignSelf: 'center',
    justifyContent: 'center',
    fontWeight: 'bold',
    textAlign: 'center'
  }
})
