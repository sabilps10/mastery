import { StyleSheet, Dimensions } from 'react-native'
import { ApplicationStyles, Fonts, Colors, Metrics } from '../../Themes'
import leftPad from 'left-pad'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  rewardsContainer: {
    flex: 1
  },
	headerContainer: {
		paddingTop: Metrics.navBarHeight,
		backgroundColor: Colors.masteryBlue
	},
  welcomeTextBold: {
    ...Fonts.style.title.h2,
    color: Colors.masteryBlue,
    fontWeight: 'bold',
    fontSize: 13,
    marginTop:Metrics.doubleBaseMargin * 3.3,
    marginLeft:20,
    marginRight:20,
    marginBottom:Metrics.doubleBaseMargin * 4,
    textAlign:"justify",
    // fontFamily: "Montserrat-Black",
  },
  welcomeTextBoldBottom: {
    ...Fonts.style.title.h2,
    color: Colors.masteryBlue,
    fontWeight: 'bold',
    fontSize: 13,
    marginTop:30,
    textAlign:"justify",
    // fontFamily: "Montserrat-Black",
  },
  ticketsContainer: {
    marginTop: 100 * (Dimensions.get('screen').width / 768) / 4 - 6.5
  },
  ticketComponent: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 330,
  },
  ticketComponentContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop:-60,
  },
  ticketImage: {
    resizeMode: 'cover',
    width: Metrics.countWidthBased(Metrics.countHeightScala(300)),
    height: Metrics.countHeightBased(Metrics.countHeightScala(100)),
    marginLeft:58,
    marginTop: 15,
    marginBottom:63,
    borderRadius: 20
  },
  ticketContainer: {
    marginLeft: 15,
    marginRight: 20,
    marginTop: -110,
  },
  ticketTitle: {
    ...Fonts.style.title.h5,
    width: Dimensions.get('screen').width / 2,
    fontWeight: 'bold',
    color: Colors.masteryBlue,
    fontSize: 16,
    textAlign: "left",
  },
  ticketDescription: {
    ...Fonts.style.body.p7,
    width: Dimensions.get('screen').width / 2,
    fontWeight: 'bold',
    color: Colors.charcoal,
    fontSize:12,
    textAlign: "left",
  },
  pointReedemContainer: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  pointText: {
    ...Fonts.style.title.h5,
    textAlign: 'center',
    color: Colors.masteryOrange,
    fontWeight: 'bold',
    marginRight: Metrics.baseMargin / 2
  },
  reedemButtonContainer: {
    alignSelf: 'center',
    alignItems: 'center',
    width: Metrics.countWidthBased(Metrics.countWidthScala(75)),
    height: Metrics.countHeightBased(Metrics.countHeightScala(25)),
    borderWidth: 1,
    borderRadius: 15,
    borderColor: Colors.masteryOrange,
    backgroundColor: Colors.masteryOrange,
    marginLeft: Metrics.baseMargin
  },
  reedemText: {
    ...Fonts.style.title.h8,
    color: Colors.white,
    alignSelf: 'center',
    justifyContent: 'center',
    fontWeight: 'bold',
    textAlign: 'center'
  }
})
