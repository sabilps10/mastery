import { StyleSheet, Dimensions } from 'react-native'
import { ApplicationStyles, Fonts, Colors, Metrics } from '../../Themes'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  rewardsContainer: {
    flex: 1
  },
	headerContainer: {
		paddingTop: Metrics.navBarHeight,
		backgroundColor: Colors.masteryBlue
	},
  ticketsContainer: {
    marginTop: 261 * (Dimensions.get('screen').width / 768) / 4 - 6.5
  },
  reedemButtonContainer: {
    alignSelf: 'center',
    alignItems: 'center',
    width: Metrics.countWidthBased(Metrics.countWidthScala(120)),
    height: Metrics.countHeightBased(Metrics.countHeightScala(25)),
    borderWidth: 1,
    borderRadius: 15,
    borderColor: Colors.masteryOrange,
    backgroundColor: Colors.masteryOrange,
    marginLeft: Metrics.baseMargin
  },
  ticketComponentContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  ticketImage: {
    justifyContent: 'center',
    alignItems: 'center',
    resizeMode: 'contain',
    width: Dimensions.get('screen').width,
    height: (Dimensions.get('screen').width) / 3
  },
  ticketContainer: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  row1: {
    height: Metrics.baseMargin * 10,
    width: Metrics.baseMargin * 35,
    marginTop: Metrics.baseMargin * 3,
    borderRadius: 10,
    backgroundColor: "#f3f3f3",
    alignItems: 'center',
    justifyContent: 'center',
  },
  pointContainer: {
    justifyContent: 'center',
    right: Dimensions.get('screen').width / 11
  },
  pointValueText: {
    ...Fonts.style.title.h2,
    textAlign: 'center',
    color: Colors.masteryOrange,
    fontWeight: 'bold', 
    fontSize: 20,
    marginTop: 30
  },
  pointTitleText: {
    ...Fonts.style.body.p5,
    textAlign: 'center',
    color: Colors.masteryOrange,
    fontWeight: 'bold',
    marginTop: -30
  },
  descriptionContainer: {
    justifyContent: 'center'
  },
  descriptionTitleText: {
    ...Fonts.style.title.h1,
    fontWeight: 'bold',
    color: Colors.coal,
    letterSpacing: 2,
    marginTop: -15,
    marginLeft: Metrics.doubleBaseMargin * 1.75
  },
  descriptionSubTitleText: {
    ...Fonts.style.title.h7,
    marginTop: -5,
    marginLeft: Metrics.doubleBaseMargin / 3
  },
  descriptionRewardText: {
    ...Fonts.style.body.p6,
    fontSize: 15,
    marginTop: 3,
    marginLeft: Metrics.doubleBaseMargin,
    marginBottom: Metrics.baseMargin
  }
})
