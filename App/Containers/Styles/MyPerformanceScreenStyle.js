import { StyleSheet, Dimensions } from 'react-native'
import { ApplicationStyles, Colors, Metrics, Fonts } from '../../Themes'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  HomeContainer: {
    flex: 1,
		paddingTop: Metrics.navBarHeight
  },
  animatedBox: {
    flex: 1,
    backgroundColor: Colors.white,
    width: 100,
    height: 100,
    padding: 10
  },
  body: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#F04812'
  },
  background: {
    position: 'absolute',
    width: Dimensions.get('screen').width,
    height: Dimensions.get('screen').height,
    backgroundColor: Colors.masteryBlue
  },
  menuButton: {
    marginTop: Metrics.baseMargin + 10,
    marginLeft: Metrics.baseMargin+10,
    width: Metrics.countWidthBased(Metrics.countWidthScala(30)),
  },
  menuImage: {
    resizeMode: 'contain',
    width: Metrics.countWidthBased(Metrics.countWidthScala(30))
  },
  welcomeTextConatiner: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: Metrics.doubleBaseMargin * 3,
  },
  welcomeText: {
    ...Fonts.style.title.h2,
    color: Colors.white
  },
  welcomeTextBold: {
    ...Fonts.style.title.h2,
    color: Colors.white,
    fontWeight: 'bold'
  },
  welcomeImageContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: Metrics.countHeightBased(Metrics.countHeightScala(-10)),
    marginBottom: Metrics.countHeightBased(Metrics.countHeightScala(30))
  },
  welcomeImageBackground: {
    resizeMode: 'contain',
    transform: [
      { scale: 0.775 }
    ]
  },
  welcomeImage: {
    resizeMode: 'center',
    transform: [
      { scale: 0.8 }
    ]
  },
  yourPointsContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: Metrics.countHeightBased(Metrics.countHeightScala(-90))
  },
  yourPointsTitle: {
    ...Fonts.style.title.h1,
    color: Colors.white,
    marginBottom: Metrics.countHeightBased(Metrics.countHeightScala(-15))
  },
  yourPointsContent: {
    ...Fonts.style.title.h1,
    fontSize: 60,
    fontWeight: 'bold',
    color: Colors.white
  },
  otherPointsContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginHorizontal: Metrics.doubleBaseMargin * 2,
    marginTop: Metrics.doubleBaseMargin * 2
  },
  pointsContainer: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  pointsText: {
    ...Fonts.style.title.h1,
    color: Colors.white
  },
  pointsName: {
    ...Fonts.style.title.h7,
    color: Colors.white
  },
  separator: {
    height: 50,
    borderWidth: 0.25,
    borderColor: Colors.white
  }
})
