import { StyleSheet, Dimensions } from 'react-native'
import { ApplicationStyles, Fonts, Colors, Metrics } from '../../Themes'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  container: {
    flex: 1
  },
	headerContainer: {
		// paddingTop: Metrics.navBarHeight,
		backgroundColor: Colors.masteryBlue,
		height: Metrics.navBarHeight
	},
  bottomBarContainer: {
    flexDirection: 'row',
    width: Dimensions.get('screen').width * 0.9,
    height: Metrics.countHeightBased(Metrics.countHeightScala(45)),
    borderWidth: 1,
    borderRadius: 15,
    borderColor: Colors.masteryOrange,
    backgroundColor: Colors.masteryOrange,
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'space-evenly',
    marginBottom: Metrics.doubleBaseMargin
  },
  bottomBarImageContainer: {
    width: Metrics.countWidthBased(Metrics.countWidthScala((100/2.5))) * 1.75,
    height: Metrics.countHeightBased(Metrics.countHeightScala((68/2.5))),
    alignItems: 'center',
    marginBottom: Metrics.baseMargin / 2
  },
  bottomBarImage: {
    alignSelf: 'center',
    resizeMode: 'contain',
    width: Metrics.countWidthBased(Metrics.countWidthScala((60/2.5))),
    height: Metrics.countHeightBased(Metrics.countHeightScala((60/2.5)))
  },
  iconTitle: {
    ...Fonts.style.body.p10,
    color: Colors.coal,
    textAlign: 'center'
  },
  bottomBarImageDeactivated: {
    alignSelf: 'center',
    resizeMode: 'contain',
    width: Metrics.countWidthBased(Metrics.countWidthScala((60/2.5))),
    height: Metrics.countHeightBased(Metrics.countHeightScala((60/2.5))),
    tintColor: Colors.charcoal
  },
  iconTitleDeactivated: {
    ...Fonts.style.body.p10,
    color: Colors.charcoal,
    textAlign: 'center'
  },
  menuContainer: {
    position: 'absolute',
    alignSelf: 'flex-end',
    alignItems: 'center',
    bottom: Dimensions.get('screen').height * 0.125,
    right: Metrics.doubleBaseMargin
  },
  iconImage: {
    resizeMode: 'contain',
    width: Metrics.countWidthBased(Metrics.countWidthScala((80/2))),
    height: Metrics.countHeightBased(Metrics.countHeightScala((80/2))),
    marginTop: Metrics.baseMargin
  },
  iconImageBig: {
    resizeMode: 'contain',
    width: Metrics.countWidthBased(Metrics.countWidthScala((150/2))),
    height: Metrics.countHeightBased(Metrics.countHeightScala((150/2))),
    marginTop: Metrics.baseMargin
  },
  flatListContainer: {
    flex: 1,  
    alignSelf: 'center',
    backgroundColor: 'transparent',
    width: Dimensions.get('screen').width,
    marginTop: 261 * (Dimensions.get('screen').width / 768) / 4 - 6.5
  },
  newsletterContainer: {
    alignSelf: 'center',
    justifyContent: 'center',
    marginTop: 261 * (Dimensions.get('screen').width / 768) / 4 - 6.5,
    marginLeft: Dimensions.get('screen').width / 2 * 0.125,
    marginRight: Dimensions.get('screen').width / 2 * 0.125
  },
  newsletterImage: {
    resizeMode: 'contain',
    width: Dimensions.get('screen').width / 2 * 0.75,
    height: Dimensions.get('screen').width / 2 * 0.75
  },
  newsletterTitle: {
    ...Fonts.style.title.h8,
    color: Colors.coal,
    fontWeight: 'bold',
    marginTop: Metrics.baseMargin / 3,
    marginBottom: -Metrics.baseMargin / 3
  },
  newsletterDate: {
    ...Fonts.style.body.p7,
    color: Colors.charcoal,
    fontWeight: 'bold'
  },
  spinner:{
    color: Colors.masteryOrange,
  },
  loaderContainer:{
    flex:1,
    justifyContent: 'center',
    flexDirection: 'row',
    padding: 10
  },
  loaderContainer2:{
    flex:1,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 10,
    position: 'absolute',
    zIndex: 100,
    backgroundColor: 'white',
    width: "100%",
    height: "100%",
    opacity: 0.6
  },
  videobox: {
    backgroundColor: '#ecf0f1',
    borderRadius:10,
    flex:1,
    flexDirection:"column",
    justifyContent:"center",
    width: Dimensions.get('screen').width / 2 * 0.75,
    height: Dimensions.get('screen').width / 2 * 0.75
  },
  docbox: {
    backgroundColor: '#ecf0f1',
    borderRadius:10,
    flex:1,
    flexDirection:"column",
    justifyContent:"center",
    width: Dimensions.get('screen').width / 2 * 0.75,
    height: Dimensions.get('screen').width / 2 * 0.75
  },
  audiobox: {
    backgroundColor: '#ecf0f1',
    borderRadius:10,
    flex:1,
    flexDirection:"column",
    justifyContent:"center",
    width: Dimensions.get('screen').width / 2 * 0.75,
    height: Dimensions.get('screen').width / 2 * 0.75
  },
  
})
