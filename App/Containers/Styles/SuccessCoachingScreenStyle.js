import { StyleSheet, Dimensions } from 'react-native'
import { ApplicationStyles, Colors, Metrics, Fonts } from '../../Themes'

export default StyleSheet.create({

    container: {

        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Colors.masteryBlue,
    },
    RectangleShapeView: {

        width: 175 * 2,
        height: 500,
        backgroundColor: '#fff'
    },
    menuImage: {
        resizeMode: 'contain',
        width: Metrics.countWidthBased(Metrics.countWidthScala(200)),
        marginLeft: 40,
        marginTop: -80,
    },
    welcomeText: {
        ...Fonts.style.title.h1,
        color: Colors.white,
        fontWeight:'bold',
        textAlign:"center",
        marginTop: 40,
        fontSize: 30,        
    },
    welcomeTextBold1: {
        ...Fonts.style.title.h1,
        color: Colors.white,
        textAlign:"justify",
        justifyContent: "center",
        marginTop: 7,
        fontSize: 13,
        marginLeft:30,
        marginRight:30,
    },
    welcomeTextBold2: {
      ...Fonts.style.title.h1,
      color: Colors.white,
      fontWeight: 'bold',
      textAlign:"center",
      marginTop: -8,
      fontSize: 35,
  },
    loginButton: {
        width: Dimensions.get('screen').width / 1.2,
        height: Metrics.countHeightBased(Metrics.countHeightScala(50)),
        borderWidth: 1,
        borderRadius: 15,
        borderColor: Colors.masteryOrange,
        backgroundColor: Colors.masteryOrange,
        alignSelf: 'center',
        justifyContent: 'center',
        marginTop: 140,
        marginBottom: -100,
      },
      loginText: {
        ...Fonts.style.subtitle.h1,
        color: Colors.white,
        alignSelf: 'center',
        fontWeight: 'bold'
      },

});
