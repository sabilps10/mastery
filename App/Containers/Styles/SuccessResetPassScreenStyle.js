import { StyleSheet, Dimensions } from 'react-native'
import { ApplicationStyles, Colors, Metrics, Fonts } from '../../Themes'

export default StyleSheet.create({

    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Colors.masteryBlue,
    },
    RectangleShapeView: {

        width: 175 * 2,
        height: 500,
        backgroundColor: '#fff'
    },
    menuImage: {
        resizeMode: 'contain',
        width: Metrics.countWidthBased(Metrics.countWidthScala(250)),
        marginLeft: 20,
        marginTop: -60,
    },
    welcomeTextBold: {
        ...Fonts.style.title.h1,
        color: Colors.white,
        fontWeight:'bold',
        textAlign:"center",
        marginTop: -40,
        fontSize: 30,        
    },
    welcomeText1: {
        ...Fonts.style.title.h1,
        color: Colors.white,
        textAlign:"justify",
        justifyContent: "center",
        marginTop: 7,
        fontSize: 17,
        marginLeft:30,
        marginRight:30,
    },
    welcomeText2: {
      ...Fonts.style.title.h1,
      color: Colors.white,
      textAlign:"center",
      marginTop: -3,
      fontSize: 17,
  },
    loginButton: {
        width: Dimensions.get('screen').width / 1.2,
        height: Metrics.countHeightBased(Metrics.countHeightScala(50)),
        borderWidth: 1,
        borderRadius: 15,
        borderColor: Colors.masteryOrange,
        backgroundColor: Colors.masteryOrange,
        alignSelf: 'center',
        justifyContent: 'center',
        marginTop: 100,
        marginBottom: -10,
      },
      loginText: {
        ...Fonts.style.subtitle.h1,
        color: Colors.white,
        alignSelf: 'center',
        fontWeight: 'bold'
      },

});
