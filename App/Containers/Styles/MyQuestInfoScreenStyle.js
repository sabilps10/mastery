import { StyleSheet, Dimensions } from 'react-native'
import { ApplicationStyles, Fonts, Colors, Metrics } from '../../Themes'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  container: {
		flex: 1
	},
	headerContainer: {
		// paddingTop: Metrics.navBarHeight,
		backgroundColor: Colors.masteryBlue,
		height: Metrics.navBarHeight
	},
  contentContainer: {
    marginTop: Metrics.doubleBaseMargin * 0,
    marginHorizontal: Metrics.doubleBaseMargin * 2
  },
  contentTitleText: {
    ...Fonts.style.title.h2,
    color: Colors.coal,
    fontWeight: 'bold'
  },
  contentTitleDate: {
    ...Fonts.style.title.h5,
    color: Colors.coal,
    marginTop: Metrics.baseMargin,
    marginBottom: Metrics.doubleBaseMargin
  },
  contentSubContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: Dimensions.get('screen').width * 0.8
  },
  contentSubPointContainer: {
    flexDirection: 'row'
  },
  contentSubText: {
    ...Fonts.style.body.p4,
    color: Colors.coal,
    marginRight: Metrics.doubleBaseMargin
  },
  contentSubDateText: {
    ...Fonts.style.body.p4,
    color: Colors.coal,
    fontWeight: 'bold',
    textAlign: 'left'
  },
  contentPointValueText: {
    ...Fonts.style.body.p3,
    color: Colors.masteryOrange,
    fontWeight: 'bold',
    textAlign: 'left',
    marginLeft: Metrics.baseMargin / 2
  },
  contentDescriptionContainerTop: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: Dimensions.get('screen').width * 0.8,
    marginTop: 10,
    marginBottom: 20
  },
  contentDescriptionContainerBottom: {
    alignItems: "flex-start",
    // marginTop: -20,
    marginBottom: 10
  },
  contentDescriptionContainerVideo: {
    alignItems: "center",
    justifyContent: 'space-between'
  },
  ticketsContainer: {
    marginTop: 0,
    alignSelf: 'center'
  },
  coachingVideoContainer: {
    // marginTop: -50
  },
  ticketComponent: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 20,
  },
  welcomeTextBoldBottom: {
    ...Fonts.style.title.h2,
    color: Colors.masteryBlue,
    fontWeight: 'bold',
    fontSize: 13,
    marginTop:120,
    textAlign:"justify",
    // fontFamily: "Montserrat-Black",
  },
  welcomeTextBoldVideo: {
    color: Colors.masteryBlue,
    fontWeight: 'bold',
    fontSize: 18,
    marginTop:10,
    marginBottom: 10,
    textAlign:"justify",
    alignSelf: 'center'
    // fontFamily: "Montserrat-Black",
  },
  videobox: {
    backgroundColor: '#ecf0f1',
    borderRadius: 10,
    width: Dimensions.get('screen').width / 2 * 1.6,
    height: Dimensions.get('screen').width / 2 * 0.75,
    marginTop: 10
  },
  thumbnail: {
    // resizeMode: 'cover',
    borderRadius: 10,
    width: Dimensions.get('screen').width / 2 * 1.6,
    height: Dimensions.get('screen').width / 2 * 0.75,
  },
  contentDescriptionText: {
    ...Fonts.style.body.p6,
    color: Colors.coal,
    marginTop: Metrics.doubleBaseMargin * 2,
    marginBottom: Metrics.doubleBaseMargin * 2
  },
  contentInstructionText: {
    ...Fonts.style.body.p3,
    color: Colors.coal,
    fontWeight: 'bold',
    marginTop: Metrics.baseMargin
  },
  contentInstructionContainer: {
    flexDirection: 'row',
    marginTop: Metrics.baseMargin / 2,
    justifyContent: 'space-between'
  },
  spinner: {
    color: Colors.masteryOrange,
  },
  loaderContainer: {
    flex: 1,
    justifyContent: 'center',
    flexDirection: 'row',
    justifyContent: 'space-around',
    padding: 10
  },
  contentInstructionDetailText: {
    ...Fonts.style.body.p3,
    color: Colors.charcoal,
    //fontWeight: 'bold'
  },
  bottomBarContainer: {
    flexDirection: 'row',
    width: Dimensions.get('screen').width * 0.9,
    height: Metrics.countHeightBased(Metrics.countHeightScala(45)),
    borderWidth: 1,
    borderRadius: 15,
    borderColor: Colors.masteryOrange,
    backgroundColor: Colors.masteryOrange,
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'space-evenly',
    marginBottom: Metrics.doubleBaseMargin
  },
  bottomBarImageContainer: {
    width: Metrics.countWidthBased(Metrics.countWidthScala((100 / 2.5))) * 1.75,
    height: Metrics.countHeightBased(Metrics.countHeightScala((68 / 2.5))),
    alignItems: 'center',
    marginBottom: Metrics.baseMargin / 2
  },
  bottomBarImage: {
    alignSelf: 'center',
    resizeMode: 'contain',
    width: Metrics.countWidthBased(Metrics.countWidthScala((60 / 2.5))),
    height: Metrics.countHeightBased(Metrics.countHeightScala((60 / 2.5)))
  },
  iconTitle: {
    ...Fonts.style.body.p10,
    color: Colors.coal,
    textAlign: 'center'
  },
  bottomBarImageDeactivated: {
    alignSelf: 'center',
    resizeMode: 'contain',
    width: Metrics.countWidthBased(Metrics.countWidthScala((60 / 2.5))),
    height: Metrics.countHeightBased(Metrics.countHeightScala((60 / 2.5))),
    tintColor: Colors.charcoal
  },
  iconTitleDeactivated: {
    ...Fonts.style.body.p10,
    color: Colors.charcoal,
    textAlign: 'center'
  },
  actionContainer: {
    marginTop: Metrics.baseMargin * 1,
    borderTopColor: "#cccccc",
    borderTopWidth: 1,
    paddingTop: Metrics.baseMargin * 2
  }
})
