import { StyleSheet, Dimensions } from 'react-native'
import { ApplicationStyles, Colors, Metrics, Fonts } from '../../Themes'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  loginContainer: {
    flex: 1,
    justifyContent: 'center',
		paddingTop: Metrics.navBarHeight
  },
  background: {
    position: 'absolute',
    width: 450,
    height: 2000,
    backgroundColor: Colors.masteryBlue
  },
  containerInputStyle: {
    marginTop: Metrics.countHeightBased(Metrics.countHeightScala(20)),
    marginHorizontal: Metrics.countWidthBased(Metrics.countWidthScala(Metrics.horizontalMargin)) * 3,
    marginLeft: Metrics.countWidthBased(Metrics.countWidthScala(60)),
    left: 30
  },
  loginButton: {
    width: Dimensions.get('screen').width / 1.75,
    height: Metrics.countHeightBased(Metrics.countHeightScala(40)),
    borderWidth: 1,
    borderRadius: 15,
    borderColor: Colors.masteryOrange,
    backgroundColor: Colors.masteryOrange,
    alignSelf: 'center',
    justifyContent: 'center',
    marginTop: 80,
    marginBottom: 80
  },
  loginText: {
    ...Fonts.style.subtitle.h2,
    color: Colors.white,
    alignSelf: 'center',
    fontWeight: 'bold'
  },
  welcomeTextConatiner: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: Metrics.baseMargin,
  },
  welcomeTextBold1: {
    marginTop:50,
    ...Fonts.style.title.h2,
    color: Colors.white,
    fontWeight: 'bold'
  },
  welcomeTextBold2: {
    marginTop:80,
    ...Fonts.style.title.h2,
    color: Colors.white,
    fontWeight: 'bold'
  },
  userIcon: {
    position: 'absolute',
    resizeMode: 'contain',
    tintColor: Colors.masteryOrange,
    width: Metrics.countWidthBased(Metrics.countWidthScala(40)),
    height: Metrics.countHeightBased(Metrics.countHeightScala(40)),
    left: Metrics.countWidthBased(Metrics.countWidthScala(40)),
    bottom: Dimensions.get('screen').height / 0.713 + 10 
  },
  locationIcon: {
    position: 'absolute',
    resizeMode: 'contain',
    width: Metrics.countWidthBased(Metrics.countWidthScala(40)),
    height: Metrics.countHeightBased(Metrics.countHeightScala(40)),
    left: Metrics.countWidthBased(Metrics.countWidthScala(40)),
    bottom: Dimensions.get('screen').height / 0.713 + 10 - Metrics.countHeightBased(Metrics.countHeightScala(80)),
  },
  emailIcon: {
    position: 'absolute',
    resizeMode: 'contain',
    width: Metrics.countWidthBased(Metrics.countWidthScala(40)),
    height: Metrics.countHeightBased(Metrics.countHeightScala(40)),
    left: Metrics.countWidthBased(Metrics.countWidthScala(40)),
    bottom: Dimensions.get('screen').height / 0.775 + 10 - Metrics.countHeightBased(Metrics.countHeightScala(90)),
  },
  telephoneIcon: {
    position: 'absolute',
    resizeMode: 'contain',
    width: Metrics.countWidthBased(Metrics.countWidthScala(40)),
    height: Metrics.countHeightBased(Metrics.countHeightScala(40)),
    left: Metrics.countWidthBased(Metrics.countWidthScala(40)),
    bottom: Dimensions.get('screen').height / 0.858 + 10 - Metrics.countHeightBased(Metrics.countHeightScala(90)),
  },
  usernameIcon: {
    position: 'absolute',
    resizeMode: 'contain',
    tintColor: Colors.masteryOrange,
    width: Metrics.countWidthBased(Metrics.countWidthScala(40)),
    height: Metrics.countHeightBased(Metrics.countHeightScala(40)),
    left: Metrics.countWidthBased(Metrics.countWidthScala(40)),
    bottom: Dimensions.get('screen').height / 1.12 + 10 - Metrics.countHeightBased(Metrics.countHeightScala(90)),
  },
  useremailIcon: {
    position: 'absolute',
    resizeMode: 'contain',
    width: Metrics.countWidthBased(Metrics.countWidthScala(40)),
    height: Metrics.countHeightBased(Metrics.countHeightScala(40)),
    left: Metrics.countWidthBased(Metrics.countWidthScala(40)),
    bottom: Dimensions.get('screen').height / 1.305 + 10 - Metrics.countHeightBased(Metrics.countHeightScala(90)),
  },
  phoneIcon: {
    position: 'absolute',
    resizeMode: 'contain',
    width: Metrics.countWidthBased(Metrics.countWidthScala(40)),
    height: Metrics.countHeightBased(Metrics.countHeightScala(40)),
    left: Metrics.countWidthBased(Metrics.countWidthScala(40)),
    bottom: Dimensions.get('screen').height / 1.55 + 10 - Metrics.countHeightBased(Metrics.countHeightScala(90)),
  },
  lockpassIcon: {
    position: 'absolute',
    resizeMode: 'contain',
    tintColor: Colors.masteryOrange,
    width: Metrics.countWidthBased(Metrics.countWidthScala(40)),
    height: Metrics.countHeightBased(Metrics.countHeightScala(40)),
    left: Metrics.countWidthBased(Metrics.countWidthScala(40)),
    bottom: Dimensions.get('screen').height / 1.927 + 10 - Metrics.countHeightBased(Metrics.countHeightScala(90)),
  },
  lockconfirmIcon: {
    position: 'absolute',
    resizeMode: 'contain',
    tintColor: Colors.masteryOrange,
    width: Metrics.countWidthBased(Metrics.countWidthScala(40)),
    height: Metrics.countHeightBased(Metrics.countHeightScala(40)),
    left: Metrics.countWidthBased(Metrics.countWidthScala(40)),
    bottom: Dimensions.get('screen').height / 2.53 + 10 - Metrics.countHeightBased(Metrics.countHeightScala(90)),
  },
})
