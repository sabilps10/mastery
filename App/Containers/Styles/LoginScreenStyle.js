import { StyleSheet, Dimensions, KeyboardAvoidingView } from 'react-native'
import { ApplicationStyles, Colors, Metrics, Fonts } from '../../Themes'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  loginContainer: {
    flex: 1,
    justifyContent: 'center'
  },
  background: {
    position: 'absolute',
    width: Dimensions.get('screen').width,
    height: Dimensions.get('screen').height,
    backgroundColor: Colors.masteryBlue
  },
  containerInputStyle: {
    marginTop:-60,
    //marginTop: Metrics.countHeightBased(Metrics.countHeightScala(20)),
    marginHorizontal: Metrics.countWidthBased(Metrics.countWidthScala(Metrics.horizontalMargin)) * 3,
    marginLeft: Metrics.countWidthBased(Metrics.countWidthScala(60)),
    left: 30
  },
  loginButton: {
    width: Dimensions.get('screen').width / 1.75,
    height: Metrics.countHeightBased(Metrics.countHeightScala(40)),
    borderWidth: 1,
    borderRadius: 15,
    borderColor: Colors.masteryOrange,
    backgroundColor: Colors.masteryOrange,
    alignSelf: 'center',
    justifyContent: 'center',
    marginTop: Metrics.doubleBaseMargin
  },
  loginText: {
    ...Fonts.style.subtitle.h2,
    color: Colors.white,
    alignSelf: 'center',
    fontWeight: 'bold'
  },
  userIcon: {
   
    resizeMode: 'contain',
    width: Metrics.countWidthBased(Metrics.countWidthScala(40)),
    height: Metrics.countHeightBased(Metrics.countHeightScala(40)),
    left: Metrics.countWidthBased(Metrics.countWidthScala(40)),
   // bottom: Dimensions.get('screen').height / 1.73 + 10
  },
  lockIcon: {
    //position: 'absolute',
    resizeMode: 'contain',
    width: Metrics.countWidthBased(Metrics.countWidthScala(40)),
    height: Metrics.countHeightBased(Metrics.countHeightScala(40)),
    left: Metrics.countWidthBased(Metrics.countWidthScala(40)),
   // bottom: Dimensions.get('screen').height / 1.7 + 10 - Metrics.countHeightBased(Metrics.countHeightScala(90)),
  },
  register: {
    marginTop: 20,
    
  },
  registerText: {
    ...Fonts.style.subtitle.h2,
    color: '#c0c0c0',
    alignSelf: 'center',
    fontWeight: 'bold' 
  },
  password: {
    marginTop: 13,
  },
  passwordText: {
    ...Fonts.style.subtitle.h2,
    color: '#c0c0c0',
    alignSelf: 'center',
    fontWeight: 'bold' 
  },
})
