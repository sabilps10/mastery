import { StyleSheet, Dimensions } from 'react-native'
import { ApplicationStyles, Fonts, Colors, Metrics } from '../../Themes'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  container: {
    flex: 1,
		paddingTop: Metrics.navBarHeight
  },
	headerContainer: {
		// paddingTop: Metrics.navBarHeight,
		backgroundColor: Colors.masteryBlue,
		height: Metrics.navBarHeight
	},
  chatContainer: {
    marginTop: 261 * (Dimensions.get('screen').width / 768) / 4
  },
  messageContainer: {
    flexDirection: 'row',
    
    marginBottom: Metrics.baseMargin
  },
  messageNameContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    
  },
  chatAvatarImageBlack: {
    borderRadius:400,
    resizeMode: 'contain',
    width: Metrics.countWidthBased(Metrics.countWidthScala((108/2))),
    height: Metrics.countHeightBased(Metrics.countHeightScala((108/2))),
    
  },
  chatAvatarImageBlue: {
    resizeMode: 'contain',
    width: Metrics.countWidthBased(Metrics.countWidthScala((108/2))),
    height: Metrics.countHeightBased(Metrics.countHeightScala((108/2))),
    tintColor: 'rgba(0, 48, 164, 0.5)'
  },
  chatAvatarImagePink: {
    resizeMode: 'contain',
    width: Metrics.countWidthBased(Metrics.countWidthScala((108/2))),
    height: Metrics.countHeightBased(Metrics.countHeightScala((108/2))),
    tintColor: 'rgba(164, 0, 48, 0.5)'
  },
  messageTextContainer: {
    
    alignItems: 'flex-start'
  },
  messageNameText: {
    ...Fonts.style.subtitle.h3,
    color: Colors.coal,
    fontWeight: 'bold',
    marginTop: Metrics.baseMargin,
  },
  messagTimestampText: {
    ...Fonts.style.body.p6,
    color: 'rgba(150,150,150,0.75)',
    marginLeft: Metrics.baseMargin / 2,
    marginTop: Metrics.baseMargin,
  },
  messageContentText: {
    ...Fonts.style.body.p6,
    color: Colors.coal,
    width: Dimensions.get('screen').width * 0.8,
    textAlign: 'left',
    alignSelf: 'flex-start',
    marginBottom: Metrics.baseMargin / 2
  },
  messageContentImageDescription: {
    ...Fonts.style.body.p6,
    color: 'rgba(125,125,125,1)',
  },
  messageContentImage: {
    resizeMode: 'contain'
  },
  textInputContainer: {
    flexDirection: 'row',
    
    marginLeft: Metrics.doubleBaseMargin
  },
  chatImageContainer: {
    flex:1,
    flexDirection:"row",
    marginTop:8,
    
  },
  chatImage: {
    alignSelf: 'center',
    resizeMode: 'contain',
    width: Metrics.countWidthBased(Metrics.countWidthScala((272/3))),
    height: Metrics.countHeightBased(Metrics.countHeightScala((272/3)))
  },
  bottomBarContainer: {
    flexDirection: 'row',
    width: Dimensions.get('screen').width * 0.9,
    height: Metrics.countHeightBased(Metrics.countHeightScala(45)),
    borderWidth: 1,
    borderRadius: 15,
    borderColor: Colors.masteryOrange,
    backgroundColor: Colors.masteryOrange,
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'space-evenly',
    marginBottom: Metrics.doubleBaseMargin,
    marginTop: 10
  },
  bottomBarImageContainer: {
    width: Metrics.countWidthBased(Metrics.countWidthScala((100/2.5))) * 1.75,
    height: Metrics.countHeightBased(Metrics.countHeightScala((68/2.5))),
    alignItems: 'center',
    marginBottom: Metrics.baseMargin / 2
  },
  bottomBarImage: {
    alignSelf: 'center',
    resizeMode: 'contain',
    width: Metrics.countWidthBased(Metrics.countWidthScala((60/2.5))),
    height: Metrics.countHeightBased(Metrics.countHeightScala((60/2.5)))
  },
  iconTitle: {
    ...Fonts.style.body.p10,
    color: Colors.coal,
    textAlign: 'center'
  },
  bottomBarImageDeactivated: {
    alignSelf: 'center',
    resizeMode: 'contain',
    width: Metrics.countWidthBased(Metrics.countWidthScala((60/2.5))),
    height: Metrics.countHeightBased(Metrics.countHeightScala((60/2.5))),
    tintColor: Colors.charcoal
  },
  iconTitleDeactivated: {
    ...Fonts.style.body.p10,
    color: Colors.charcoal,
    textAlign: 'center'
  },
  menuContainer: {
    position: 'absolute',
    alignSelf: 'flex-end',
    alignItems: 'center',
    bottom: Dimensions.get('screen').height * 0.125,
    right: Metrics.doubleBaseMargin
  },
  iconImage: {
    resizeMode: 'contain',
    width: Metrics.countWidthBased(Metrics.countWidthScala((80/2))),
    height: Metrics.countHeightBased(Metrics.countHeightScala((80/2))),
    marginTop: Metrics.baseMargin
  },
  iconImageBig: {
    resizeMode: 'contain',
    width: Metrics.countWidthBased(Metrics.countWidthScala((150/2))),
    height: Metrics.countHeightBased(Metrics.countHeightScala((150/2))),
    marginTop: Metrics.baseMargin
  },
  lineBreak: {
    borderWidth: 1,
    borderColor: Colors.cloud,
    alignSelf: 'center',
    width: Dimensions.get('screen').width * 0.9
  }
})
