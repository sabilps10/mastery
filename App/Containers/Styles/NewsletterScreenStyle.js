import { StyleSheet, Dimensions } from 'react-native'
import { ApplicationStyles, Fonts, Colors, Metrics } from '../../Themes'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  rewardsContainer: {
    flex: 1,
		paddingTop: Metrics.navBarHeight
  },
  flatListContainer: {
    flex: 1,
    alignSelf: 'center',
    backgroundColor: 'transparent',
    width: Dimensions.get('screen').width,
    marginTop: 261 * (Dimensions.get('screen').width / 768) / 4 - 6.5
  },
  newsletterContainer: {
    alignSelf: 'center',
    justifyContent: 'center',
    marginTop: 261 * (Dimensions.get('screen').width / 768) / 4 - 6.5,
    marginLeft: Dimensions.get('screen').width / 2 * 0.125,
    marginRight: Dimensions.get('screen').width / 2 * 0.125
  },
  newsletterImage: {
    resizeMode: 'contain',
    width: Dimensions.get('screen').width / 2 * 0.75,
    height: Dimensions.get('screen').width / 2 * 0.75
  },
  newsletterTitle: {
    ...Fonts.style.title.h8,
    color: Colors.coal,
    fontWeight: 'bold',
    marginTop: Metrics.baseMargin / 3,
    marginBottom: -Metrics.baseMargin / 3
  },
  newsletterDate: {
    ...Fonts.style.body.p7,
    color: Colors.charcoal,
    fontWeight: 'bold'
  },
  ticketContainer: {
    marginLeft: Metrics.doubleBaseMargin
  },
  ticketTitle: {
    ...Fonts.style.title.h3,
    width: Dimensions.get('screen').width / 2,
    fontWeight: 'bold',
    color: Colors.coal,
    marginBottom: Metrics.baseMargin
  },
  ticketDescription: {
    ...Fonts.style.body.p5,
    width: Dimensions.get('screen').width / 2,
    fontWeight: 'bold',
    color: Colors.charcoal,
    marginBottom: Metrics.baseMargin
  },
  pointReedemContainer: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  pointText: {
    ...Fonts.style.title.h5,
    textAlign: 'center',
    color: Colors.masteryOrange,
    fontWeight: 'bold',
    marginRight: Metrics.baseMargin / 2
  },
  reedemButtonContainer: {
    alignSelf: 'center',
    alignItems: 'center',
    width: Metrics.countWidthBased(Metrics.countWidthScala(75)),
    height: Metrics.countHeightBased(Metrics.countHeightScala(25)),
    borderWidth: 1,
    borderRadius: 15,
    borderColor: Colors.masteryOrange,
    backgroundColor: Colors.masteryOrange,
    marginLeft: Metrics.baseMargin
  },
  reedemText: {
    ...Fonts.style.title.h8,
    color: Colors.white,
    alignSelf: 'center',
    justifyContent: 'center',
    fontWeight: 'bold',
    textAlign: 'center'
  }
})
