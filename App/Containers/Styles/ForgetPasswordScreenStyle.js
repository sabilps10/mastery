import { StyleSheet, Dimensions } from 'react-native'
import { ApplicationStyles, Colors, Metrics, Fonts } from '../../Themes'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  rewardsContainer: {
    flex: 1,
    justifyContent: 'center',
		paddingTop: Metrics.navBarHeight
  },
  loginContainer: {
    flex: 1,
    justifyContent: 'center'
  },
  background: {
    position: 'absolute',
    width: Dimensions.get('screen').width,
    height: Dimensions.get('screen').height,
    backgroundColor: Colors.masteryBlue
  },
  containerInputStyle: {
    marginTop: Metrics.countHeightBased(Metrics.countHeightScala(-20)),
    marginHorizontal: Metrics.countWidthBased(Metrics.countWidthScala(Metrics.horizontalMargin)) * 3,
    marginLeft: Metrics.countWidthBased(Metrics.countWidthScala(60)),
    left: 30
  },
  loginButton: {
    width: Dimensions.get('screen').width / 1.75,
    height: Metrics.countHeightBased(Metrics.countHeightScala(40)),
    borderWidth: 1,
    borderRadius: 15,
    borderColor: Colors.masteryOrange,
    backgroundColor: Colors.masteryOrange,
    alignSelf: 'center',
    justifyContent: 'center',
    marginTop: 10
  },
  loginText: {
    ...Fonts.style.subtitle.h2,
    color: Colors.white,
    alignSelf: 'center',
    fontWeight: 'bold'
  },
  emailIcon: {
    position: 'absolute',
    resizeMode: 'contain',
    width: Metrics.countWidthBased(Metrics.countWidthScala(40)),
    height: Metrics.countHeightBased(Metrics.countHeightScala(40)),
    left: Metrics.countWidthBased(Metrics.countWidthScala(40)),
    bottom: Dimensions.get('screen').height / 2.165 + 10
  },
})
