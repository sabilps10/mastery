import { StyleSheet, Dimensions } from 'react-native'
import { ApplicationStyles, Fonts, Colors, Metrics } from '../../Themes'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  rewardsContainer: {
    flex: 1
  },
  questButtonContainer: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    marginTop: Metrics.baseMargin
  },
  questButtonText: {
    ...Fonts.style.body.p4,
    color: Colors.white,
    fontWeight: 'bold'
  },
  questButtonTextDeactivated: {
    ...Fonts.style.body.p4,
    color: Colors.cloud,
    fontWeight: 'bold'
  },
  questsContainer: {
    marginTop: 261 * (Dimensions.get('screen').width / 768) / 4 - 6.5
  },
  questComponentContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginHorizontal: Metrics.doubleBaseMargin * 2,
    marginBottom: Metrics.doubleBaseMargin,
  },
  questIconContainer: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  questIcon: {
    resizeMode: 'contain',
    width: Metrics.countWidthBased(Metrics.countWidthScala(106 / 2)),
    height: Metrics.countHeightBased(Metrics.countHeightScala(106 / 2)),
  },
  questDescriptionContainer: {
    marginLeft: Metrics.baseMargin
  },
  questTitleText: {
    ...Fonts.style.title.h4,
    width: Dimensions.get('screen').width / 2,
    fontWeight: 'bold',
    color: Colors.coal
  },
  questDateText: {
    ...Fonts.style.body.p6,
    color: Colors.coal
  },
  expandButtonContainer: {
    marginLeft: Metrics.doubleBaseMargin
  },
  expandButtonImage: {
    resizeMode: 'contain',
    width: Metrics.countWidthBased(Metrics.countWidthScala(44 / 2)),
    height: Metrics.countHeightBased(Metrics.countHeightScala(38 / 2)),
  }
})
