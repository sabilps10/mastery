import { StyleSheet, Dimensions } from 'react-native'
import { ApplicationStyles, Colors, Metrics, Fonts } from '../../Themes'
import colors from '../../Themes/Colors'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  rewardsContainer: {
		flex: 1,
		backgroundColor: Colors.masteryBlue,
    justifyContent: 'center',
    width: Dimensions.get('screen').width,
    height: Dimensions.get('screen').height,
		paddingTop: Metrics.navBarHeight
  },
  loginContainer: {
    flex: 1,
    justifyContent: 'center',
    width: Dimensions.get('screen').width,
    height: Dimensions.get('screen').height,
  },
  background: {
    position: 'absolute',
    width: Dimensions.get('screen').width,
    height: Dimensions.get('screen').height,
    backgroundColor: colors.masteryBlue,
  },
  avatarImage: {
    resizeMode: 'cover',
    width: Metrics.countWidthBased(Metrics.countWidthScala((100))),
    height: Metrics.countHeightBased(Metrics.countHeightScala((100))),
    borderRadius: Metrics.countWidthBased(Metrics.countWidthScala((100))) / 2,
    marginTop: Metrics.doubleBaseMargin * 3
  },
  spinner: {
    color: Colors.masteryOrange,
  },
  loginButton: {
    width: 120,
    height: 40,
    borderWidth: 1,
    borderRadius: 15,
    borderColor: Colors.masteryBlue,
    backgroundColor: Colors.masteryOrange,
    alignSelf: 'center',
    justifyContent: 'center',
    marginTop: 50,
    marginLeft: 230,
  },
  loginText: {
    ...Fonts.style.subtitle.h2,
    color: Colors.masteryBlue,
    fontSize: 15,
    alignSelf: 'center',
    fontWeight: 'bold',
    marginTop: 18,
    marginLeft: -40,
  },
  menuImage: {
    resizeMode: 'contain',
    width: Metrics.countWidthBased(Metrics.countWidthScala(30)),
    marginLeft: 65,
    marginTop: -38,
  },
  containerInputStylename: {
    marginHorizontal: Metrics.countWidthBased(Metrics.countWidthScala(Metrics.horizontalMargin)) * 4,
    marginLeft: Metrics.countWidthBased(Metrics.countWidthScala(60)),
    left: 40,
    color: 'white',
    fontSize: 20,
    borderBottomColor: "white",
    borderBottomWidth: 0.2,
    paddingBottom: 8,
    marginTop: Metrics.doubleBaseMargin * 3
  },
  containerInputStyleemail: {
    marginHorizontal: Metrics.countWidthBased(Metrics.countWidthScala(Metrics.horizontalMargin)) * 4,
    marginLeft: Metrics.countWidthBased(Metrics.countWidthScala(60)),
    left: 40,
    color: 'white',
    fontSize: 20,
    borderBottomColor: "white",
    borderBottomWidth: 0.2,
    paddingBottom: 8,
    marginTop: Metrics.doubleBaseMargin * 2.5
  },
  containerInputStylephone: {
    marginHorizontal: Metrics.countWidthBased(Metrics.countWidthScala(Metrics.horizontalMargin)) * 4,
    marginLeft: Metrics.countWidthBased(Metrics.countWidthScala(60)),
    left: 40,
    color: 'white',
    fontSize: 20,
    borderBottomColor: "white",
    borderBottomWidth: 0.2,
    paddingBottom: 8,
    marginTop: Metrics.doubleBaseMargin * 2.5
  },
  usernameIcon: {
    position: 'absolute',
    resizeMode: 'contain',
    width: Metrics.countWidthBased(Metrics.countWidthScala(40)),
    height: Metrics.countHeightBased(Metrics.countHeightScala(40)),
    left: Metrics.countWidthBased(Metrics.countWidthScala(40)),
    marginTop: Metrics.doubleBaseMargin * 2.3
  },
  useremailIcon: {
    position: 'absolute',
    resizeMode: 'contain',
    width: Metrics.countWidthBased(Metrics.countWidthScala(40)),
    height: Metrics.countHeightBased(Metrics.countHeightScala(40)),
    left: Metrics.countWidthBased(Metrics.countWidthScala(40)),
    marginTop: Metrics.doubleBaseMargin * 1.8
  },
  phoneIcon: {
    position: 'absolute',
    resizeMode: 'contain',
    width: Metrics.countWidthBased(Metrics.countWidthScala(40)),
    height: Metrics.countHeightBased(Metrics.countHeightScala(40)),
    left: Metrics.countWidthBased(Metrics.countWidthScala(40)),
    marginTop: Metrics.doubleBaseMargin * 1.8
  },
})
