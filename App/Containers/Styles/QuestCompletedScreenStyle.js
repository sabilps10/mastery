import { StyleSheet, Dimensions } from 'react-native'
import { ApplicationStyles, Fonts, Colors, Metrics } from '../../Themes'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  message:{
    flex:1,
    flexDirection:'row',
    alignItems:'center',
    padding:20,
    justifyContent: 'space-evenly',
    textAlign:'center',
  },
  messageText:{
    fontWeight:'bold',
    textAlign:'center',
  },
  rewardsContainer: {
    flex: 1
  },
  questButtonContainer: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    marginTop: Metrics.baseMargin
  },
  questButtonText: {
    ...Fonts.style.body.p4,
    color: Colors.white,
    fontWeight: 'bold'
  },
  questButtonTextDeactivated: {
    ...Fonts.style.body.p4,
    color: Colors.cloud,
    fontWeight: 'bold'
  },
  questsContainer: {
    marginTop: Metrics.doubleBaseMargin * 2.5
  },
  questComponentContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    flex:1,
    justifyContent:'space-evenly',
    marginHorizontal: Metrics.doubleBaseMargin,
    marginBottom: Metrics.doubleBaseMargin * 0.5
  },
  questIconContainer: {
    flex:1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  questIcon: {
    resizeMode: 'contain',
    width: Metrics.countWidthBased(Metrics.countWidthScala(106 / 2)),
    height: Metrics.countHeightBased(Metrics.countHeightScala(106 / 2)),
  },
  questDescriptionContainer: {
    flex:3,
    marginLeft: Metrics.baseMargin
  },
  questTitleText: {
    ...Fonts.style.title.h5,
    width: Dimensions.get('screen').width / 2,
    fontWeight: 'bold',
    color: Colors.coal
  },
  questDateText: {
    ...Fonts.style.body.p6,
    color: Colors.coal
  },
  questPointsText: {
    ...Fonts.style.title.h4,
    flex:1,
    color: Colors.masteryOrange,
    fontWeight: 'bold',
    marginLeft: Metrics.baseMargin+20,
    textAlign: 'center'
  }
})
