import { StyleSheet, Dimensions } from 'react-native'
import { ApplicationStyles, Fonts, Colors, Metrics } from '../../Themes'

export default StyleSheet.create({
  ...ApplicationStyles.screen,

  message:{
    flex:1,
    flexDirection:'row',
    alignItems:'center',
    padding:20,
    justifyContent: 'space-evenly',
    textAlign:'center',
  },
  messageText:{
    fontWeight:'bold',
    textAlign:'center',
  },
  rewardsContainer: {
		flex: 1
	},
	headerContainer: {
		paddingTop: Metrics.navBarHeight,
		backgroundColor: Colors.masteryBlue
	},
  questButtonContainer: {
		flexDirection: 'row',
		justifyContent: 'space-evenly',
		marginTop: Metrics.baseMargin
  },
  questButtonText: {
    ...Fonts.style.body.p4,
    color: Colors.white,
    fontWeight: 'bold'
  },
  questButtonTextDeactivated: {
    ...Fonts.style.body.p4,
    color: Colors.cloud,
    fontWeight: 'bold'
  },
  questsContainer: {
    marginTop: Metrics.doubleBaseMargin * 2.5
  },
  questComponentContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginHorizontal: Metrics.doubleBaseMargin * 2,
    marginBottom: Metrics.doubleBaseMargin
  },
  questIconContainer: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  questIcon: {
    resizeMode: 'contain',
    width: Metrics.countWidthBased(Metrics.countWidthScala(106 / 2)),
    height: Metrics.countHeightBased(Metrics.countHeightScala(106 / 2)),
  },
  questDescriptionContainer: {
    marginLeft: Metrics.baseMargin,
    marginTop: Metrics.baseMargin
  },
  questTitleText: {
    ...Fonts.style.title.h5,
    width: Dimensions.get('screen').width / 2,
    fontWeight: 'bold',
    color: Colors.coal
  },
  questDateText: {
    ...Fonts.style.body.p6,
    color: Colors.coal
  },
  expandPanelContainer: {
    flex:1,
    flexDirection:'row'
  },
  expandButtonContainer: {
    marginLeft: Metrics.doubleBaseMargin
  },
  expandButtonImage: {
    resizeMode: 'contain',
    width: Metrics.countWidthBased(Metrics.countWidthScala(44 / 2)),
    height: Metrics.countHeightBased(Metrics.countHeightScala(38 / 2)),
  },
  questDetailContainer: {
    marginHorizontal: Metrics.doubleBaseMargin * 2,
    marginBottom: Metrics.doubleBaseMargin,
    marginTop: -Metrics.baseMargin,
    marginLeft: Metrics.countWidthBased(Metrics.countWidthScala(106)) - Metrics.baseMargin / 2
  },
  questDetailPointContainer: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  questDetailPointTitleText: {
    ...Fonts.style.body.p4,
    color: Colors.coal
  },
  questDetailPointValueText: {
    ...Fonts.style.body.p3,
    color: Colors.masteryOrange,
    fontWeight: 'bold'
  },
  questDetailDescriptionText: {
    ...Fonts.style.body.p6,
    marginTop: Metrics.baseMargin,
    marginBottom: Metrics.baseMargin
  },
  questDetailTakeQuestText: {
    ...Fonts.style.body.p2,
    color: Colors.coal,
    marginTop: Metrics.baseMargin
  },
  questDetailButtonContainer: {
    flexDirection: 'row',
    marginTop: Metrics.baseMargin / 2
  },
  questDetailButton: {
    backgroundColor: Colors.masteryOrange,
    borderRadius: 3,
    width: Metrics.screenWidth / 4,
    height: 30,
    paddingTop:4,
    alignItems: 'center'
  },
  questDetailButtonText: {
    ...Fonts.style.body.p2,
    color: Colors.white,
    // fontWeight: 'bold'
  }
})
