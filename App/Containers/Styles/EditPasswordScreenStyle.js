import { StyleSheet, Dimensions } from 'react-native'
import { ApplicationStyles, Colors, Metrics, Fonts } from '../../Themes'

export default StyleSheet.create({
    ...ApplicationStyles.screen,
    loginContainer: {
        flex: 1,
        backgroundColor: Colors.masteryBlue,
				paddingTop: Metrics.navBarHeight
    },
    background: {
        flex: 1,
        position: 'absolute',
        width: Dimensions.get('screen').width,
        height: Dimensions.get('screen').height,
        backgroundColor: Colors.masteryBlue,
    },
    inputContainer: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        marginHorizontal: Metrics.doubleBaseMargin * 2,
        marginTop: Metrics.countHeightBased(Metrics.countHeightScala(75))
    },
    containerInputStyle: {
        width: Dimensions.get('screen').width * 0.65
    },
    passButton: {
        width: Dimensions.get('screen').width / 1.75,
        height: Metrics.countHeightBased(Metrics.countHeightScala(40)),
        borderWidth: 1,
        borderRadius: 15,
        borderColor: Colors.masteryOrange,
        backgroundColor: Colors.masteryOrange,
        alignSelf: 'center',
        justifyContent: 'center',
        marginTop: 100,
        marginBottom: 25
    },
    loginButton: {
        width: Dimensions.get('screen').width / 1.75,
        height: Metrics.countHeightBased(Metrics.countHeightScala(40)),
        borderWidth: 1,
        borderRadius: 15,
        borderColor: Colors.masteryOrange,
        backgroundColor: Colors.masteryOrange,
        alignSelf: 'center',
        justifyContent: 'center',
        marginTop: 15,
        marginBottom: 80
    },
    loginText: {
        ...Fonts.style.subtitle.h2,
        color: Colors.white,
        alignSelf: 'center',
        fontWeight: 'bold'
    },
    inputIcon: {
        resizeMode: 'contain',
        width: Metrics.countWidthBased(Metrics.countWidthScala(40)),
        height: Metrics.countHeightBased(Metrics.countHeightScala(40)),
        marginRight: Metrics.baseMargin
    },
    passIcon: {
        position: 'absolute',
        resizeMode: 'contain',
        tintColor: Colors.masteryOrange,
        width: Metrics.countWidthBased(Metrics.countWidthScala(40)),
        height: Metrics.countHeightBased(Metrics.countHeightScala(40)),
        left: Metrics.countWidthBased(Metrics.countWidthScala(40)),
        bottom: Dimensions.get('screen').height / 1.31 + 10 - Metrics.countHeightBased(Metrics.countHeightScala(90)),
    },
    confirmIcon: {
        position: 'absolute',
        resizeMode: 'contain',
        tintColor: Colors.masteryOrange,
        width: Metrics.countWidthBased(Metrics.countWidthScala(40)),
        height: Metrics.countHeightBased(Metrics.countHeightScala(40)),
        left: Metrics.countWidthBased(Metrics.countWidthScala(40)),
        bottom: Dimensions.get('screen').height / 1.58 + 10 - Metrics.countHeightBased(Metrics.countHeightScala(90)),
    },
})
