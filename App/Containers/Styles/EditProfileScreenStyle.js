import { StyleSheet, Dimensions } from 'react-native'
import { ApplicationStyles, Colors, Metrics, Fonts } from '../../Themes'

export default StyleSheet.create({
    ...ApplicationStyles.screen,
    loginContainer: {
        flex: 1,
        backgroundColor: Colors.masteryBlue,
				paddingTop: Metrics.navBarHeight
    },
    containerInputStyle: {
        marginTop: Metrics.countHeightBased(Metrics.countHeightScala(20)),
        marginHorizontal: Metrics.countWidthBased(Metrics.countWidthScala(Metrics.horizontalMargin)) * 3,
        marginLeft: Metrics.countWidthBased(Metrics.countWidthScala(60)),
        left: 30,
        marginTop: 45,
    },
    photoContainer: {
        justifyContent: "center",
        alignItems: "center",
        marginBottom: Metrics.baseMargin,
        marginTop: -Metrics.doubleBaseMargin * 2
    },
    inputContainer: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        marginHorizontal: Metrics.doubleBaseMargin * 2,
        marginTop: Metrics.countHeightBased(Metrics.countHeightScala(75))
    },
    containerInputStyle: {
        width: Dimensions.get('screen').width * 0.65
    },
    avatarImage: {
        resizeMode: 'cover',
        width: Metrics.countWidthBased(Metrics.countWidthScala((100))),
        height: Metrics.countHeightBased(Metrics.countHeightScala((100))),
        borderRadius: Metrics.countWidthBased(Metrics.countWidthScala((100))) / 2,
        marginTop: Metrics.doubleBaseMargin * 4.5
      },
    passButton: {
        width: Dimensions.get('screen').width / 1.75,
        height: Metrics.countHeightBased(Metrics.countHeightScala(40)),
        borderWidth: 1,
        borderRadius: 15,
        borderColor: Colors.masteryOrange,
        backgroundColor: Colors.masteryOrange,
        alignSelf: 'center',
        justifyContent: 'center',
        marginTop: Metrics.countHeightBased(Metrics.countHeightScala((75))),
        marginBottom: 25
    },
    loginButton: {
        width: Dimensions.get('screen').width / 1.75,
        height: Metrics.countHeightBased(Metrics.countHeightScala(40)),
        borderWidth: 1,
        borderRadius: 15,
        borderColor: Colors.masteryOrange,
        backgroundColor: Colors.masteryOrange,
        alignSelf: 'center',
        justifyContent: 'center',
        marginTop: 15,
        marginBottom: 80
    },
    loginText: {
        ...Fonts.style.subtitle.h2,
        color: Colors.white,
        alignSelf: 'center',
        fontWeight: 'bold'
    },
    inputIcon: {
        resizeMode: 'contain',
        width: Metrics.countWidthBased(Metrics.countWidthScala(40)),
        height: Metrics.countHeightBased(Metrics.countHeightScala(40)),
        marginRight: Metrics.baseMargin
    },
    passIcon: {
        position: 'absolute',
        resizeMode: 'contain',
        tintColor: Colors.masteryOrange,
        width: Metrics.countWidthBased(Metrics.countWidthScala(40)),
        height: Metrics.countHeightBased(Metrics.countHeightScala(40)),
        left: Metrics.countWidthBased(Metrics.countWidthScala(40)),
        bottom: Dimensions.get('screen').height / 1.475 + 10 - Metrics.countHeightBased(Metrics.countHeightScala(90)),
    },
    confirmIcon: {
        position: 'absolute',
        resizeMode: 'contain',
        tintColor: Colors.masteryOrange,
        width: Metrics.countWidthBased(Metrics.countWidthScala(40)),
        height: Metrics.countHeightBased(Metrics.countHeightScala(40)),
        left: Metrics.countWidthBased(Metrics.countWidthScala(40)),
        bottom: Dimensions.get('screen').height / 1.885 + 10 - Metrics.countHeightBased(Metrics.countHeightScala(90)),
    },
    backgroundLoading: {
        flex: 1,
        backgroundColor: '#00000066',
        justifyContent: 'center',
        alignItems: 'center'
    }
})
