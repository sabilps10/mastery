import { StyleSheet, Dimensions } from 'react-native'
import { ApplicationStyles, Colors, Metrics, Fonts } from '../../Themes'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  HomeContainer: {
		flex: 1,
		paddingTop: Metrics.navBarHeight
  },
  welcomeTextBoldBottom: {
    ...Fonts.style.title.h2,
    color: Colors.masteryBlue,
    fontWeight: 'bold',
    fontSize: 13,
    marginTop:30,
    textAlign:"justify",
    // fontFamily: "Montserrat-Black",
  },
  ticketComponent: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },

  background: {
    position: 'absolute',
    width: Dimensions.get('screen').width,
    height: Dimensions.get('screen').height,
    backgroundColor: Colors.masteryBlue
  },
  menuButton: {
    marginTop: Metrics.baseMargin + 10,
    marginLeft: Metrics.baseMargin + 10,
    width: Metrics.countWidthBased(Metrics.countWidthScala(30)),
  },
  menuImage: {
    resizeMode: 'contain',
    width: Metrics.countWidthBased(Metrics.countWidthScala(30))
  },
  cp: {
    resizeMode: 'contain',
    width: "100%",
    height: "100%"
  },
  detailProgrammeContainer: {
    position: 'absolute',
    bottom: 10
  },
  ticketsContainer: {
    marginTop: 100 * (Dimensions.get('screen').width / 768) / 4 - 6.5
  },
  TextTop: {
    fontSize: 16,
    fontWeight: "bold",
    color: "white",
    marginLeft: Metrics.baseMargin * 2,
    marginTop: Metrics.baseMargin * 2.5,
    marginBottom: Metrics.baseMargin * 1.5,
  },
  loginButton: {
    width: Dimensions.get('screen').width / 1.85,
    height: Dimensions.get('screen').height / 22,
    borderRadius: 13,
    backgroundColor: "white",
    alignSelf: 'center',
    justifyContent: 'flex-end',
    marginTop: Metrics.baseMargin * 1,
    marginLeft: Metrics.baseMargin * 9.5,
    marginRight: Metrics.doubleBaseMargin,
    padding: 8
  },
  searchImage: {
    resizeMode: 'contain',
    width: 30,
    height: 30,
    right: Metrics.doubleBaseMargin * 3,
    top: Metrics.doubleBaseMargin
  },
  TextMid: {
    fontSize: 16,
    fontWeight: "bold",
    color: "white",
    marginLeft: Metrics.baseMargin * 2,
    marginTop: Metrics.baseMargin * 3,
    marginBottom: Metrics.baseMargin * 1,
  },
  TextBot: {
    fontSize: 16,
    fontWeight: "bold",
    color: "white",
    marginLeft: Metrics.baseMargin * 2,
    marginTop: Metrics.baseMargin * 3,
    marginBottom: Metrics.baseMargin * 0,
  },
  box: {
    alignItems: 'flex-start',
    justifyContent: 'center',
    backgroundColor: '#f3f3f3',
    borderRadius: 10,
    borderColor: "#f3f3f3",
    width: '90%',
    height: "80%",
    shadowColor: '#000',
    shadowOffset: { width: 3, height: 4 },
    shadowOpacity: 25,
    shadowRadius: 4,
    elevation: 3,
    paddingLeft: 30,
  },
  box1: {

  },
  row: {
    height: Metrics.baseMargin * 3,
    marginTop: Metrics.baseMargin * 1,
    marginBottom: Metrics.baseMargin * 1,
    // backgroundColor: "#171A21",
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  row1: {
    height: Metrics.baseMargin * 10,
    marginTop: Metrics.baseMargin * -1,
    // backgroundColor: "#171A21",
    alignItems: 'center',
    justifyContent: 'center',
  },
  row2: {
    resizeMode: 'contain',
    marginTop: Metrics.baseMargin * 0,
    // backgroundColor: "#171A21",
    alignItems: 'center',
    justifyContent: 'center',
  },
  row3: {
    height: Metrics.baseMargin * 15,
    marginTop: Metrics.baseMargin * 0,
    marginBottom: Metrics.baseMargin * 4,
    // backgroundColor: "#171A21",
    alignItems: 'center',
    justifyContent: 'center',
  },
  box1: {
    alignItems: 'flex-start',
    justifyContent: 'center',
    borderRadius: 3,
    borderColor: "#f3f3f3",
    // width: '95%',
    // height: "100%",
    shadowColor: '#f3f3f3',
    shadowOpacity: 3,
    elevation: 1,
    paddingTop: 3,
    paddingLeft: 2,
    paddingRight: 2,
    paddingBottom: 3
  },
  Title: {
    fontSize: 17,
    color: '#ffffcc',
    marginLeft: Metrics.baseMargin * 2,
    marginTop: Metrics.baseMargin * 5.5,
    textShadowColor: 'black',
    textShadowRadius: 40
  },
  Coach: {
    fontSize: 28,
    fontWeight: "bold",
    letterSpacing: 1,
    color: '#ffffcc',
    marginLeft: Metrics.baseMargin * 2,
    marginTop: Metrics.baseMargin * -0.7,
    textShadowColor: 'black',
    textShadowRadius: 40
  },
  Price: {
    fontSize: 17,
    color: '#ffffcc',
    marginLeft: Metrics.baseMargin * 2,
    marginTop: Metrics.baseMargin * -0.5,
    textShadowColor: 'black',
    textShadowRadius: 40
  },
})
