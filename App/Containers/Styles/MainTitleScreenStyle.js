import { StyleSheet, Dimensions } from 'react-native'
import { ApplicationStyles, Fonts, Colors, Metrics } from '../../Themes'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  rewardsContainer: {
    flex: 1,
  },
  buttonBackContainer: {
    zIndex: 10,
    position: 'absolute',
    width: Metrics.countWidthBased(Metrics.countWidthScala(75)),
    height: Metrics.countHeightBased(Metrics.countHeightScala(37.5)),
		marginTop: Metrics.countWidthBased(Metrics.countWidthScala(10)) + Metrics.navBarHeight,
  },
  buttonBack: {
    resizeMode: 'contain',
    marginLeft: Metrics.baseMargin * 1.5,
    width: 35,
    height:40
  },
  welcomeTextBold1: {
    ...Fonts.style.title.h2,
    color: "black",
    fontWeight: 'bold',
    fontSize: 28,
    position: 'absolute',
    bottom: Metrics.doubleBaseMargin,
    left: Metrics.doubleBaseMargin,
    textAlign: "left",
    // fontFamily: "Montserrat-Black",
  },
  spinner: {
    color: Colors.masteryOrange,
  },
  welcomeTextBold2: {
    ...Fonts.style.title.h2,
    color: "black",
    fontWeight: '400',
    fontSize: 18,
    position: 'absolute',
    bottom: -70,
    width: Dimensions.get('screen').width,
    height: 50,
    left: Metrics.doubleBaseMargin,
    textAlign: "left",
    // fontFamily: "Montserrat-Black",
    marginTop: Metrics.doubleBaseMargin * 0,
    marginBottom: Metrics.doubleBaseMargin * 2
  },
  welcomeTextBold: {
    ...Fonts.style.title.h2,
    color: "black",
    fontWeight: 'normal',
    fontSize: Metrics.baseMargin * 1.2,
    marginLeft: Metrics.baseMargin * 3,
    marginRight: Metrics.baseMargin * 2.3,
    textAlign: "left",
    // fontFamily: "Montserrat-Black",
    lineHeight: 20
  },
  welcomeText: {
    ...Fonts.style.title.h2,
    color: "black",
    fontWeight: 'bold',
    fontSize: Metrics.baseMargin * 1.5,
    marginLeft: Metrics.baseMargin * 3,
    marginRight: Metrics.baseMargin * 2,
    marginTop: Metrics.baseMargin * 1,
    textAlign: "left",
    // fontFamily: "Montserrat-Black",
  },
  questTitleText: {
    ...Fonts.style.title.h2,
    color: "black",
    fontWeight: 'bold',
    fontSize: Metrics.baseMargin * 1.8,
    marginLeft: 20,
    marginRight: 30,
    textAlign: "left",
    // fontFamily: "Montserrat-Black",
  },
  questText: {
    ...Fonts.style.title.h2,
    color: "black",
    fontWeight: 'bold',
    fontSize: Metrics.baseMargin * 1.8,
    // position: 'absolute',
    // bottom: 10,
    marginLeft: Metrics.doubleBaseMargin,
    marginTop: Metrics.baseMargin,
    textAlign: "left",
    // fontFamily: "Montserrat-Black",
  },
  descriptionText: {
    ...Fonts.style.title.h2,
    color: "black",
    fontSize: Metrics.baseMargin * 1.3,
    // position: 'absolute',
    // top: -5,
    marginLeft: Metrics.doubleBaseMargin,
    marginTop: Metrics.baseMargin,
    // marginLeft: 30,
    // marginRight: 30,
    textAlign: "left",
    // fontFamily: "Montserrat-Black",
  },
  priceText: {
    ...Fonts.style.title.h2,
    color: "red",
    fontWeight: 'bold',
    fontSize: Metrics.baseMargin * 1.5,
    textAlign: "center",
    // fontFamily: "Montserrat-Black",
    marginBottom: 10,
    marginRight: 10
  },
  InActiveText: {
    ...Fonts.style.title.h2,
    color: "red",
    fontWeight: 'bold',
    fontSize: Metrics.baseMargin * 1.7,
    textAlign: "center",
    // fontFamily: "Montserrat-Black",
    marginRight:Metrics.baseMargin * 14,
    marginTop: 5
  },
  priceText: {
    ...Fonts.style.title.h2,
    color: "red",
    fontWeight: 'bold',
    fontSize: Metrics.baseMargin * 1.5,
    textAlign: "center",
    // fontFamily: "Montserrat-Black",
    marginBottom: 10,
    marginRight: 10
  },
  jamImage: {
    resizeMode: 'contain',
    width: 30,
    height: 30,
    marginLeft:Metrics.baseMargin * 14,
  },
  contentDescriptionContainerVideo: {
    alignItems: "center",
    justifyContent: 'space-between',
    marginTop: 50
  },
  contentDescriptionContainerTop: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: Dimensions.get('screen').width * 0.9,
    marginTop: 10,
    marginBottom: 20,
    marginLeft: 20,
    marginRight: 20
  },
  contentDescriptionContainerBottom: {
    alignItems: "flex-start",
    marginTop: 70,
  },
  contentDescriptionContainer: {
    alignItems: 'flex-start',
    // justifyContent: 'center',
    // height: 80,
    borderRadius: 10,
    borderColor: "#DFC390",
    width: '90%',
    shadowColor: '#000',
    shadowOffset: { width: 5, height: 5 },
    shadowOpacity: 25,
    shadowRadius: 4,
    elevation: 3,
    backgroundColor: "#f3f3f3",
    marginLeft: 20,
    marginRight: 20,
    marginBottom: 15
  },
  videobox: {
    backgroundColor: '#ecf0f1',
    borderRadius: 20,
    width: Dimensions.get('screen').width / 2 * 1.65,
    height: Dimensions.get('screen').width / 2 * 0.75,
    marginTop: -30
  },
  loginButton: {
    width: Dimensions.get('screen').width / 1.13,
    height: Metrics.countHeightBased(Metrics.countHeightScala(40)),
    borderWidth: 1,
    borderRadius: 15,
    borderColor: Colors.masteryOrange,
    backgroundColor: Colors.masteryOrange,
    alignSelf: 'center',
    justifyContent: 'center',
    marginTop: 10,
    marginBottom: 30
  },
  login2Button: {
    width: Dimensions.get('screen').width / 1.13,
    height: Metrics.countHeightBased(Metrics.countHeightScala(40)),
    borderWidth: 1,
    borderRadius: 15,
    borderColor: Colors.masteryOrange,
    backgroundColor: Colors.masteryOrange,
    alignSelf: 'center',
    justifyContent: 'center',
    marginTop: 30,
    marginBottom: 30
  },
  loginText: {
    ...Fonts.style.subtitle.h1,
    color: Colors.white,
    alignSelf: 'center',
    fontWeight: 'bold'
  },
  welcomeTextBoldVideo: {
    ...Fonts.style.title.h2,
    color: Colors.masteryOrange,
    fontWeight: 'bold',
    fontSize: 15,
    marginTop:0,
    textAlign:"justify",
    paddingLeft: 3,
    paddingRight: 3,
    letterSpacing: -1
    // fontFamily: "Montserrat-Black",
  },
  welcomeTextBoldBottom: {
    ...Fonts.style.title.h2,
    color: Colors.masteryBlue,
    fontWeight: 'bold',
    fontSize: 13,
    marginTop:30,
    textAlign:"justify",
    // fontFamily: "Montserrat-Black",
  },
  welcomeTextBoldBottomQuest: {
    ...Fonts.style.title.h2,
    color: Colors.masteryOrange,
    fontWeight: 'bold',
    fontSize: 17,
    marginTop:10,
    textAlign:"justify",
    // fontFamily: "Montserrat-Black",
  },
  ticketsContainer: {
    marginTop: 1 * (Dimensions.get('screen').width / 768) / 4 - 6.5
  },
  ticketComponent: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 330,
  },
  ticketComponentVid: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 30,
    marginTop: 20,
    paddingLeft: 40, 
    paddingRight: 40
  },
  ticketComponentBot: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 30
  },
  ticketComponentContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 90,
  },
  ticketImage: {
    resizeMode: 'cover',
    width: "100%",
    height: Dimensions.get('screen').height / 1.5,
    marginTop: Metrics.doubleBaseMargin * -4.5,
    marginBottom: Metrics.doubleBaseMargin * 1.5,
  },
  blur: {
    position: 'absolute',
    width: '100%',
    height: 200,
    bottom: 0
  },
  ticketContainer: {
    marginLeft: 15,
    marginRight: 20,
    marginTop: -110,
  },
  ticketTitle: {
    ...Fonts.style.title.h5,
    width: Dimensions.get('screen').width / 2,
    fontWeight: 'bold',
    color: Colors.masteryBlue,
    fontSize: 16,
    textAlign: "left",
  },
  ticketDescription: {
    ...Fonts.style.body.p7,
    width: Dimensions.get('screen').width / 2,
    fontWeight: 'bold',
    color: Colors.charcoal,
    fontSize: 12,
    textAlign: "left",
  },
  pointReedemContainer: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  pointText: {
    ...Fonts.style.title.h5,
    textAlign: 'center',
    color: Colors.masteryOrange,
    fontWeight: 'bold',
    marginRight: Metrics.baseMargin / 2
  },
  reedemButtonContainer: {
    alignSelf: 'center',
    alignItems: 'center',
    width: Metrics.countWidthBased(Metrics.countWidthScala(75)),
    height: Metrics.countHeightBased(Metrics.countHeightScala(25)),
    borderWidth: 1,
    borderRadius: 15,
    borderColor: Colors.masteryOrange,
    backgroundColor: Colors.masteryOrange,
    marginLeft: Metrics.baseMargin
  },
  reedemText: {
    ...Fonts.style.title.h8,
    color: Colors.white,
    alignSelf: 'center',
    justifyContent: 'center',
    fontWeight: 'bold',
    textAlign: 'center'
  },
  containerVideo: {
    flex: 1,
    position: "absolute",
    width: "100%",
    height: "100%"
  },
  thumbnail: {
    backgroundColor: '#ecf0f1',
    borderRadius: 20,
    width: Dimensions.get('screen').width / 2 * 1.65,
    height: Dimensions.get('screen').width / 2 * 0.75,
  },
  youtubeContainer: {
    alignSelf: 'center'
  }
})
