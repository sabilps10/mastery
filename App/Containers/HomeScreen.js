import React, { Component } from "react";
import {
  View,
  Text,
  Image,
  ImageBackground,
  AsyncStorage,
  TextInput,
  ScrollView,
  Linking,
  Dimensions,
  ActivityIndicator,
	Platform,
} from "react-native";
import { connect } from "react-redux";
import SideMenu from "react-native-side-menu";
import styles from "./Styles/HomeScreenStyle";
import { Images } from "../Themes";
import TouchableOsResponsive from "../Components/MaterialUi/TouchableOsResponsive";
import SideBar from "../Components/SideBar";
import NewHomeActions from "../Redux/NewHomeRedux"
import CoachingProgrammeActions from "../Redux/CoachingProgrammeRedux";;
import UpdateFCMActions from "../Redux/UpdateFCMRedux";
import QuestActions from "../Redux/QuestRedux";
import LoginActions from "../Redux/LoginRedux";
import firebase from "react-native-firebase";
import io from "socket.io-client";
import Secrets from "react-native-config";
import { Col, Row, Grid } from "react-native-easy-grid";

import ScalableImage from "../Components/ScalableImage";

export class HomeScreen extends Component {
  constructor(props) {
    super(props);

    const length = props.newhome ? props.newhome.programmes ? props.newhome.programmes.length : 100 : 100
    let programmeLoaded = []
    for (let index = 0; index < length; index++) {
      programmeLoaded.push(false)
    }

    this.state = {
      menuOpen: false,
      loaded: false,
      programmeLoaded,
      progress: false,
      quests: [],
      programmes: [],
      banners: [],
    };

    this.handleNewHomePayload = this.handleNewHomePayload.bind(this);
  }

  async componentDidMount() {
    if (__DEV__) console.tron.log("isi home", this.props);
    this.socket = io(Secrets.SOCKET_URL);
    this.socket.on("connect", () => {
      this.socket.emit("joinServer", {
        id: this.props.login.loggedInUser.id,
      });
    });
    this.checkPermission();
    this.createNotificationListeners(); //add this lin
    this.props.getNewHome();
    if (typeof this.props.login.loggedInUser === "undefined")
      return this.props.navigation.navigate("LoginScreen");
    // BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
  }

  componentWillUnmount() {
    this.notificationListener;
    this.notificationOpenedListener;

    // BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
  }

  // handleBackButton() {
  //   return true;
  // }
  //1
  async checkPermission() {
    const enabled = await firebase.messaging().hasPermission();
    if (enabled) {
      this.getToken();
    } else {
      this.requestPermission();
    }
  }

  //2
  async requestPermission() {
    try {
      await firebase.messaging().requestPermission();
      // User has authorised
      this.getToken();
    } catch (error) {
      // User has rejected permissions
      if (__DEV__) console.tron.log("permission rejected");
    }
  }
  //3
  async getToken() {
    let fcmToken = await AsyncStorage.getItem("fcmToken");
    if (!fcmToken) {
      fcmToken = await firebase.messaging().getToken();
      if (fcmToken) {
        // user has a device token
        if (__DEV__) console.tron.log("fcmToken:", fcmToken);
        await AsyncStorage.setItem("fcmToken", fcmToken);
      }
    }
    this.props.updatefcm(fcmToken);
    if (__DEV__) console.tron.log("fcmToken:", fcmToken);
  }
  async createNotificationListeners() {
    /*
     * Triggered when a particular notification has been received in foreground
     * */
    this.notificationListener = firebase
      .notifications()
      .onNotification((notification) => {
        const { title, body } = notification;
        if (__DEV__) console.tron.log("onNotification:");

        const localNotification = new firebase.notifications.Notification({
          sound: "sampleaudio",
          show_in_foreground: true,
        })
          .setSound("sampleaudio.wav")
          .setNotificationId(notification.notificationId)
          .setTitle(notification.title)
          .setBody(notification.body)
          .android.setChannelId("fcm_FirebaseNotifiction_default_channel") // e.g. the id you chose above
          .android.setSmallIcon("@drawable/ic_launcher") // create this icon in Android Studio
          .android.setColor("#000000") // you can set a color here
          .android.setPriority(firebase.notifications.Android.Priority.High);

        firebase
          .notifications()
          .displayNotification(localNotification)
          .catch((err) => console.error(err));
      });

    const channel = new firebase.notifications.Android.Channel(
      "fcm_FirebaseNotifiction_default_channel",
      "Demo app name",
      firebase.notifications.Android.Importance.High
    ).setDescription("Demo app description");
    firebase.notifications().android.createChannel(channel);

    /*
     * If your app is in background, you can listen for when a notification is clicked / tapped / opened as follows:
     * */
    this.notificationOpenedListener = firebase
      .notifications()
      .onNotificationOpened((notificationOpen) => {
        const { title, body } = notificationOpen.notification;
        if (__DEV__) console.tron.log("onNotificationOpened:");
        this.props.navigation.navigate("QuestNewScreen");
      });

    /*
     * If your app is closed, you can check if it was opened by a notification being clicked / tapped / opened as follows:
     * */
    const notificationOpen = await firebase
      .notifications()
      .getInitialNotification();
    if (notificationOpen) {
      const { title, body } = notificationOpen.notification;
      if (__DEV__) console.tron.log("getInitialNotification:");
      this.props.navigation.navigate("QuestNewScreen");
    }
    /*
     * Triggered for data only payload in foreground
     * */
    this.messageListener = firebase.messaging().onMessage((message) => {
      //process data message
      if (__DEV__) console.tron.log("JSON.stringify:", JSON.stringify(message));
    });
  }

  componentDidUpdate(prevProps, prevStates) {
    const { newhome } = this.props;
    if (prevProps.home !== this.props.home) {
      if (this.props.home.status === 401) {
        this.props.logout();
        this.props.navigation.navigate("LoginScreen");
      } else {
        this.setState({ loaded: true });
      }
    }
    else if (prevProps.login.loggedInUser !== this.props.login.loggedInUser) {
      if (this.props.login.loggedInUser === null)
        return this.props.navigation.navigate("LoginScreen");
    }

    else if (prevProps.newhome !== newhome) {
      this.handleNewHomePayload(newhome);
    }
  }
  hideMenu() {
    this.setState({ menuOpen: false });
  }
  handleNewHomePayload(newhome) {
    if (typeof newhome === "undefined") return;
    if (newhome === null) return;
    let quests = newhome.quests ? newhome.quests : [];
    let programmes = newhome.programmes ? newhome.programmes : [];
    let banners = newhome.banners ? newhome.banners : [];
    let profile = newhome.profile ? newhome.profile : [];


    this.setState({
      quests,
      programmes,
      banners,
      profile,
      loaded: true
    })
  }
  renderQuest(data, index) {
    const title = data.title;
    const deadline = data.deadline;
    return (
      <View>
        <Row size={0.2} style={styles.row1}>
          <TouchableOsResponsive
            style={styles.box}
            onPress={() => {
              // this.props.navigation.navigate("CoachingProgrammeMainTitleScreen", { id: id });
              this.props.setDetailId(data.id);
              this.props.navigation.navigate("MyQuestInfoScreen");
            }}
            key={index}
          >
            <Text style={{ fontSize: 17, fontWeight: "bold", color: "#000" }}>
              {title}
            </Text>
            <Text style={{ fontSize: 10, color: "#000" }}>{deadline}</Text>
          </TouchableOsResponsive>
        </Row>
      </View>
    );
  }

  onPressCoach(id) {
    if (__DEV__) console.tron.log("set id ", id);
    this.props.setCoachId(id);
    this.props.navigation.navigate("CoachingProgrammeMainTitleScreen", { id: id });
  }

  renderProgramme(data, index) {
    const title = data.title;
    const coach_name = data.coach_name;
    const photo_url = data.photo_url;
    const price = data.price;
    function currencyFormat(num) {
      return (
        "Rp." + num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.") + ",-"
      );
    }

    return (
      <View>
        <TouchableOsResponsive
          style={styles.box1}
          onPress={() => {
            this.props.navigation.navigate("CoachingProgrammeMainTitleScreen", {
              id: data.id,
            });
          }}
          key={index}
        >
          <ScalableImage style={styles.cp} uri={photo_url} index={index} width={Dimensions.get('screen').width} onLoaded={(index) => {
            let loaded = this.state.programmeLoaded
            loaded[index] = true
            this.setState({
              programmeLoaded: loaded
            })
          }} />
          {this.state.programmeLoaded[index] &&
            <View style={styles.detailProgrammeContainer} >
              <Text style={styles.Title}>{title}</Text>
              <Text style={styles.Coach}>{coach_name}</Text>
              <Text style={styles.Price}>
                {price > 0 ? currencyFormat(price) : "Free"}
              </Text>
            </View>
          }
        </TouchableOsResponsive>
      </View>
    );
  }

  renderBanner(data, index) {
    const banner_url = data.banner_url;
    const url = data.url;

    return (
      <View>
        <TouchableOsResponsive
          style={styles.box1}
          onPress={() => {
            Linking.openURL(url);
          }}
          key={index}
        >
          <ScalableImage
            style={styles.cp}
            uri={banner_url}
            width={Dimensions.get("screen").width}
          />
        </TouchableOsResponsive>
      </View>
    );
  }
  renderLoading() {
    const menu = (
      <SideBar
        onLogout={() => this.props.logout()}
        Profile={
          this.props.newhome.profile !== null
            ? this.props.newhome.profile
            : null
        }
        navigation={this.props.navigation}
        onPressSummary={() => this.hideMenu()}
      />
    );

    return (
      <SideMenu
        menu={menu}
        isOpen={this.state.menuOpen}
        onChange={(isOpen) => this.setState({ menuOpen: isOpen })}
      >
        <View style={styles.HomeContainer}>
          <View style={styles.background} />
          <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
            <TouchableOsResponsive
              style={styles.menuButton}
              onPress={() => {
                this.setState({ menuOpen: true });
              }}
            >
              <Image style={styles.menuImage} source={Images.menu} />
            </TouchableOsResponsive>
            <View style={{
              alignContent: 'center',
              padding: 10,
              alignSelf: 'center'
            }}>
              <ActivityIndicator color={'white'} size={Platform.OS === "ios" ? 'large' : 30} />
              <Text style={{ color: 'white', paddingTop: 20, fontSize: 16 }}>Retrieving Your Progresses...</Text>
            </View>

          </ScrollView>
        </View>
      </SideMenu>
    );
  }
  render() {
    const menu = (
      <SideBar
        onLogout={() => this.props.logout()}
        Profile={
          this.props.newhome.profile !== null
            ? this.props.newhome.profile
            : null
        }
        navigation={this.props.navigation}
        onPressSummary={() => this.hideMenu()}
      />
    );
    this.createNotificationListeners();
    const { newhome } = this.props;
    const { quests, programmes, banners, loaded } = this.state;


    if (!loaded) return this.renderLoading();
    return (
      <SideMenu
        menu={menu}
        isOpen={this.state.menuOpen}
        onChange={(isOpen) => this.setState({ menuOpen: isOpen })}
      >
        <View style={styles.HomeContainer}>
          <View style={styles.background} />
          <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
            <TouchableOsResponsive
              style={styles.menuButton}
              onPress={() => {
                this.setState({ menuOpen: true });
              }}
            >
              <Image style={styles.menuImage} source={Images.menu} />
            </TouchableOsResponsive>
            {quests.length > 0 ? (
              <Row style={{ marginBottom: 18 }}>
                <Text style={styles.TextTop}>YOUR QUEST(S)</Text>
                {/*} <TextInput placeholder={"Search Here"} style={{...styles.loginButton,marginRight:15,width:Dimensions.get('window').width *0.49}}/>
              <Image style={styles.searchImage} source={Images.search}></Image> {*/}
              </Row>
            ) : (null)}
            {quests.length > 0 ? (
              <ScrollView>
                {quests.map((data, index) => {
                  return this.renderQuest(data, index);
                })}
              </ScrollView>
            ) : (
                null
              )}

            {programmes.length > 0 ? (
              <ScrollView>
                <Text style={styles.TextMid}>SPECIAL FOR YOU</Text>
                {programmes.map((data, index) => {
                  return this.renderProgramme(data, index);
                })}
              </ScrollView>
            ) : (
                null

              )}

            {banners.length > 0 ? (
              <ScrollView>
                <Text style={styles.TextMid}>OUR LATEST STUFFS</Text>
                {banners.map((data, index) => {
                  return this.renderBanner(data, index);
                })}
              </ScrollView>
            ) : (
                null

              )}
          </ScrollView>
        </View>
      </SideMenu>
    );
  }
}
//

const mapStateToProps = (state) => {
  const { login, newhome, update_fcm } = state;
  return {
    login,
    newhome,
    update_fcm,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getNewHome: () => dispatch(NewHomeActions.newHomeRequest()),
    logout: () => dispatch(LoginActions.logout()),
    setDetailId: (id) => dispatch(QuestActions.questDetailId(id)),
    updatefcm: (data) => dispatch(UpdateFCMActions.updateFCMRequest(data)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);
