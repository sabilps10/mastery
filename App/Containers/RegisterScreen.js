import React, { Component } from 'react'
import { View, Text, Image, ActivityIndicator, ScrollView, Platform } from 'react-native'
import { connect } from 'react-redux'
import styles from './Styles/RegisterScreenStyle'
import { Images } from '../Themes'

import MaterialEditText from '../Components/MaterialEditText'
import TouchableOSResponsive from '../Components/MaterialUi/TouchableOsResponsive'
import SnackBar from 'react-native-snackbar-component'
import Secrets from "react-native-config";
import HeaderMastery from '../Components/HeaderMastery'


import RegisterActions from '../Redux/RegisterRedux';

class RegisterScreen extends Component {
    constructor(props) {
        super(props)

        this.state = {
            snackbar: false,
            snackbarText: "",
            company_name: "",
            company_address: "",
            company_email: "",
            company_phone: "",
            name: "",
            user_email: "",
            user_phone: "",
            password: "",
            confirm_password: "",
            auth: false,
            debugmsg: ""
        }
    }
    // componentDidMount() {
    //     if (this.props.login.logout) {
    //         this.props.login.signout()
    //     }
    // }
    // componentDidUpdate(prevProps, oldStates) {
    //     //check if access_token still exists.
    //     //if exists, then we verify the token lifetime.
    //     //if token expired, we need to re-login.
    //     //but if token still alive, then we autologin
    //     if (prevProps.login.access_token !== this.props.login.access_token) {
    //         if (this.props.login.access_token !== null) {
    //             this.props.navigation.navigate('HomeScreen')
    //         }

    //     }
    //     if (prevProps.login !== this.props.login) {
    //         if (!this.state.auth) return;
    //         if(__DEV__) console.tron.log("login berubah:", this.props.login);

    //         if (this.props.login.payload !== null) {

    //             if (this.props.login.payload.status === 1) {
    //                 if(__DEV__) console.tron.log("can login");
    //                 if (this.state.auth) {
    //                     this.setState({ auth: false })
    //                     this.props.navigation.navigate('HomeScreen')
    //                 }
    //             } else {
    //                 if(__DEV__) console.tron.log("cannot login");

    //                 this.setState({
    //                     auth: false,
    //                     snackbar: true,
    //                     snackbarText: "Login Failed.."
    //                 })
    //             }

    //         }
    //         if (this.props.login.error) {
    //             if(__DEV__) console.tron.log('login failed')

    //             this.setState({
    //                 auth: false,
    //                 snackbar: true,
    //                 snackbarText: "Email dan Password anda salah !"
    //             })
    //         }
    //     }


    // }
    onPressRegister() {
        const { company_name, company_address, company_email, company_phone, name, user_email, user_phone, password, confirm_password, auth } = this.state
        if(__DEV__) console.tron.log("register nih");
        if (company_name.length > 0 && company_address.length > 0 && company_email.length > 0 && company_phone.length > 0 && name.length > 0 && user_email.length > 0 && user_phone.length > 0 && password.length > 0 && confirm_password.length > 0) {
            if(__DEV__) console.tron.log("do register");
            this.setState({ auth: true })
            this.props.registerAction({
                company_name,
                company_address,
                company_email,
                company_phone,
                name,
                user_email,
                user_phone,
                password,
                confirm_password
            })
            this.props.navigation.navigate('SuccessRegisterScreen')
        }
        else if (company_name.length == 0 && company_address.length == 0 && company_email.length == 0 && company_phone.length == 0 && name.length == 0 && user_email.length == 0 && user_phone.length == 0 && password.length == 0 && confirm_password.length == 0) {
            if(__DEV__) console.tron.log("do register");
            this.props.registerAction({
                company_name,
                company_address,
                company_email,
                company_phone,
                name,
                user_email,
                user_phone,
                password,
                confirm_password
            })
            alert("Please Fill The Form!")
        }
    }

    render() {
        return (
            <ScrollView>  
                <View style={styles.loginContainer}>
                <HeaderMastery
                    navigation={this.props.navigation}
                    title={'REGISTER'}
                />
                    <View style={styles.background} />
                    <View style={styles.welcomeTextConatiner}>
                        <Text style={styles.welcomeTextBold1}>Company Details</Text>
                    </View>
                    <Image style={styles.userIcon} source={Images.userRegister} />
                    <MaterialEditText
                        containerInputStyle={styles.containerInputStyle}
                        value={''}
                        label={'NAME'}
                        onChangeText={(company_name) => {
                            this.setState({ company_name })
                        }} />
                    <Image style={styles.locationIcon} source={Images.location} />
                    <MaterialEditText
                        containerInputStyle={styles.containerInputStyle}
                        value={''}
                        label={'ADDRESS'}
                        onChangeText={(company_address) => {
                            this.setState({ company_address })
                        }} />
                    <Image style={styles.emailIcon} source={Images.email} />
                    <MaterialEditText
                        containerInputStyle={styles.containerInputStyle}
                        value={''}
                        label={'EMAIL'}
                        onChangeText={(company_email) => {
                            this.setState({ company_email })
                        }} />
                    <Image style={styles.telephoneIcon} source={Images.telephone} />
                    <MaterialEditText
                        containerInputStyle={styles.containerInputStyle}
                        value={''}
                        label={'PHONE NUMBER'}
                        onChangeText={(company_phone) => {
                            this.setState({ company_phone })
                        }} />
                    <View style={styles.welcomeTextConatiner}>
                        <Text style={styles.welcomeTextBold2}>New Account</Text>
                    </View>
                    <Image style={styles.usernameIcon} source={Images.userRegister} />
                    <MaterialEditText
                        containerInputStyle={styles.containerInputStyle}
                        value={''}
                        label={'NAME'}
                        onChangeText={(name) => {
                            this.setState({ name })
                        }} />
                    <Image style={styles.useremailIcon} source={Images.email} />
                    <MaterialEditText
                        containerInputStyle={styles.containerInputStyle}
                        value={''}
                        label={'E-MAIL'}
                        onChangeText={(user_email) => {
                            this.setState({ user_email })
                        }} />
                    <Image style={styles.phoneIcon} source={Images.phone} />
                    <MaterialEditText
                        containerInputStyle={styles.containerInputStyle}
                        value={''}
                        label={'PHONE NUMBER'}
                        onChangeText={(user_phone) => {
                            this.setState({ user_phone })
                        }} />
                    <Image style={styles.lockpassIcon} source={Images.lockRegister} />
                    <MaterialEditText
                        containerInputStyle={styles.containerInputStyle}
                        value={''}
                        label={'PASSWORD'}
                        type={'password'}
                        onChangeText={(password) => {
                            this.setState({ password })
                        }} />
                    <Image style={styles.lockconfirmIcon} source={Images.lockRegister} />
                    <MaterialEditText
                        containerInputStyle={styles.containerInputStyle}
                        value={''}
                        label={'CONFIRM PASSWORD'}
                        type={'password'}
                        onChangeText={(confirm_password) => {
                            this.setState({ confirm_password })
                        }} />

                    {(this.state.auth === true) ? (
                        <ActivityIndicator size={Platform.OS === "ios" ? 'large' : "large"} color="#ffffff" />
                    ) : (
                            <TouchableOSResponsive style={styles.loginButton} onPress={() => this.onPressRegister()}>
                                <Text style={styles.loginText}>REGISTER</Text>
                            </TouchableOSResponsive>
                        )}
                    <SnackBar visible={this.state.snackbar} textMessage={this.state.snackbarText} actionHandler={() => { this.setState({ snackbar: false }) }} actionText="Got it!" />
                </View>
            </ScrollView>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        register: state.register
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        registerAction: (input) => dispatch(RegisterActions.registerRequest(input))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(RegisterScreen)
