import React, { Component } from 'react'
import { View, Text, Image, ActivityIndicator, KeyboardAvoidingView, Platform } from 'react-native'
import { connect } from 'react-redux'
import styles from './Styles/LoginScreenStyle'
import { Images } from '../Themes'

import MaterialEditText from '../Components/MaterialEditText'
import TouchableOSResponsive from '../Components/MaterialUi/TouchableOsResponsive'
import SnackBar from 'react-native-snackbar-component'
import Secrets from "react-native-config";

import LoginActions from '../Redux/LoginRedux';
import { StackActions, NavigationActions } from 'react-navigation'

class LoginScreen extends Component {
  constructor(props) {
    super(props)

    this.state = {
      snackbar: false,
      snackbarText: "",
      email: "",
      password: "",
      auth: false,
      debugmsg: ""
    }
  }
  componentDidMount() {
    if (this.props.login.logout) {
      this.props.login.signout()
    }
  }
  componentDidUpdate(prevProps, oldStates) {
    //check if access_token still exists.
    //if exists, then we verify the token lifetime.
    //if token expired, we need to re-login.
		//but if token still alive, then we autologin
		
		if (prevProps.login.access_token !== this.props.login.access_token) {
      if (this.props.login.access_token !== null) {
        const resetAction = StackActions.reset({
          index:0,
          actions: [ NavigationActions.navigate({ routeName: 'HomeScreen'})]
        })
        this.props.navigation.dispatch(resetAction);
      }

    }
    if (prevProps.login !== this.props.login) {
      if (!this.state.auth) return;
      if (__DEV__) console.tron.log("login berubah:", this.props.login);

      if (this.props.login.payload !== null) {

        if (this.props.login.payload.status === 1) {
          if (__DEV__) console.tron.log("can login");
          if (this.state.auth) {
            this.setState({ auth: false })

            // const prevGetStateForAction = Navigator.router.getStateForAction;

            // Navigator.router.getStateForAction = (action, state) => {
            //   // Do not allow to go back from Home
            //   if (action.type === 'Navigation/BACK' && state && state.routes[state.index].routeName === 'HomeScreen') {
            //     return null;
            //   }

            //   // Do not allow to go back to Login
            //   if (action.type === 'Navigation/BACK' && state) {
            //     const newRoutes = state.routes.filter(r => r.routeName !== 'LoginScreen');
            //     const newIndex = newRoutes.length - 1;
            //     return prevGetStateForAction(action, { index: newIndex, routes: newRoutes });
            //   }
            //   return prevGetStateForAction(action, state);
            // };
            
            const resetAction = StackActions.reset({
              index:0,
              actions: [ NavigationActions.navigate({ routeName: 'HomeScreen'})]
            })
            this.props.navigation.dispatch(resetAction);
          
          }

        } else {
          if (__DEV__) console.tron.log("cannot login");

          this.setState({
            auth: false,
            snackbar: true,
            snackbarText: "Login Failed.."
          })
        }

      }
      if (this.props.login.error) {
        if (__DEV__) console.tron.log('login failed')

        this.setState({
          auth: false,
          snackbar: true,
          snackbarText: "Email dan Password anda salah !"
        })
      }
    }


  }
  onPressLogin() {
    const { email, password, auth } = this.state
    if (__DEV__) console.tron.log("login nih");
    if (email.length > 0 && password.length > 0) {
      if (__DEV__) console.tron.log("do login");
      this.setState({ auth: true })
      this.props.loginAction({
        email,
        password
      })
    }
  }

  render() {

    return (
      <View style={styles.loginContainer}>
        <View style={styles.background} />
        <View>
          <Text style={{ color: "#ffffff", fontSize: 40, alignSelf: "center", marginBottom: 30 }}>
            MASTERY
          </Text>
        </View>
        <View style={{

        }}>
          <Image style={styles.userIcon} source={Images.email} />
          <MaterialEditText
            containerInputStyle={styles.containerInputStyle}
            value={''}
            label={'E-MAIL'}
            onChangeText={(email) => {
              this.setState({ email })
            }} />
        </View>
        <View>
          <Image style={styles.lockIcon} source={Images.lock} />
          <MaterialEditText
            containerInputStyle={styles.containerInputStyle}
            value={''}
            label={'PASSWORD'}
            type={'password'}
            onChangeText={(password) => {
              this.setState({ password })
            }} />
        </View>

        {(this.state.auth === true) ? (
          <ActivityIndicator size={Platform.OS === "ios" ? 'large' : "large"} color="#ffffff" />
        ) : (
            <TouchableOSResponsive style={styles.loginButton} onPress={() => this.onPressLogin()}>
              <Text style={styles.loginText}>LOG IN</Text>
            </TouchableOSResponsive>
          )}
        <TouchableOSResponsive style={styles.register} onPress={() => this.props.navigation.navigate('RegisterScreen')}>
          <Text style={styles.registerText}>Don't have an account? Register now!</Text>
        </TouchableOSResponsive>

        <TouchableOSResponsive style={styles.password} onPress={() => this.props.navigation.navigate('ForgetPasswordScreen')}>
          <Text style={styles.passwordText}>Forget password?</Text>
        </TouchableOSResponsive>



        <SnackBar visible={this.state.snackbar} textMessage={this.state.snackbarText} actionHandler={() => { this.setState({ snackbar: false }) }} actionText="Got it!" />
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    login: state.login
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    signout: () => dispatch(LoginActions.logout()),
    loginAction: (input) => dispatch(LoginActions.loginRequest(input))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen)
