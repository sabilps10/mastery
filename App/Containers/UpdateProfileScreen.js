// import React, { Component } from "react";
// import { View, Text, Image, Dimensions, ActivityIndicator } from "react-native";
// import { connect } from "react-redux";

// import styles from "./Styles/MyQuestInfoScreenStyle";

// import TouchableOSResponsive from "../Components/MaterialUi/TouchableOsResponsive";
// import HeaderMastery from "../Components/HeaderMastery";

// import { ScrollView, TextInput } from "react-native-gesture-handler";
// import UpdateProfileActions from "../Redux/UpdateProfileRedux";
// //halaman form update profile
// //untuk dapat mengirimkan data ke API, kita memerlukan redux dan saga khusus untuk UpdateProfile
// //generate redux dan saga :  
// //ignite g redux UpdateProfile
// //ignite g saga UpdateProfile
// //lalu edit /Redux/index.js
// //dan tambahkan entry berikut :
// //update_profile: require('./UpdateProfileRedux').reducer didalam array Reducers.
// //langkah berikutnya setup Sagas/UpdateProfileSaga
// // kita membutuhkan select effects dari redux-saga/effects : 
// //import { call, put, select } from 'redux-saga/effects'
// //pasang LoginSelectors sehingga kita bisa mengakses access token
// //pasang const access_token = yield select(LoginSelectors.getAccessToken); diatas response.
// //sehingga skrg kita memiliki access token
// //ubah response nya menjadi seperti ini : 
// //  const response = yield call(api.getupdateProfile, {data, access_token})
// // perhatikan inputnya berubah dari 
// //yield call(api.getupdateProfile, {data, access_token})
// //menjadi,
// //  const response = yield call(api.getupdateProfile, {data, access_token})
// // sampai sini saga sudah ready.  sekarang kita edit cara memanggil saganya.
// //edit file /Sagas/index.js
// //takeLatest(UpdateProfileTypes.UPDATE_PROFILE_REQUEST, getUpdateProfile, api),
// //getUpdateProfile adalah method UpdateProfileSagas yang kita dispatch

// //redux dan saga sudah selesai di setup,
// //sekarang kita setup api.
// // di saga,  saga akan memanggil  api.getupdateProfile
// // tp method ini blm ada di Api.js
// // edit /srvices/Api.js, dan setup method getUpdateProfile
// // copas fungsi POST sebelumnya, lalu rename menjadi : 
// // dan tambahkan getUpdateProfile didalam return array.

// class UpdateProfileScreen extends Component {
//   constructor(props) {
//     super(props);
//     this.state = {
//       confirm_popup: false,
//       confirm_data: null,
//       close_progress: false,
//       name: "test"
//     };
//   }

//   componentDidMount() {
//       //semua yg di declare di mapStateToProps klo bisa di panggil disini biar bisa di pakai 
//     //didalam fungsi componentDidMount().
//     //semua  dispatcher yang di declare di mapDispatchToProps jg panggil saja disini (jika ada)
//     // login adalah state global yg nilainya di set ketika kita berhasil Login
//     // Cek LoginScreen.js 
//     // nilai login ini di populate ketika loginActions.loginRequest dijalankan (dispatch)
//     //Ingat, componentDidMount() itu dipanggil hanya 1x di awal ketika component ini di mount ke dalam HTML.
//     const { login, navigation } = this.props; 

//     if(__DEV__)
//       console.tron.log("quest info mounting : ", this.props.login.id);
//   }

//   /*
//    * @_props old props,  props adalah global states
//    * @_states old local states
//    */
//   componentDidUpdate(_props, _states) {
//       //semua yg di declare di mapStateToProps klo bisa di panggil disini biar bisa di pakai 
//     //didalam fungsi componentDidMount().
//     //semua  dispatcher yang di declare di mapDispatchToProps jg panggil saja disini (jika ada)
//     // login adalah state global yg nilainya di set ketika kita berhasil Login
//     // Cek LoginScreen.js 
//     // nilai login ini di populate ketika loginActions.loginRequest dijalankan (dispatch)
//     //Ingat, componentDidUpdate() itu dipanggil 
//     //setiap ada perubahan nilai baik nilai Props (global state) dan states (local states).
//     const { login } = this.props; 

//   }

//   render() {
//     //semua yg di declare di mapStateToProps klo bisa di panggil disini biar bisa di pakai 
//     //didalam fungsi render.
//     //semua  dispatcher yang di declare di mapDispatchToProps jg panggil saja disini (jika ada)
//     // login adalah state global yg nilainya di set ketika kita berhasil Login
//     // Cek LoginScreen.js 
//     // nilai login ini di populate ketika loginActions.loginRequest dijalankan (dispatch)
//     const { login,navigation } = this.props; 
//     // local state jg perlu kita bikin shortcutnya
//     const { name } = this.state  //bisa psang field2 yg lainnya nanti
//     //render view
//     return (
//       <ScrollView style={styles.container}>
//         <View style={styles.container}>
//           <HeaderMastery navigation={this.props.navigation} title={"PROFILE"} />
//           <TextInput onChangeText={(value)=>{
//               this.setState('name', value) //update state local yg bernama name.
//           }}/>
//           <TouchableOSResponsive onPress={()=>{
//               //jika tombol update di click, maka kita jalankan action 
//               //utk update profile yg sudah kita transfer dari 
//               //mapDispatchToProps
//               updateProfile({
//                   id: login.loggedInUser.id,
//                   name: name //name didapat dari state local
//               })
//           }}>
//               <View style={{
//                   padding:"10 10",
//                   backgroundColor:"#cc0000",
//                   color:"#fff"
//               }}>
//                   <Text>Update</Text>
//               </View>
//           </TouchableOSResponsive>
//         </View>
//       </ScrollView>
//     );
//   }
// }

// const mapStateToProps = state => {
//   const { login } = state;
//   return {
//     login
//   };
// };

// const mapDispatchToProps = dispatch => {
//   return {
//     getUpdateProfile: (data) => dispatch(UpdateProfileActions.updateProfileRequest(data))
//   };
// };

// //updateProfile diatas. equivalet dgn syntax berikut : 
// //updateProfile = function(data) {
//  //   return dispatch(UpdateProfileActions.updateProfileScreen(data)
// //} 
// export default connect(mapStateToProps, mapDispatchToProps)(UpdateProfileScreen);
