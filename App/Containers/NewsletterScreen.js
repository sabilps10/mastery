import React, { Component } from 'react'
import { View, Text, Image, FlatList, Dimensions } from 'react-native'
import { connect } from 'react-redux'

import styles from './Styles/NewsletterScreenStyle'

import HeaderMastery from '../Components/HeaderMastery';

class NewsletterScreen extends Component {

  renderNewsletter (item, index) {
    // MOCK DATA
    const events = ['CAR FREE DAY', 'NOBAR', 'DADU', 'LEGO']
    const idReward = Math.floor(Math.random() * 3) + 0 
    const uri = 'https://lorempixel.com/15'+index.toString()+'/15'+index.toString()
    const marginToRight = (Dimensions.get('screen').width / 2 * 0.125 / 4)
    const marginToLeft = marginToRight * -1
    const date = '08/07/2019'

    return(
      <View style={{...styles.newsletterContainer, left: (index + 1) % 2 === 0 ? marginToLeft : marginToRight  }}>
        <Image style={styles.newsletterImage} source={{uri: uri}}/>
        <Text style={styles.newsletterTitle}>{events[idReward]}</Text>
        <Text style={styles.newsletterDate}>{date}</Text>
      </View>
    )
  }

  render () {
    const newslettersData = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

    return (
      <View style={styles.rewardsContainer}>
        <HeaderMastery
          navigation={this.props.navigation}
          title={'NEWSLETTER'}
        />
        <FlatList
          removeClippedSubviews
          numColumns={2}
          style={styles.flatListContainer}
          data={newslettersData}
          renderItem={({item, index}) => this.renderNewsletter(item, index)}
          keyExtractor={(item, index) => index.toString()}
        />
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(NewsletterScreen)
