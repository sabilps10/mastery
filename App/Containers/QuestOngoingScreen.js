import React, { Component } from 'react'
import { View, ScrollView, Text, Image, FlatList, Dimensions } from 'react-native'
import { connect } from 'react-redux'

import styles from './Styles/QuestOngoingScreenStyle'
import { Images } from '../Themes'

import TouchableOSResponsive from '../Components/MaterialUi/TouchableOsResponsive'
import HeaderMastery from '../Components/HeaderMastery';

class QuestOngoingScreen extends Component {

  onPressQuest () {
    this.props.navigation.navigate('MyQuestInfoScreen')
  }

  componentDidUpdate(oldProps, oldStates) {
    if (oldProps.quest.id !== this.props.quest.id) {
      if (this.props.quest.id === null)
        return this.props.navigation.navigate("LoginScreen");
    }
    if (oldProps.quest !== this.props.quest) {
      if (__DEV__) console.tron.log("quest status : ", this.props);
      if (this.props.quest.status === 403) {
        if (__DEV__) console.tron.log("quest status : ", this.props);
        this.props.logout();
        this.props.navigation.navigate("LoginScreen");
      } else {
        this.setState({ loaded: true })
      }
    }
  
  }

  renderQuest (data, index) {
    // MOCK DATA
    const title = "BUG FIXINGS"
    const date = "08/07/2019"

    return (
      <TouchableOSResponsive style={styles.questComponentContainer} key={index} onPress={() => { this.onPressQuest() }}>
        <View style={styles.questIconContainer}>
          <Image style={styles.questIcon} source={Images.questIcon}/>
        </View>
        <View style={styles.questDescriptionContainer}>
          <Text style={styles.questTitleText}>{title}</Text>
          <Text style={styles.questDateText}>{date}</Text>
        </View>
      </TouchableOSResponsive>
    )
  }

  render () {
    const questsData = [1]

    return (
      <View style={styles.rewardsContainer}>
        <HeaderMastery
          navigation={this.props.navigation}
          title={"QUESTS"}
        >
          <View style={styles.questButtonContainer}>
            <TouchableOSResponsive onPress={() => { this.props.navigation.navigate('QuestNewScreen') }}>
              <Text style={styles.questButtonTextDeactivated}>NEW QUEST</Text>
            </TouchableOSResponsive>
            <Text style={styles.questButtonText}>YOUR QUEST</Text>
            <TouchableOSResponsive onPress={() => { this.props.navigation.navigate('QuestCompletedScreen') }}>
              <Text style={styles.questButtonTextDeactivated}>COMPLETED</Text>
            </TouchableOSResponsive>
          </View>
        </HeaderMastery>
        <ScrollView style={styles.questsContainer}>
          {
            questsData.map((data, index) => {
              return this.renderQuest(data, index)
            })
          }
        </ScrollView>
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(QuestOngoingScreen)
