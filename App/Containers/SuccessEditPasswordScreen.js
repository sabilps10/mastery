import React, { Component } from 'react'
import { View, Text, Image, ActivityIndicator, StyleSheet } from 'react-native'
import { connect } from 'react-redux'

import styles from './Styles/SuccessRegisterScreenStyle'
import { Images } from '../Themes'

import MaterialEditText from '../Components/MaterialEditText'
import TouchableOSResponsive from '../Components/MaterialUi/TouchableOsResponsive'
import SnackBar from 'react-native-snackbar-component'
import Secrets from "react-native-config";



class SuccessEditPasswordScreen extends Component {
    
  render() {

    return (
        <View style={styles.container}>
                <Image style={styles.menuImage} source={Images.checklist}></Image>
                <Text style={styles.welcomeText}>CONGRATULATION</Text>
                <Text style={styles.welcomeTextBold1}>YOUR PASSWORD</Text>
                <Text style={styles.welcomeTextBold2}>HAS BEEN UPDATED!</Text>
                <TouchableOSResponsive style={styles.loginButton} onPress={() => this.props.navigation.navigate('HomeScreen')}>
                    <Text style={styles.loginText}>BACK TO HOMESCREEN</Text>
                </TouchableOSResponsive>
        </View>
    );
}
}
    
    
export default SuccessEditPasswordScreen




 
