import React, { Component } from "react";
import { View, Text, Image, FlatList, Dimensions, ActivityIndicator, Platform } from "react-native";
import { connect } from "react-redux";

import styles from "./Styles/ShowProfileScreenStyle";
import { Images } from '../Themes'



import HeaderMastery from "../Components/HeaderMastery";
import TouchableOsResponsive from "../Components/MaterialUi/TouchableOsResponsive";
import { Metrics } from "../Themes";

class ShowProfileScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            progress: false,
            profile: false
        };
    }
    componentDidMount() {

        const { newhome } = this.props;
        this.setState({
            profile: newhome.profile
        });

    }
    componentDidUpdate(_props, _states) { }
    render() {
        const { newhome } = this.props;
        const { profile } = this.state;
        if (__DEV__) console.tron.log(newhome);
        return (
            <View style={styles.rewardsContainer}>
                <HeaderMastery
                    navigation={this.props.navigation}
                    title={"MY PROFILE"}
                />
                {profile ? (
                    <View style={styles.loginContainer}>
                        <View style={styles.background} />
                        <View style={styles.container}>
                            <TouchableOsResponsive style={styles.loginButton} onPress={() => this.props.navigation.navigate('EditProfileScreen')}>
                                <Text style={styles.loginText}>EDIT</Text>
                                <Image style={styles.menuImage} source={Images.edit}></Image>
                            </TouchableOsResponsive>

                            <View style={{ justifyContent: "center", alignItems: "center" }}>
                                <Image style={styles.avatarImage} source={{ uri: profile.avatar }}></Image>
                            </View>
                            <View>
                                <Image style={styles.usernameIcon} source={Images.user} />
                                <Text style={styles.containerInputStylename}>{profile.name}</Text>
                            </View>
                            <View>
                                <Image style={styles.useremailIcon} source={Images.email} />
                                <Text style={styles.containerInputStyleemail}>{profile.email}</Text>
                            </View>
                            <View>
                                <Image style={styles.phoneIcon} source={Images.phone} />
                                <Text style={styles.containerInputStylephone}>{profile.phone_number}</Text>
                            </View>
                        </View>
                    </View>

                ) : (
                        <View style={{ marginTop: Metrics.baseMargin * 10 }}>
                            <ActivityIndicator size={Platform.OS === "ios" ? 'large' : 30}></ActivityIndicator>
                        </View>
                    )}

            </View>
        );
    }
}

const mapStateToProps = state => {
    const { login, newhome } = state;

    return {
        login,
        newhome
    };
};

const mapDispatchToProps = dispatch => {
    return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(ShowProfileScreen);
