import React, { Component } from "react";
import {
  View,
  Text,
  Image,
  ImageBackground,
  KeyboardAvoidingView,
  ActivityIndicator,
  AsyncStorage,
  Alert,
  BackHandler,
	Platform
} from "react-native";
import { connect } from "react-redux";
import SideMenu from "react-native-side-menu";
import styles from "./Styles/MyPerformanceScreenStyle";
import { Images } from "../Themes";
import TouchableOsResponsive from "../Components/MaterialUi/TouchableOsResponsive";
import SideBar from "../Components/SideBar";
import HomeActions from "../Redux/HomeRedux";
import UpdateFCMActions from "../Redux/UpdateFCMRedux";
import LoginActions from "../Redux/LoginRedux";
import firebase from 'react-native-firebase';
import io from "socket.io-client";
import Secrets from "react-native-config";
import HeaderMastery from '../Components/HeaderMastery'



export class MyPerformanceScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      menuOpen: false,
      loaded: false,
    };
  }

  async componentDidMount() {
    this.socket = io(Secrets.SOCKET_URL);
    this.socket.on('connect', () => {
      this.socket.emit("joinServer", {
        id: this.props.login.loggedInUser.id
      });
    });
    this.checkPermission();
    this.createNotificationListeners(); //add this lin
    this.props.getHome();
    if (typeof this.props.login.loggedInUser === "undefined")
    return this.props.navigation.navigate("LoginScreen");
    // BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
  }

  componentWillUnmount() {
    this.notificationListener;
    this.notificationOpenedListener;
    // BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
  }

  // handleBackButton() {
  //   return true;
  // }
  //1
  async checkPermission() {
    const enabled = await firebase.messaging().hasPermission();
    if (enabled) {
      this.getToken();
    } else {
      this.requestPermission();
    }
  }

  //2
  async requestPermission() {
    try {
      await firebase.messaging().requestPermission();
      // User has authorised
      this.getToken();
    } catch (error) {
      // User has rejected permissions
      if(__DEV__) console.tron.log('permission rejected');
    }
  }
  //3
  async getToken() {
    let fcmToken = await AsyncStorage.getItem('fcmToken');
    if (!fcmToken) {
      fcmToken = await firebase.messaging().getToken();
      if (fcmToken) {
        // user has a device token
        if(__DEV__) console.tron.log('fcmToken:', fcmToken);
        await AsyncStorage.setItem('fcmToken', fcmToken);
      }
    }
    this.props.updatefcm(
      fcmToken
    );
    if(__DEV__) console.tron.log('fcmToken:', fcmToken);
  }
  async createNotificationListeners() {
    /*
    * Triggered when a particular notification has been received in foreground
    * */
    this.notificationListener = firebase.notifications().onNotification((notification) => {
      const { title, body } = notification;
      if(__DEV__) console.tron.log('onNotification:');

      const localNotification = new firebase.notifications.Notification({
        sound: 'sampleaudio',
        show_in_foreground: true,
      })
      .setSound('sampleaudio.wav')
      .setNotificationId(notification.notificationId)
      .setTitle(notification.title)
      .setBody(notification.body)
      .android.setChannelId('fcm_FirebaseNotifiction_default_channel') // e.g. the id you chose above
      .android.setSmallIcon('@drawable/ic_launcher') // create this icon in Android Studio
      .android.setColor('#000000') // you can set a color here
      .android.setPriority(firebase.notifications.Android.Priority.High);

      firebase.notifications()
        .displayNotification(localNotification)
        .catch(err => console.error(err));
    });

    const channel = new firebase.notifications.Android.Channel('fcm_FirebaseNotifiction_default_channel', 'Demo app name', firebase.notifications.Android.Importance.High)
      .setDescription('Demo app description')
    firebase.notifications().android.createChannel(channel);

    /*
    * If your app is in background, you can listen for when a notification is clicked / tapped / opened as follows:
    * */
    this.notificationOpenedListener = firebase.notifications().onNotificationOpened((notificationOpen) => {
      const { title, body } = notificationOpen.notification;
      if(__DEV__) console.tron.log('onNotificationOpened:');
      this.props.navigation.navigate("QuestNewScreen");

    });

    /*
    * If your app is closed, you can check if it was opened by a notification being clicked / tapped / opened as follows:
    * */
    const notificationOpen = await firebase.notifications().getInitialNotification();
    if (notificationOpen) {
      const { title, body } = notificationOpen.notification;
      if(__DEV__) console.tron.log('getInitialNotification:');
      this.props.navigation.navigate("QuestNewScreen");
    }
    /*
    * Triggered for data only payload in foreground
    * */
    this.messageListener = firebase.messaging().onMessage((message) => {
      //process data message
      if(__DEV__) console.tron.log("JSON.stringify:", JSON.stringify(message));
    });
  }

  componentDidUpdate(prevProps, prevStates) {
    if (prevProps.home !== this.props.home) {
      if (this.props.home.status === 401) {
        this.props.logout();
        this.props.navigation.navigate("LoginScreen");
      } else {
        this.setState({ loaded: true })
      }
    }
    if (prevProps.login.loggedInUser !== this.props.login.loggedInUser) {
      if (this.props.login.loggedInUser === null)
        return this.props.navigation.navigate("LoginScreen");
    }
  }
  hideMenu() {
    this.setState({ menuOpen: false });
  }

  render() {
    const menu = (
      <SideBar
        onLogout={() => this.props.logout()}
        loggedInUser={this.props.login.loggedInUser !== null ? this.props.login.loggedInUser : null}
        navigation={this.props.navigation}
        onPressSummary={() => this.hideMenu()}
      />
    );
    this.createNotificationListeners();


    const payload =
      this.props.home.payload !== null
        ? this.props.home.payload
        : {
          stats: {
            points_left: 0,
            completed: 0,
            outstandings: 0,
            missed: 0
          }
        };
    return (
      
      
        <View style={styles.HomeContainer}>
          <HeaderMastery
          navigation={this.props.navigation}
          title={'MY PERFORMANCE'}
        />
          <View style={styles.background} />
          {/* <TouchableOsResponsive
            style={styles.menuButton}
            onPress={() => {
              this.setState({ menuOpen: true });
            }}
          >
            <Image style={styles.menuImage} source={Images.menu} />
          </TouchableOsResponsive> */}
          <View style={styles.welcomeImageContainer}>
            <ImageBackground
              style={styles.welcomeImageBackground}
              imageStyle={styles.welcomeImageBackground}
              source={Images.welcomeBackground}
            >
              <Image
                style={styles.welcomeImage}
                source={Images.welcome}
              ></Image>
            </ImageBackground>
          </View>
          {this.state.loaded ? (
            <View>
              <View style={styles.yourPointsContainer}>
                <Text style={styles.yourPointsTitle}>Your Point</Text>
                <Text style={styles.yourPointsContent}>
                  {payload.stats.points_left}
                </Text>
              </View>
              <View style={styles.otherPointsContainer}>
                <View style={styles.pointsContainer}>
                  <Text style={styles.pointsText}>
                    {payload.stats.completed}
                  </Text>
                  <Text style={styles.pointsName}>Completed</Text>
                </View>
                <View style={styles.separator} />
                <View style={styles.pointsContainer}>
                  <Text style={styles.pointsText}>
                    {payload.stats.outstandings}
                  </Text>
                  <Text style={styles.pointsName}>Outstanding</Text>
                </View>
                <View style={styles.separator} />
                <View style={styles.pointsContainer}>
                  <Text style={styles.pointsText}>{payload.stats.missed}</Text>
                  <Text style={styles.pointsName}>Missed</Text>
                </View>
              </View>
            </View>
          ) : (
              <View style={{
                flex: 1,
                flexDirection: "row",
                alignContent: "center",
                justifyContent: "center"
              }}>
                <ActivityIndicator size={Platform.OS === "ios" ? 'large' : "large"} color="#ffffff" />
              </View>
            )}
        </View>
    );
  }
}
//

const mapStateToProps = state => {
  const { login, home, update_fcm } = state;
  return {
    login,
    home,
    update_fcm,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getHome: () => dispatch(HomeActions.homeRequest()),
    logout: () => dispatch(LoginActions.logout()),
    updatefcm: data => dispatch(UpdateFCMActions.updateFCMRequest(data))
  };

};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MyPerformanceScreen);
