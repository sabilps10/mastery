import React, { Component } from 'react'
import { View, Text, Image, ActivityIndicator, StyleSheet } from 'react-native'
import { connect } from 'react-redux'

import styles from './Styles/SuccessCoachingScreenStyle'
import { Images } from '../Themes'

import MaterialEditText from '../Components/MaterialEditText'
import TouchableOSResponsive from '../Components/MaterialUi/TouchableOsResponsive'
import SnackBar from 'react-native-snackbar-component'
import Secrets from "react-native-config";



class ReturnCoachingScreen extends Component {
    
  render() {

    return (
        <View style={styles.container}>
                <Image style={styles.menuImage} source={Images.checklist}></Image>
                <Text style={styles.welcomeText}>CONGRATULATION</Text>
                <Text style={styles.welcomeTextBold1}>Selangkah Lagi Sebelum Memulai Program Anda ! Coaching Programme baru akan tersedia untuk anda setelah anda menyelesaikan Pembayaran anda.</Text>
               
            <TouchableOSResponsive style={styles.loginButton} onPress={() => this.props.navigation.navigate('CoachingProgrammeMainTitleScreen')}>
                    <Text style={styles.loginText}>START QUEST</Text>
                </TouchableOSResponsive>
        </View>
    );
}
}
    
    
export default ReturnCoachingScreen




 
