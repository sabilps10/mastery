import React, { Component } from 'react'
import { View, ScrollView, Text, Image } from 'react-native'
import { connect } from 'react-redux'

import styles from './Styles/ProgrameeListScreenStyle'
import { Images } from '../Themes'
import CoachingProgrammeActions from "../Redux/CoachingProgrammeRedux";

import TouchableOSResponsive from '../Components/MaterialUi/TouchableOsResponsive'
import HeaderMastery from '../Components/HeaderMastery'

class CoachingProgrammeListScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      progress: false,
    };
  }
  componentDidMount() {
    this.props.getCoachs();
  }

  componentDidUpdate(oldProps, oldStates) {
    if (oldProps.coach.id !== this.props.coach.id) {
      if (__DEV__) console.tron.log("coach id : ", this.props.coach);
      if (this.props.coach.id === null)
        return this.props.navigation.navigate("LoginScreen");
    }
    if (oldProps.coach.pick_result !== this.props.coach.pick_result) {
      this.props.getCoachs();
      this.setState({
        progress: false
      })
    }
  }
  onPressCoach(id) {
    if (__DEV__) console.tron.log("set id ", id);
    this.props.setCoachId(id);
    this.props.navigation.navigate("CoachingProgrammeMainTitleScreen", { id: id });
  }

  renderRewardData(data, index) {
    const coach_photo = data.coach_photo
    const coach_name = data.coach_name
    const id = data.id
    const name = data.name

    return (
      <TouchableOSResponsive onPress={() => this.onPressCoach(id)} key={index}>
        <View style={{ ...styles.ticketComponentContainer, marginTop: -50 }}>
          <Image style={styles.ticketImage} source={{ uri: coach_photo }} ></Image>
          <View style={{ ...styles.ticketContainer }}>
            <Text style={styles.ticketTitle}>{coach_name}</Text>
            <Text style={styles.ticketDescription}>{name}</Text>
          </View>
        </View>
      </TouchableOSResponsive>
    )
  }


  render() {
    // const coach = [1]
    const { coach } = this.props;
    if (__DEV__) console.tron.log("isi coach ", coach);
    return (
      <View style={styles.rewardsContainer}>
				<View style={styles.headerContainer}/>
        <HeaderMastery
          navigation={this.props.navigation}
          title={'PROGRAMME'}
        />
        <ScrollView style={styles.ticketsContainer}>

          {
            coach.payload !== null ? (
              <ScrollView style={styles.ticketsContainer}>
                <Text style={styles.welcomeTextBold}>Selamat datang di Coaching Programme !</Text>
                {
                  coach.payload.map((data, index) => {
                    return this.renderRewardData(data, index)
                  })
                }
              </ScrollView>
            ) :
              (
                <View style={styles.ticketComponent}>
                  <Text style={styles.welcomeTextBoldBottom}>You don't have any coaching programme right now!</Text>
                </View>
              )
          }

        </ScrollView>
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  const { coach } = state;

  return {
    coach
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    getCoachs: data => dispatch(CoachingProgrammeActions.coachingProgrammeRequest(data)),
    setCoachId: id => dispatch(CoachingProgrammeActions.coachingProgrammeDetailId(id)),
    pickCoach: id => dispatch(CoachingProgrammeActions.pickCoach(id))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(CoachingProgrammeListScreen)