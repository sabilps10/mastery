import React, { Component } from "react";
import {
  View,
  ScrollView,
  Text,
  Image,
  TextInput,
  Platform
} from "react-native";
import { connect } from "react-redux";

import styles from "./Styles/MyQuestWorkspaceScreenStyle";
import { Images, Metrics, Colors } from "../Themes";
import TouchableOSResponsive from "../Components/MaterialUi/TouchableOsResponsive";

import PostsActions from "../Redux/PostsRedux";
import { GiftedChat } from "react-native-gifted-chat";
import io from "socket.io-client";
import HeaderMastery from "../Components/HeaderMastery";
import ImagePicker from "react-native-image-picker";
import SoundPlayer from "react-native-sound-player";
import SendPhotoActions from "../Redux/SendPhotoRedux";
import Secrets from "react-native-config";

class QuestWorkspaceScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      message: "",
      menuOpen: false,
      photoSource: null,
      videoSource: null,
			chatMessages: [],
      posttype: "",
      task_id: props.navigation.state.params.taskid.task_id,
      id: props.login.loggedInUser.id
    };
    this.onSend = this.onSend.bind(this);
    this.handlePosts = this.handlePosts.bind(this);
  }
  componentDidMount() {
    SoundPlayer.stop();
    this.props.getPosts(this.props.navigation.state.params.taskid.task_id);

    this.socket = io(Secrets.SOCKET_URL);
    this.socket.on("connect", () => {
      this.socket.emit("joinServer", {
        id: this.props.login.loggedInUser.id
      });
    });
    this.socket.on("message", message => {
			if(__DEV__) console.tron.log("onMessage:", message);
			if (this.state.id === message.from_id) {
			} else {
				this.props.getPosts(this.props.navigation.state.params.taskid.task_id);
				// this.setState(previousState => ({
				// 	chatMessages: GiftedChat.append(previousState.chatMessages, {
				// 		text: message.message,
				// 		createdAt: Date.now(),
				// 		user: {
				// 			_id: message.from_id,
				// 			name: message.name,
				// 			avatar: message.avatar
				// 		}
				// 	})
				// }));
			}
    });
  }
  componentDidUpdate(oldProps, oldStates) {
    const { posts } = this.props;
    if (oldProps.posts !== posts) {
      // if(__DEV__) console.tron.log(posts);
      this.handlePosts();
    }
    if (oldStates !== this.state) {
      if (oldStates.photoSource !== this.state.photoSource) {
        const uploadOpts = {
          file: this.state.photoSource,
          body: {
            submit_type: "photo"
          },
          id: this.state.task_id
        };

        this.props.getPhoto(uploadOpts);
      }
    }
    if (oldStates !== this.state) {
      if (oldStates.videoSource !== this.state.videoSource) {
        const uploadOpts = {
          file: this.state.videoSource,
          body: {
            submit_type: "video"
          },
          id: this.state.task_id
        };

        this.props.getPhoto(uploadOpts);
      }
    }
  }
  handlePosts() {
    const { posts } = this.props;
    if (typeof posts.payload === "undefined") return;
    if (posts.payload === null) return;
    /*
      id:214
user_id:9
created_at:2020-03-04T19:37:12.000Z
task_id:66
text:test
hp_text:test
thumbnail_text:test
uploaded_file:""
hp_file:""
thumbnail_file:""
post_type:text
name:Hapsoro Renaldy
profile_pic:IMG20180228112758.jpg
avatar:IMG20180228112758.jpg
when:in 4 hours
*/
    let chatMessages = [];
    let _posts = posts.payload.posts;

    _posts.map((post, index) => {
      chatMessages.push({
        _id: post.id,
        text: post.text,
        createdAt: post.created_at,
        user: post.user_id === this.props.login.loggedInUser.id ? {
          name: post.name,
          avatar: post.avatar
        } : {
          _id: post.user_id,
          name: post.name,
          avatar: post.avatar
        },
        image: post.post_type === 'image' ? post.uploaded_file : null,
      });
    });

    this.setState({
			chatMessages: chatMessages.reverse()
    });
  }
  showImagePicker() {
    this.setState({
      posttype: "image"
    });
    const options = {
      title: "Take Photo",
      takePhotoButtonTitle: "From Camera",
      mediaType: "photo",
      storageOptions: {
        skipBackup: true,
        path: "mastery"
      }
    };
    ImagePicker.showImagePicker(options, response => {
      if(__DEV__) console.tron.log("Response = ", response);
      if (response.didCancel) {
        if(__DEV__) console.tron.log("User cancelled image picker");
      } else if (response.error) {
        if(__DEV__) console.tron.log("ImagePicker Error: ", response.error);
      } else if (response.customButton) {
        if(__DEV__)
          console.tron.log(
            "User tapped custom button: ",
            response.customButton
          );
      } else {
        if(__DEV__) console.tron.log("workspace photo response =", response);
        this.setState({
          photoSource: response
        });
      }
    });
  }

  showVideoPicker() {
    this.setState({
      posttype: "video"
    });
    const options = {
      title: "Take Video",
      mediaType: "video",
      durationLimit: 100,
      takePhotoButtonTitle: "From Camera",
      storageOptions: {
        skipBackup: true,
        path: "mastery"
      }
    };
    ImagePicker.showImagePicker(options, response => {
      if(__DEV__) console.tron.log("Response = ", response);
      if (response.didCancel) {
        if(__DEV__) console.tron.log("User cancelled image picker");
      } else if (response.error) {
        if(__DEV__) console.tron.log("ImagePicker Error: ", response.error);
      } else if (response.customButton) {
        if(__DEV__)
          console.tron.log(
            "User tapped custom button: ",
            response.customButton
          );
      } else {
        if(__DEV__) console.tron.log("workspace photo response =", response);
        this.setState({
          videoSource: response
        });
      }
    });
  }

  onSend(chatMessages = []) {
    let oldMessage = this.chat.chatMessages;
    if(__DEV__) console.tron.log("chatMessages:", this.state.message, chatMessages);
    this.socket.emit("newMessage", {
			msg: this.state.message,
			createdAt: Date.now(),
      room: this.state.task_id,
      from_id: this.state.id,
      post_type: "text"
    });
    if(__DEV__) console.tron.log("append text:", oldMessage);
    this.setState(previousState => ({
      chatMessages: GiftedChat.append(previousState.chatMessages, chatMessages)
    }));
    
  }

  setCustomText = message => {
    this.setState({ message: message });
  };

  render() {
    return (
      <View style={{ flex: 1 }}>
				<View style={styles.headerContainer}/>
        <HeaderMastery navigation={this.props.navigation} title={"WORKSPACE"} />
        <GiftedChat
					ref={chat => (this.chat = chat)}
					messagesContainerStyle={{paddingTop: Metrics.navBarHeight}}
          messages={this.state.chatMessages}
          alwaysShowSend={true}
          onSend={chatMessages => this.onSend(chatMessages)}
          text={this.state.message}
          onInputTextChanged={message => this.setCustomText(message)}
        />
         {/*}
        <TouchableOSResponsive
          onPress={() => {
            this.showImagePicker();
          }}
          style={{
            marginRight: Metrics.horizontalMargin,
            marginBottom: Metrics.marginVertical,
            alignSelf: "flex-end"
          }}
        >
          <Image
            style={styles.iconImage}
            source={Images.cameraIcon}
            style={{ marginLeft: 0, width: 40, height: 40 }}
          />
        </TouchableOSResponsive>
        {/*}
        <TouchableOSResponsive
          onPress={() => {
            this.showVideoPicker();
          }}
          style={{
            marginRight: Metrics.horizontalMargin,
            marginBottom: Metrics.marginVertical,
            alignSelf: "flex-end"
          }}
        >
          <Image
            style={styles.iconImage}
            source={Images.videoIcon}
            style={{ marginLeft: 0, width: 40, height: 40 }}
          />
        </TouchableOSResponsive>{*/}

        <View style={styles.bottomBarContainer}>
          <TouchableOSResponsive
            style={styles.bottomBarImageContainer}
            onPress={() => {
              this.props.navigation.navigate("MyQuestInfoScreen");
            }}
          >
            <Image
              style={styles.bottomBarImageDeactivated}
              source={Images.infoIcon}
            />
            <Text style={styles.iconTitleDeactivated}>INFO</Text>
          </TouchableOSResponsive>
          <TouchableOSResponsive
            style={styles.bottomBarImageContainer}
            onPress={() => {}}
          >
            <Image
              style={styles.bottomBarImage}
              source={Images.workspaceIcon}
            />
            <Text style={styles.iconTitle}>WORKSPACE</Text>
          </TouchableOSResponsive>
          <TouchableOSResponsive
            style={styles.bottomBarImageContainer}
            onPress={() => {
              this.props.navigation.navigate("MyQuestFilesScreen");
            }}
          >
            <Image
              style={styles.bottomBarImageDeactivated}
              source={Images.filesIcon}
            />
            <Text style={styles.iconTitleDeactivated}>EVIDENCE</Text>
          </TouchableOSResponsive>
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => {
  const { login, posts, send_photo, quest } = state;
  return {
    posts,
    login,
    send_photo,
    quest
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getPosts: id => dispatch(PostsActions.postsRequest({ id })),
    getPhoto: data => dispatch(SendPhotoActions.sendPhotoRequest(data))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(QuestWorkspaceScreen);
