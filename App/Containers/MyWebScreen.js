import React, { Component } from 'react';
import { WebView, ActivityIndicator, View, Linking, Platform } from 'react-native'
import { connect } from 'react-redux'
import { Colors } from '../Themes'
import { StackActions } from 'react-navigation';


class MyWebScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
            url: props.navigation.state.params.url,
            canGoForward: false
        };
        if (__DEV__) console.tron.log("isi nilai :", this.props);
    }

    _onNavigationStateChange(webViewState) {
        if (__DEV__) console.tron.log("isi URL = ", webViewState.url)
        if (__DEV__) console.tron.log("isi URL return = ", webViewState.url.indexOf('/return'))
        if(webViewState.url.indexOf(' /notify/payment/return') === -1 &&  webViewState.url.indexOf('/return') > 0) {
            this.props.navigation.navigate('ReturnCoachingScreen')
        }
        if(webViewState.url.indexOf(' /notify/payment/success') === -1 &&  webViewState.url.indexOf('/success') > 0) {
            this.props.navigation.navigate('SuccessCoachingScreen')
        }
    }


    renderLoadingView() {
        return (
            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                <ActivityIndicator
                    size={Platform.OS === "ios" ? 'large' : 'large'}
                    style={{ color: Colors.masteryOrange }}
                />
            </View>
        );
    }

    render() {
        const { url } = this.props.navigation.state.params;
        const { navigate, state, dispatch } = this.props.navigation;
        return (
            <WebView
                source={{ uri: url }}
                renderLoading={this.renderLoadingView}
                startInLoadingState={true}
                ref={(ref) => { this.webview = ref; }}
                onNavigationStateChange={
                    this._onNavigationStateChange.bind(this)
                    // if (__DEV__) console.tron.log("isi URL = ", webViewState.url)
                    // if (webViewState.url.indexOf('/success')){
                    //     this.props.navigation.navigate("SuccessCoachingScreen");
                    // } else if (webViewState.url.indexOf('/return')) {
                    //     this.props.navigation.navigate("SuccessCoachingScreen");
                    // }
                    // if (event.url !== url) {
                    //     this.webview.stopLoading();
                    //     if (webViewState.url.indexOf('/success')) {
                    //         this.props.navigation.navigate("SuccessCoachingScreen");
                    //     } else if (event.url.includes('http://local/')) {
                    //         const pageLink = event.url.substring(event.url.lastIndexOf('/') + 1, event.url.length);
                    //         navigate(pageLink)
                    //         this.webview.reload()
                    //     } else {
                    //         Linking.openURL(event.url);
                    //     }
                    // }
                }
            />
        );
    }
}
const mapStateToProps = (state) => {
    const { coach } = state;
    return {
        coach
    }
}

const mapDispatchToProps = (dispatch) => {
    return {

    }
}

export default connect(mapStateToProps, mapDispatchToProps)(MyWebScreen)