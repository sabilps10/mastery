import React, { Component } from "react";
import {
  View,
  Text,
  Image,
  FlatList,
  Dimensions,
  ActivityIndicator,
  BackHandler,
	Platform
} from "react-native";
import { connect } from "react-redux";
import Icon from "react-native-vector-icons/FontAwesome";
import styles from "./Styles/MyQuestFilesScreenStyle";
import { Images } from "../Themes";
import TouchableOSResponsive from "../Components/MaterialUi/TouchableOsResponsive";
import HeaderMastery from "../Components/HeaderMastery";
import SendProofActions from "../Redux/SendProofRedux";
import SendDocActions from "../Redux/SendDocRedux";
import SendAudioActions from "../Redux/SendAudioRedux";
import EvidenceActions from "../Redux/EvidenceRedux";
import ImagePicker from "react-native-image-picker";
import VideoPlayer from "../Components/VideoPlayer";
import AudioPlayer from "../Components/AudioPlayer";
import DownloadFile from "../Components/DownloadFile";
import DocumentPicker from 'react-native-document-picker';
import SoundPlayer from 'react-native-sound-player';
import { PermissionsAndroid } from 'react-native';
import AudioRecorder from "../Components/AudioRecorder";


class MyQuestFilesScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      document_download: false,
      audio_player: false,
      video_player: false,
      menuOpen: false,
      photoSource: null,
      videoSource: null,
      loading: false,
    };

    this.onCancelAudioRecord = this.onCancelAudioRecord.bind(this)
    this.onPressUploadAudio = this.onPressUploadAudio.bind(this)
  }

  onCancelAudioRecord () {
    this.setState({
      audio_player: false
    })
  }

  onPressUploadAudio (path) {
    this.setState({
      loading: true
    })

    try {
      const audio = {
        name: 'audio.mp4',
        type: 'audio/mp4',
        uri: "file://" + path
      }
      audio.fileName = audio.name;
      let audios = {
        file: audio,
        body: {
          submit_type: "audio"
        },
        id: this.props.quest.detail_payload.participant_task_id

      }

      this.props.uploadAudio(audios);
    } catch (err) {
      if (DocumentPicker.isCancel(err)) {
        alert('Canceled from single audio picker');
      } else {
        alert('Unknown Error: ' + JSON.stringify(err));
        throw err;
      }

      this.setState({
        loading: false
      })
    }
  }

  showImagePicker() {
    const options = {
      title: "Take Photo",
      takePhotoButtonTitle: "From Camera",
      mediaType: "photo",
      // customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
      storageOptions: {
        skipBackup: true,
        path: "mastery"
      }
    };
    ImagePicker.showImagePicker(options, response => {
      if (__DEV__) console.tron.log("Response = ", response);

      if (response.didCancel) {
        if (__DEV__) console.tron.log("User cancelled image picker");
      } else if (response.error) {
        if (__DEV__) console.tron.log("ImagePicker Error: ", response.error);
      } else if (response.customButton) {
        if (__DEV__) console.tron.log("User tapped custom button: ", response.customButton);
      } else {
        this.setState({
          photoSource: response,
          menuOpen: false,
          loading: true
        });
        const uploadOpts = {
          file: response,
          body: {
            //description: this.state.photoSource.fileName,
            submit_type: "photo"
          },
          id: this.props.quest.detail_payload.participant_task_id
        };

        this.props.upload(uploadOpts);
        setTimeout(() => {
          this.setState({
            loading: false
          })
          this.loadList()
        }, 10000)
      }
    });
  }
  showVideoPicker() {
    const options = {
      title: "Take Video",
      mediaType: "video",
      durationLimit: 60,
      takePhotoButtonTitle: "From Camera",
      // customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
      storageOptions: {
        skipBackup: true,
        path: "mastery"
      }
    };
    ImagePicker.showImagePicker(options, response => {
      if (__DEV__) console.tron.log("Response = ", response);
      if (response.didCancel) {
        if (__DEV__) console.tron.log("User cancelled video picker");
      } else if (response.error) {
        if (__DEV__) console.tron.log("ImagePicker Error: ", response.error);
      } else if (response.customButton) {
        if (__DEV__) console.tron.log("User tapped custom button: ", response.customButton);
      } else {
        // You can also display the image using data:
        // const source = { uri: 'data:image/jpeg;base64,' + response.data };
        if (__DEV__) console.tron.log("user pick video", response);
        this.setState({
          videoSource: response,
          menuOpen: false,
          loading: true
        });
        const uploadOpts = {
          file: response,
          body: {
            //description: this.state.photoSource.fileName,
            submit_type: "video"
          },
          id: this.props.quest.detail_payload.participant_task_id
        };

        this.props.upload(uploadOpts);
      }
    });
  }

  async showDocPicker() {
    this.setState({
      loading: true,
    })
    try {
      const doc = await DocumentPicker.pick({
        type: [DocumentPicker.types.pdf],
      });
      doc.fileName = doc.name;
      let docs = {
        file: doc,
        body: {
          submit_type: "document"
        },
        id: this.props.quest.detail_payload.participant_task_id

      }
      //Printing the log realted to the file
      if (__DEV__) console.tron.log('doc : ' + JSON.stringify(docs));
      if (__DEV__) console.tron.log('URI : ' + doc.uri);
      if (__DEV__) console.tron.log('Type : ' + doc.type);
      if (__DEV__) console.tron.log('name : ' + doc.name);
      if (__DEV__) console.tron.log('File Size : ' + doc.size);

      this.props.uploadDocs(docs);
      setTimeout(() => {
        this.setState({
          loading: false
        })
        this.loadList()
      }, 10000)
    } catch (err) {
      //Handling any exception (If any)
      if (DocumentPicker.isCancel(err)) {
        //If user canceled the document selection
        alert('Canceled from single doc picker');
      } else {
        //For Unknown Error
        alert('Unknown Error: ' + JSON.stringify(err));
        throw err;
      }

      this.setState({
        loading: false
      })
    }
  }

  async showAudioPicker() {
    this.setState({
      audio_player: true
    })

    // this.setState({
    //   loading: true
    // })
    // try {
    //   const audio = await DocumentPicker.pick({
    //     type: [DocumentPicker.types.audio],
    //   });
    //   // let type = audio.type.split("/");
    //   // audio.name = `${audio.name}.${type[1]}`;
    //   audio.fileName = audio.name;
    //   let audios = {
    //     file: audio,
    //     body: {
    //       submit_type: "audio"
    //     },
    //     id: this.props.quest.detail_payload.participant_task_id

    //   }
    //   //Printing the log realted to the file
    //   if (__DEV__) console.tron.log('audio : ' + JSON.stringify(audios));
    //   if (__DEV__) console.tron.log('URI : ' + audio.uri);
    //   if (__DEV__) console.tron.log('Type : ' + audio.type);
    //   if (__DEV__) console.tron.log('name : ' + audio.fileName);
    //   if (__DEV__) console.tron.log('File Size : ' + audio.size);

    //   this.props.uploadAudio(audios);
    //   setTimeout(async () => {
    //     await this.loadList()
    //     this.setState({
    //       loading: false,
    //     })
    //   }, 10000)

    // } catch (err) {
    //   //Handling any exception (If any)
    //   if (DocumentPicker.isCancel(err)) {
    //     //If user canceled the document selection
    //     alert('Canceled from single audio picker');
    //   } else {
    //     //For Unknown Error
    //     alert('Unknown Error: ' + JSON.stringify(err));
    //     throw err;

    //   }

    //   this.setState({
    //     loading: false
    //   })
    // }

  }

  async requestRecordPermission() 
  {
		if (Platform.OS === 'ios') {
			this.requestWritePermission();
		} else {
			try {
				const granted = await PermissionsAndroid.request(
					PermissionsAndroid.PERMISSIONS.RECORD_AUDIO,
					{
						'title': 'Mastery',
						'message': 'Please allow for record permission'
					}
				)
				if (granted === PermissionsAndroid.RESULTS.GRANTED) {
					this.requestWritePermission();
				} else {
					alert("Record permission denied");
				}
			} catch (err) {
				console.warn(err)
			}
		}
  }

  async requestWritePermission() {
		if (Platform.OS === 'ios') {
			this.showAudioPicker();
		} else {
			try {
				const granted = await PermissionsAndroid.request(
					PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
					{
						'title': 'Mastery',
						'message': 'Please allow for write permission'
					}
				)
				if (granted === PermissionsAndroid.RESULTS.GRANTED) {
					this.showAudioPicker();
				} else {
					alert("Record permission denied");
				}
			} catch (err) {
				console.warn(err)
			}
		}
  }

  componentDidMount() {
    this.loadList();
    SoundPlayer.stop();
  }
  async componentDidUpdate(oldProps, oldStates) {
    // try {

    //   if (oldProps.send_proof.payload !== this.props.send_proof.payload) {
    //     if (this.props.send_proof.payload !== null) this.loadList();
    //   }

    if (oldProps.send_proof.fetching && !this.props.send_proof.fetching) {
      this.setState({
        loading: false
      })
      this.loadList()
    }

    if (oldProps.send_audio.fetching && !this.props.send_audio.fetching) {
      this.setState({
        loading: false
      })
      this.loadList()
    }

    //   if (oldStates !== this.state) {
    //     if (oldStates.photoSource !== this.state.photoSource) {

    //       //if(__DEV__) console.tron.log(this.props.quest.detail_payload);
    //       const uploadOpts = {
    //         file: this.state.photoSource,
    //         body: {
    //           //description: this.state.photoSource.fileName,
    //           submit_type: "photo"
    //         },
    //         id: this.props.quest.detail_payload.participant_task_id
    //       };

    //       this.props.upload(uploadOpts);
    //     }
    //     if (oldStates.videoSource !== this.state.videoSource) {
    //       if(__DEV__) console.tron.log(this.state.videoSource);
    //       const uploadOpts = {
    //         file: this.state.videoSource,
    //         body: {
    //           //description: this.state.videoSource.fileName,
    //           submit_type: "video"
    //         },
    //         id: this.props.quest.detail_payload.participant_task_id
    //       };

    //       this.props.upload(uploadOpts);
    //     }
    //   }
    // } catch (error) {
    //   if(__DEV__) console.tron.log(error);

    // }

  }
  loadList() {
    this.props.getEvidence({
      id: this.props.quest.detail_payload.participant_task_id,
      page: 1
    });
  }
  onBuffer(event) {
    if (__DEV__) console.tron.log("video is buffering");
  }
  videoError(event) {
    if (__DEV__) console.tron.log("video_error");
  }
  renderData(item, index) {
    if (__DEV__) console.tron.log("item=", item);
    return (
      <View
        key={index}
        style={{
          ...styles.newsletterContainer,
          left:
            (index + 1) % 2 === 0
              ? -(((Dimensions.get("screen").width / 2) * 0.1) / 4)
              : ((Dimensions.get("screen").width / 2) * 0.125) / 4
        }}
      >
        {item.submit_type === "photo" ? (
          <Image
            style={styles.newsletterImage}
            source={{
              uri: item.path
            }}
          />
        ) : null}

        {item.submit_type === "video" ? (
          <TouchableOSResponsive
            onPress={() => {
              this.setState({
                video: item.path
              });

              setTimeout(() => {
                this.setState({
                  video_player: true
                })
                this.loadList()
              }, 250)
            }}
          >
            <View style={styles.videobox}>
              <Icon
                name="film"
                size={50}
                color="#cccccc"
                style={{
                  flex: 2,
                  alignContent: "center",
                  textAlign: "center",
                  justifyContent: "center",
                  marginTop: 20
                }}
              />
              <Text
                style={{
                  flex: 1,
                  alignContent: "center",
                  textAlign: "center",
                  justifyContent: "center"
                }}
              >
                Click to Play Video
              </Text>
            </View>
          </TouchableOSResponsive>
        ) : null}

        {item.submit_type === "document" ? (
          <TouchableOSResponsive
            onPress={() => {
              this.props.navigation.navigate('DownloadFile', { url: item.path })
              // this.setState({
              //   document_download: true,
              //   document: item.path
              // });
            }}
          >
            <View style={styles.docbox}>
              <Icon
                name="file"
                size={50}
                color="#cccccc"
                style={{
                  flex: 2,
                  alignContent: "center",
                  textAlign: "center",
                  justifyContent: "center",
                  marginTop: 20
                }}
              />
              <Text
                style={{
                  flex: 1,
                  alignContent: "center",
                  textAlign: "center",
                  justifyContent: "center"
                }}
              >
                Click to Download File
              </Text>
            </View>
          </TouchableOSResponsive>
        ) : null}

        {item.submit_type === "audio" ? (
          <TouchableOSResponsive
            onPress={() => {
              this.setState({
                audio: item.original_url
              });

              setTimeout(() => {
                // this.setState({
                //   audio_player: true
                // })
                // const track = new Sound(this.state.audio, null, (e) => {
                //   if (e) {
                //     console.tron.log('error loading track:', e)
                //   } else {
                //     console.tron.log(track)
                //     // track.play();
                //     this.props.navigation.navigate('PlayerScreen', {title: 'Audio File', filepath: this.state.audio})
                //   }
                // })
                this.props.navigation.navigate('PlayerScreen', {title: 'Audio File', filepath: this.state.audio})
              }, 250)
            }}
          >
            <View style={styles.audiobox}>
              <Icon
                name="music"
                size={50}
                color="#cccccc"
                style={{
                  flex: 2,
                  alignContent: "center",
                  textAlign: "center",
                  justifyContent: "center",
                  marginTop: 20
                }}
              />
              <Text
                style={{
                  flex: 1,
                  alignContent: "center",
                  textAlign: "center",
                  justifyContent: "center"
                }}
              >
                Click to Play Audio
      </Text>
            </View>
          </TouchableOSResponsive>
        ) : null}


        {/* {item.submit_type === "document" ? (
          <View style={styles.docbox}>
            <Image
              style={styles.newsletterImage}
              source={{
                uri: item.uri
              }}
            />
          </View>
        ) : null}

        {item.submit_type === "audio" ? (
          <View style={styles.audiobox}>
            <Image
              style={styles.newsletterImage}
              source={{
                uri: item.uri 
              }}
            />
          </View>
        ) : null} */}

        <Text style={styles.newsletterDate}>{item.created_at}</Text>
      </View>
    );
  }

  renderMenuMinimized(task_status) {
    if (task_status !== 0) return
    return (

      <View style={styles.menuContainer}>
        <TouchableOSResponsive
          onPress={() => {
            this.setState({ menuOpen: true });
          }}
        >
          <Image style={styles.iconImageBig} source={Images.plusIconSmall} />
        </TouchableOSResponsive>
      </View>
    );
  }

  renderMenuMaximized() {
    return (
      <View style={styles.menuContainer}>
        <TouchableOSResponsive
          onPress={() => {
            this.showVideoPicker();
          }}
        >
          <Image style={styles.iconImage} source={Images.videoIcon} />
        </TouchableOSResponsive>
        <TouchableOSResponsive
          onPress={() => {
            this.showImagePicker();
          }}
        >
          <Image style={styles.iconImage} source={Images.cameraIcon} />
        </TouchableOSResponsive>
        <TouchableOSResponsive
          onPress={() => {
            this.showDocPicker();
          }}
        >
          <Image style={styles.iconImage} source={Images.docIcon} />
        </TouchableOSResponsive>
        <TouchableOSResponsive
          onPress={() => {
            this.requestRecordPermission();
            // this.showAudioPicker();
          }}
        >
          <Image style={styles.iconImage} source={Images.audioIcon} />
        </TouchableOSResponsive>
        <TouchableOSResponsive
          onPress={() => {
            this.setState({ menuOpen: false });
          }}
        >
          <Image style={styles.iconImageBig} source={Images.plusIconBig} />
        </TouchableOSResponsive>
      </View>
    );
  }
  renderFlatList() {
    const { payload } = this.props.evidence;
    if (payload === null) return null;
    return (
      <FlatList
        removeClippedSubviews
        numColumns={2}
        style={styles.flatListContainer}
        data={payload.submits}
        renderItem={({ item, index }) => this.renderData(item, index)}
      />
    );
  }
  render() {
    if (__DEV__) console.tron.log('files', this.props.quest.detail_payload);
    const { task_status } = this.props.quest.detail_payload;
    const { payload } = this.props.evidence;
    return (
      <View style={{ flex: 1 }}>
        <View style={styles.container}>
          <View style={styles.headerContainer}/>
					<HeaderMastery
						navigation={this.props.navigation}
						title={"MY EVIDENCE"}
					/>

					{this.props.send_proof.fetching && this.props.send_doc.fetching && this.props.send_audio.fetching ? (

						<View
							style={{
								flex: 1,
								flexDirection: "column",
								alignContent: "center"
							}}
						>
							<ActivityIndicator size={Platform.OS === "ios" ? 'large' : "large"} style={{ flex: 1 }} />
							<Text
								style={{ flex: 1, alignContent: "center", textAlign: "center" }}
							>
								Uploading, please wait...
						</Text>
						</View>
					) : (
							this.renderFlatList()
						)}

          {this.state.menuOpen
            ? this.renderMenuMaximized()
            : this.renderMenuMinimized(task_status)}

          <View style={styles.bottomBarContainer}>
            <TouchableOSResponsive
              style={styles.bottomBarImageContainer}
              onPress={() => {
                this.props.navigation.navigate("MyQuestInfoScreen");
              }}
            >
              <Image
                style={styles.bottomBarImageDeactivated}
                source={Images.infoIcon}
              />
              <Text style={styles.iconTitleDeactivated}>INFO</Text>
            </TouchableOSResponsive>
            {(task_status === 0) ? (
              <TouchableOSResponsive
                style={styles.bottomBarImageContainer}
                onPress={() => {
                  this.props.navigation.navigate("QuestWorkspaceScreen", { taskid: this.props.quest.detail_payload });
                }}
              >
                <Image
                  style={styles.bottomBarImageDeactivated}
                  source={Images.workspaceIcon}
                />
                <Text style={styles.iconTitleDeactivated}>WORKPLACE</Text>
              </TouchableOSResponsive>
            ) : null}

            <TouchableOSResponsive
              style={styles.bottomBarImageContainer}
              onPress={() => { }}
            >
              <Image style={styles.bottomBarImage} source={Images.filesIcon} />
              <Text style={styles.iconTitle}>EVIDENCE</Text>
            </TouchableOSResponsive>
          </View>

          {this.state.video_player ? (
            <VideoPlayer
              uri={this.state.video}
              style={{
                position: "absolute",
                top: 0,
                left: 0,
                right: 0,
                bottom: 0,
                width: "100%",
                height: "100%"
              }}
              close={() => {
                this.setState({ video_player: false });
              }}
            />
          ) : null}

          {/* {this.state.audio_player ? (
            <VideoPlayer
            uri={this.state.audio}
            is_audio={true}
            style={{
              position: "absolute",
              top: 0,
              left: 0,
              right: 0,
              bottom: 0,
              width: "100%",
              height: "100%"
            }}
            close={() => {
              this.setState({ audio_player: false });
            }}
            // <AudioPlayer
            //   navigation={this.props.navigation}
            //   uri={this.state.audio}
            //   style={{
            //     position: "absolute",
            //     top: 0,
            //     left: 0,
            //     right: 0,
            //     bottom: 0,
            //     width: "100%",
            //     height: "100%"
            //   }}
            //   close={() => {
            //     this.setState({ audio_player: false });
            //   }}
            />
          ) : null} */}

          {this.state.document_download ? (
            <DownloadFile
              uri={this.state.document}
              style={{
                position: "absolute",
                top: 0,
                left: 0,
                right: 0,
                bottom: 0,
                width: "100%",
                height: "100%"
              }}
              close={() => {
                this.setState({ document_download: false });
              }}
            />
          ) : null}
          <AudioRecorder
            visible={this.state.audio_player}
            navigation={this.props.navigation}
            onCancelAudioRecord={this.onCancelAudioRecord}
            onPressUploadAudio={this.onPressUploadAudio}
          />
        </View>
        {
          this.state.loading &&

          <View style={styles.loaderContainer2}>
            <ActivityIndicator size={Platform.OS === "ios" ? 'large' : "large"} color={styles.spinner.color} />
          </View>
        }
      </View>
    );
  }
}


const mapStateToProps = state => {
  return {
    send_proof: state.send_proof,
    send_doc: state.send_doc,
    send_audio: state.send_audio,
    quest: state.quest,
    evidence: state.evidence
  };
};

const mapDispatchToProps = dispatch => {
  return {
    upload: data => dispatch(SendProofActions.sendProofRequest(data)),
    uploadDocs: data => dispatch(SendDocActions.sendDocRequest(data)),
    uploadAudio: data => dispatch(SendAudioActions.sendAudioRequest(data)),
    getEvidence: data => dispatch(EvidenceActions.evidenceRequest(data))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MyQuestFilesScreen);
