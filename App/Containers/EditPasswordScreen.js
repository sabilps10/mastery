import React, { Component } from 'react'
import { View, Text, Image, ActivityIndicator, ScrollView, Dimensions } from 'react-native'
import { connect } from 'react-redux'
import styles from './Styles/EditPasswordScreenStyle'
import { Images } from '../Themes'

import MaterialEditText from '../Components/MaterialEditText'
import TouchableOSResponsive from '../Components/MaterialUi/TouchableOsResponsive'
import HeaderMastery from '../Components/HeaderMastery'

import EditProfileActions from '../Redux/EditProfileRedux'

class EditPasswordScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            progress: false,
            loggedInUser: false,
            password: "",
            confirm_password: "",
        };
    }

    componentDidMount() {

        const { login } = this.props;
        this.setState({
            loggedInUser: login.loggedInUser
        });
    }

    onPressEditProfile() {
        const { name, phone_number, password, confirm_password, } = this.state
        
        // this.props.navigation.navigate('SuccessEditPasswordScreen')
        if (password.length == 0 || password == "" || confirm_password.length == 0 || confirm_password == "") {
            alert("Please Fill The Form!");
        } else {
            this.props.doEditProfile({
                password,
                confirm_password,
            })
        }
    }

    render() {
        const { login } = this.props;
        const { loggedInUser } = this.state;
        if (__DEV__) console.tron.log(login);
        return (
            <View style={styles.loginContainer}>
                <HeaderMastery
                    navigation={this.props.navigation}
                    title={'EDIT PASSWORD'}
                />
                <View style={{ marginTop: Dimensions.get('screen').height / 8 }}>
                    <View style={styles.inputContainer}>
                        <Image style={styles.inputIcon} source={Images.lock} />
                        <MaterialEditText
                            containerInputStyle={styles.containerInputStyle}
                            value={''}
                            isFocused={this.state.password.length > 0}
                            propsValue={this.state.password}
                            label={'NEW PASSWORD'}
                            onChangeText={(password) => {
                                this.setState({ password })
                            }} />
                    </View>
                    <View style={styles.inputContainer}>
                        <Image style={styles.inputIcon} source={Images.lock} />
                        <MaterialEditText
                            containerInputStyle={styles.containerInputStyle}
                            value={''}
                            isFocused={this.state.confirm_password.length > 0}
                            propsValue={this.state.confirm_password}
                            label={'CONFIRM PASSWORD'}
                            onChangeText={(confirm_password) => {
                                this.setState({ confirm_password })
                            }} />
                    </View>
                    <TouchableOSResponsive style={styles.passButton} onPress={() => this.props.navigation.navigate('EditProfileScreen')}>
                        <Text style={styles.loginText}>EDIT PROFILE</Text>
                    </TouchableOSResponsive>
                    <TouchableOSResponsive style={styles.loginButton} onPress={() => this.onPressEditProfile()}>
                        <Text style={styles.loginText}>SAVE EDIT</Text>
                    </TouchableOSResponsive>
                </View>
            </View>
        )
    }
}

const mapStateToProps = (state) => {
    const { login } = state
    return {
        login,
        edit_profile: state.edit_profile
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        doEditProfile: (data) => dispatch(EditProfileActions.editProfileRequest(data))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(EditPasswordScreen)
