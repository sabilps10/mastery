import React, { Component } from 'react'
import { View, ScrollView, Text, Image, TextInput, Dimensions } from 'react-native'
import { connect } from 'react-redux'

import styles from './Styles/MyQuestWorkspaceScreenStyle'
import { Images, Metrics } from '../Themes'

import TouchableOSResponsive from '../Components/MaterialUi/TouchableOsResponsive'
import HeaderMastery from '../Components/HeaderMastery'
import QuestActions from "../Redux/QuestRedux";
import PostsActions from "../Redux/PostsRedux";
import SendPostsActions from "../Redux/SendPostsRedux";
import SendPhotoActions from "../Redux/SendPhotoRedux";

import HTML from 'react-native-render-html';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Colors } from '../Themes';
import ImagePicker from "react-native-image-picker";
import { TouchableOpacity } from 'react-native-gesture-handler'

class MyQuestWorkspaceScreen extends Component {
  constructor(props) {
    if(__DEV__) console.tron.log("ayam");
    super(props);

    this.state = {
      chatText: "",
      photoSource: null,
    }
  }

  showImagePicker() {
    const options = {
      title: "Take Photo",
      takePhotoButtonTitle: "From Camera",
      mediaType: "photo",
      // customButtons: [{ nabme: 'fb', title: 'Choose Photo from Facebook' }],
      storageOptions: {
        skipBackup: true,
        path: "mastery"
      }
    };
    ImagePicker.showImagePicker(options, response => {
      if(__DEV__) console.tron.log("Response = ", response);

      if (response.didCancel) {
        if(__DEV__) console.tron.log("User cancelled image picker");
      } else if (response.error) {
        if(__DEV__) console.tron.log("ImagePicker Error: ", response.error);
      } else if (response.customButton) {
        if(__DEV__) console.tron.log("User tapped custom button: ", response.customButton);
      } else {
        if(__DEV__) console.tron.log("workspace photo response =", response);
        this.setState({
          photoSource: response,
          menuOpen: false,
        });
      }
    });
  }
  componentDidMount() {
    this.setState({
      chatText: "",
    })
    if(__DEV__) console.tron.log("workspace props", this.props.quest);
    this.props.getPosts(this.props.quest.detail_payload.task_id);
  }
  componentDidUpdate(oldProps, oldStates) {
    // if (oldProps.posts.payload !== this.props.posts.payload) {

    // }

    if (oldProps.send_posts.payload !== this.props.send_posts.payload) {
      this.props.getPosts(this.props.quest.detail_payload.task_id);
    }
    if (oldStates !== this.state) {
      if (oldStates.photoSource !== this.state.photoSource) {
        //if(__DEV__) console.tron.log(this.props.quest.detail_payload);
        const uploadOpts = {
          file: this.state.photoSource,
          body: {
            //description: this.state.photoSource.fileName,
            submit_type: "photo"
          },
          id: this.props.quest.detail_payload.task_id
        };

        this.props.getPhoto(uploadOpts);
      }
    }
  }
  renderMessage(data, index) {
    const { name, avatar, text, post_type, when } = data
    return (
      <View style={styles.messageContainer} key={index}>
        <Image style={styles.chatAvatarImageBlack} source={{ uri: avatar }} />
        <View style={styles.messageTextContainer}>
          <View style={styles.messageNameContainer}>
            <Text style={styles.messageNameText}>{name}</Text>
            <Text style={styles.messagTimestampText}>{when}</Text>
          </View>

          {post_type === 'text' ?
            <Text style={styles.messageContentText}>{text}</Text>
            : null}

          {post_type !== 'text' ?
            <HTML html={text} imagesMaxWidth={Dimensions.get('window').width} ></HTML>
            : null}

          {post_type === 'sample' ?
            <View style={styles.messageContentImageContainer}>
              <Text style={styles.messageContentImageDescription}>DCIM_0932281012_001_07982019.jpg</Text>
              <Image style={{ height: 150, width: 150 }} source={{ uri: 'https://lorempixel.com/150/150/' }} />
            </View>
            : null}
          {post_type === 'docs' ?
            <View>
              {
                this.state.photoSource != null &&
                <Image style={{ height: 250, width: 180, marginLeft: -40 }} source={{ uri: `data:image/gif;base64,${this.state.photoSource.data}` }} />
              }
            </View>
            : null}
        </View>
      </View>
    )
  }

  renderInput() {
    return (
      <View style={styles.textInputContainer}>

        <TextInput
          placeholder={'Tulis pesan...'}
          title={'pesan'}
          value={this.state.chatText}
          onChangeText={(chatText) => {
            if(__DEV__) console.tron.log(chatText)
            this.setState({ chatText })
          }}
          style={{ flex: 3 }}
        />

        <TouchableOSResponsive
          onPress={
            () => {
              if(__DEV__) console.tron.log("kirim", this.state.chatText);
              this.props.reply({
                id: this.props.quest.detail_payload.task_id,
                text: this.state.chatText,
                post_type: "text"
              })
              this.setState({ chatText: "" })

            }}
          style={styles.chatImageContainer}>
          <Text style={{
            marginRight: 5,
            marginTop: 7,
            color: Colors.masteryBlue
          }}>KIRIM</Text>

          <Icon name="angle-double-right" size={30} color={Colors.masteryBlue} />
        </TouchableOSResponsive>



      </View>
    )
  }

  renderMenuMaximized() {

  }

  render() {
    if(__DEV__) console.tron.log(this.props);
    return (
      <View style={styles.container}>
        <View style={styles.container}>
          <HeaderMastery
            navigation={this.props.navigation}
            title={"WORKSPACE"}
          />
          <ScrollView style={styles.chatContainer}
            ref={ref => this.scrollView = ref}
            onContentSizeChange={(contentWidth, contentHeight) => {
              this.scrollView.scrollToEnd({ animated: true });
            }}>
            {
              this.props.posts.payload === null ? <Text></Text> :
                this.props.posts.payload.posts.map((data, index) => {
                  return this.renderMessage(data, index)
                })
            }
          </ScrollView>
          <TouchableOpacity
            onPress={() => {
              this.showImagePicker();
            }}
            style={{


              marginRight: Metrics.horizontalMargin,
              marginBottom: Metrics.marginVertical,
              alignSelf: "flex-end",

            }}>
            <Image style={styles.iconImage} source={Images.cameraIcon} style={{ marginLeft: 0, width: 40, height: 40 }} />
          </TouchableOpacity>

        </View>
        <View style={styles.lineBreak} />

        {this.renderInput()}

        <View style={styles.bottomBarContainer}>
          <TouchableOSResponsive style={styles.bottomBarImageContainer} onPress={() => { this.props.navigation.navigate('MyQuestInfoScreen') }}>
            <Image style={styles.bottomBarImageDeactivated} source={Images.infoIcon} />
            <Text style={styles.iconTitleDeactivated}>INFO</Text>
          </TouchableOSResponsive>
          <TouchableOSResponsive style={styles.bottomBarImageContainer} onPress={() => { }}>
            <Image style={styles.bottomBarImage} source={Images.workspaceIcon} />
            <Text style={styles.iconTitle}>WORKSPACE</Text>
          </TouchableOSResponsive>
          <TouchableOSResponsive style={styles.bottomBarImageContainer} onPress={() => { this.props.navigation.navigate('MyQuestFilesScreen') }}>
            <Image style={styles.bottomBarImageDeactivated} source={Images.filesIcon} />
            <Text style={styles.iconTitleDeactivated}>EVIDENCE</Text>
          </TouchableOSResponsive>
        </View>
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  if(__DEV__) console.tron.log('the state', state);
  return {
    quest: state.quest,
    posts: state.posts,
    send_posts: state.send_posts,
    send_photo: state.send_photo

  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    //id -> quest_id
    getPosts: (id) => dispatch(PostsActions.postsRequest({ id })),
    reply: (data) => dispatch(SendPostsActions.sendPostsRequest(data)),
    getPhoto: (data) => dispatch(SendPhotoActions.sendPhotoRequest(data))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(MyQuestWorkspaceScreen)
