import React, { Component } from 'react'
import { View, Text, Image, ActivityIndicator, StyleSheet } from 'react-native'
import { connect } from 'react-redux'

import styles from './Styles/SuccessResetPassScreenStyle'
import { Images } from '../Themes'

import MaterialEditText from '../Components/MaterialEditText'
import TouchableOSResponsive from '../Components/MaterialUi/TouchableOsResponsive'
import SnackBar from 'react-native-snackbar-component'
import Secrets from "react-native-config";
import colors from '../Themes/Colors'



class SuccessResetPassScreen extends Component {

    render() {

        return (
            <View style={styles.container}>
                    <Image style={styles.menuImage} source={Images.mail}></Image>
                    <Text style={styles.welcomeTextBold}>PLEASE CHECK YOUR EMAIL </Text>
                    <Text style={styles.welcomeText1}>FOR THE INSTRUCTION TO RESET</Text>
                    <Text style={styles.welcomeText2}>YOUR PASSWORD</Text>
                    <TouchableOSResponsive style={styles.loginButton} onPress={() => this.props.navigation.navigate('LoginScreen')}>
                        <Text style={styles.loginText}>BACK TO LOG IN</Text>
                    </TouchableOSResponsive>
               
            </View>
        );
    }
}


export default SuccessResetPassScreen





