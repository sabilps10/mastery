import React, { Component } from "react";
import { View, Text, } from "react-native";
import { connect } from "react-redux";
import styles from "./Styles/QuestNewScreenStyle";
import styles2 from "./Styles/QuestCompletedScreenStyle";
import TouchableOSResponsive from "../Components/MaterialUi/TouchableOsResponsive";
import HeaderMastery from "../Components/HeaderMastery";
import QuestList from "../Components/QuestList";
import QuestActions from "../Redux/QuestRedux";
import OnGoingQuestList from "../Components/OngoingQuestList";
import FinishedQuestList from "../Components/FinishedQuestList";
import TakeQuestConfirmPopup from "../Components/TakeQuestConfirmPopup";
import LoginActions from "../Redux/LoginRedux";

class QuestNewScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pick_progress: false,
      tabIndex: 0,
      questDetailOpenState: [false, false],
      confirm_popup: false,
      confirm_data: {}
    };
  }
  componentDidMount() {
    this.props.getQuests();
    if (typeof this.props.login.loggedInUser === "undefined")
      return this.props.navigation.navigate("LoginScreen");
  }
  componentDidUpdate(oldProps, oldStates) {
    if (oldProps.quest.id !== this.props.quest.id) {
      if (this.props.quest.id === null)
        return this.props.navigation.navigate("LoginScreen");
    }
    if (oldProps.quest !== this.props.quest) {
      if (__DEV__) console.tron.log("quest status : ", this.props);
      if (this.props.quest.status === 403) {
        if (__DEV__) console.tron.log("quest status : ", this.props);
        this.props.logout();
        this.props.navigation.navigate("LoginScreen");
      } else {
        this.setState({ loaded: true })
      }
    }
    if (oldProps.quest.pick_result !== this.props.quest.pick_result) {
      this.props.getQuests();
      this.setState({
        confirm_popup: false,
        pick_progress: false
      })
    }
  
  }
  onPressExpandShrink(index) {
    let newState = this.state.questDetailOpenState;
    newState[index] = !newState[index];

    this.setState({ questDetailOpenState: newState });
  }
  onPressQuest(id) {
    if (__DEV__) console.tron.log("detail ", id);
    this.props.setDetailId(id);
    this.props.navigation.navigate("MyQuestInfoScreen");
  }


  render() {
    return (

      <View style={styles.rewardsContainer}>
				<View style={styles.headerContainer}>
					<HeaderMastery navigation={this.props.navigation} title={"QUESTS"}>
						<View style={styles.questButtonContainer}>
							<TouchableOSResponsive
								onPress={() => {
									this.setState({ tabIndex: 0 });
									// this.props.navigation.navigate("QuestOngoingScreen");
								}}
							>
								<Text
									style={
										this.state.tabIndex === 0
											? styles.questButtonText
											: styles.questButtonTextDeactivated
									}
								>
									NEW QUEST
								</Text>
							</TouchableOSResponsive>
							<TouchableOSResponsive
								onPress={() => {
									this.setState({ tabIndex: 1 });
								}}
							>
								<Text
									style={
										this.state.tabIndex === 1
											? styles.questButtonText
											: styles.questButtonTextDeactivated
									}
								>
									YOUR QUEST
								</Text>
							</TouchableOSResponsive>
							<TouchableOSResponsive
								onPress={() => {
									this.setState({ tabIndex: 2 });
								}}
							>
								<Text
									style={
										this.state.tabIndex === 2
											? styles.questButtonText
											: styles.questButtonTextDeactivated
									}
								>
									COMPLETED
								</Text>
							</TouchableOSResponsive>
						</View>
					</HeaderMastery>
				</View>

        {this.state.tabIndex === 0 ? (
          <QuestList
            progress={this.props.quest.fetching}
            styles={styles}
            takeQuest={data => {
              this.setState({ confirm_popup: true, confirm_data: data });
            }}
            data={
              this.props.quest.payload !== null
                ? this.props.quest.payload.available
                : []
            }
          />
        ) : (
            <View></View>
          )}

        {this.state.tabIndex === 1 ? (
          <OnGoingQuestList
            progress={this.props.quest.fetching}
            styles={styles}
            onclick={id => {
              this.onPressQuest(id);
              if (__DEV__) console.tron.log("close" + id);
            }}
            data={
              typeof this.props.quest.payload.ongoings !== null
                ? this.props.quest.payload.ongoings
                : []
            }
          />
        ) : (
            <View></View>
          )}
        {this.state.tabIndex === 2 ? (
          <FinishedQuestList
            styles={styles2}
            onclick={id => {
              this.onPressQuest(id);
            }}
            progress={this.props.quest.fetching}

            data={
              typeof this.props.quest.payload.finished !== null
                ? this.props.quest.payload.finished
                : []
            }
          />
        ) : (
            <View></View>
          )}
        <TakeQuestConfirmPopup
          visible={this.state.confirm_popup}
          data={this.state.confirm_data}
          progress={this.state.pick_progress}
          confirm={() => {
            const { confirm_data } = this.state;
            this.setState({ pick_progress: true })
            this.props.pickQuest(confirm_data.id);
          }}
          cancel={() => {
            this.setState({ confirm_popup: false });
          }}
          styles={styles}
        />
      </View>
    );
  }
}

const mapStateToProps = state => {
  const { quest, login } = state;

  return {
    quest,
    login
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getQuests: () => dispatch(QuestActions.questRequest({ page: 1 })),
    setDetailId: id => dispatch(QuestActions.questDetailId(id)),
    pickQuest: id => dispatch(QuestActions.pickQuest(id)),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(QuestNewScreen);
