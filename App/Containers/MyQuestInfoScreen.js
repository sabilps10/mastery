import React, { Component } from "react";
import { View, Text, Image, Dimensions, ActivityIndicator, Platform } from "react-native";
import { connect } from "react-redux";

import styles from "./Styles/MyQuestInfoScreenStyle";
import { Images } from "../Themes";
import Icon from "react-native-vector-icons/FontAwesome";

import TouchableOSResponsive from "../Components/MaterialUi/TouchableOsResponsive";
import HeaderMastery from "../Components/HeaderMastery";
import QuestActions from "../Redux/QuestRedux";
import CloseQuestActions from "../Redux/CloseQuestRedux";
import HTML from "react-native-render-html";
import RoundedButton from "../Components/RoundedButton";
import { ScrollView } from "react-native-gesture-handler";
import CloseQuestConfirmPopup from "../Components/CloseQuestConfirmPopup";
import { ApplicationStyles, Fonts, Colors, Metrics } from "../Themes";
import SoundPlayer from 'react-native-sound-player';
import MediaControls, { PLAYER_STATES } from "react-native-media-controls";
import CoachingProgrammeActions from "../Redux/CoachingProgrammeRedux";
import HTMLView from 'react-native-htmlview';
import YouTube from 'react-native-youtube';


class MyQuestInfoScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      confirm_popup: false,
      confirm_data: null,
      close_progress: false,
      detail: "",
      video_player: false,
      text_description: false,
      currentTime: 0,
      duration: 0,
      isFullScreen: true,
      isLoading: true,
      paused: false,
      playerState: PLAYER_STATES.PLAYING,
      screenType: "cover",
      youtubePlayerReady: false,
      fullscreen: false
    };

    this.renderContent = this.renderContent.bind(this)
  }
  componentDidMount() {
    this.props.getCoachs();
    this.props.getQuest(this.props.quest.id);
    SoundPlayer.stop();

    setTimeout(() => {
      this.setState({
        youtubePlayerReady: true
      })
    }, 500)
  }
  componentDidUpdate(oldProps, oldStates) {
    if (oldProps.close_quest.payload !== this.props.close_quest.payload) {
      this.setState({ close_quest: false });
      // this.props.refresh();
      this.props.navigation.navigate("QuestNewScreen");
    }
    if (oldProps.coach.pick_result !== this.props.coach.pick_result) {
      this.props.getCoachs();
      this.setState({
        progress: false
      })
    }
  }

  onSeek = seek => {
    this.videoPlayer.seek(seek);
  };
  onPaused = playerState => {
    this.setState({
      paused: !this.state.paused,
      playerState
    });
  };
  onReplay = () => {
    this.setState({ playerState: PLAYER_STATES.PLAYING });
    this.videoPlayer.seek(0);
  };
  onProgress = data => {
    const { isLoading, playerState } = this.state;
    // Video Player will continue progress even if the video already ended
    if (!isLoading && playerState !== PLAYER_STATES.ENDED) {
      this.setState({ currentTime: data.currentTime });
    }
  };
  onLoad = data => this.setState({ duration: data.duration, isLoading: false });
  onLoadStart = data => this.setState({ isLoading: true });
  onEnd = () => this.setState({ playerState: PLAYER_STATES.ENDED });
  onError = () => alert("Oh! ", error);
  exitFullScreen = () => {
    alert("Exit full screen");
  };
  enterFullScreen = () => { };
  onFullScreen = () => {
    if (this.state.screenType == "content")
      this.setState({ screenType: "cover" });
    else this.setState({ screenType: "content" });
  };

  renderVideo(data) {
    const video_url = data.video_url
    const thumbnail = data.thumbnail_url ? data.thumbnail_url : false
    const isYoutube = data.is_youtube_video ? data.is_youtube_video : false

    if (isYoutube) {
      if (this.state.youtubePlayerReady) {
        return (
          <View>
            <Text style={styles.welcomeTextBoldVideo}>Video Coaching</Text>
            <YouTube
              apiKey={'AIzaSyDRB9xT9wbJ9671RjH79AV-_eUTbo-KBkY'} // <= Todo: Pindah ke env
              videoId={video_url} // <= Youtube Video ID, Contoh: wOcCu31oi7Q
              play={false}
              fullscreen={this.state.fullscreen}
              onReady={e => this.setState({ isReady: true })}
              onChangeState={e => {
                if (e.state === 'playing') {
                  this.setState({ fullscreen: true })
                } else if (e.state === 'ended') {
                  this.setState({ fullscreen: false });
                }
              }}
              onChangeQuality={e => this.setState({ quality: e.quality })}
              onError={e => this.setState({ error: e.error })}
              style={{ height: Dimensions.get('screen').width / 1.75, width: Dimensions.get('screen').width * 0.8 }}
            />
          </View>
        )
      }
    } else {
      return (
        <View style={styles.coachingVideoContainer}>
          {video_url !== null && video_url !== undefined && video_url.length > 0 ?
            <View style={styles.contentDescriptionContainerVideo}>
              <Text style={styles.welcomeTextBoldVideo}>Video Coaching</Text>
              <TouchableOSResponsive onPress={() => this.props.navigation.navigate('VideoPlayerCoach')}>
                <View style={styles.videobox}>
                  {thumbnail && <Image style={styles.thumbnail} source={{ uri: thumbnail }} />}
                  <Icon
                    name="play"
                    size={50}
                    color="#cccccc"
                    style={{
                      position: 'absolute',
                      alignContent: "center",
                      textAlign: "center",
                      justifyContent: "center",
                      alignSelf: 'center',
                      marginTop: 40
                    }}
                  />
                  {!thumbnail &&
                    <Text
                      style={{
                        position: 'absolute',
                        alignContent: "center",
                        textAlign: "center",
                        justifyContent: "center",
                        alignSelf: 'center',
                        marginTop: 95,
                      }}
                    >
                      Click Here To Play Video
                      </Text>
                  }
                </View>
              </TouchableOSResponsive>

            </View>
            : (
              <View style={styles.ticketComponent}>
                <Text style={styles.welcomeTextBoldBottom}>You don't have any video coaching programme right now!</Text>
              </View>
            )}
        </View>
      )
    }
  }

  renderContent(detail) {
    const { coach } = this.props;

    if (detail === null)
      return (
        <View style={styles.loaderContainer}>
          <ActivityIndicator size={Platform.OS === "ios" ? 'large' : "large"} color={styles.spinner.color} />
        </View>
      );
    const {
      name,
      quest_start,
      quest_end,
      work_start,
      points,
      description,
      requirements,
      total_preq,
      durations,
      total_submits,
      submits,
      task_status,
      review_text,
      review_score,
      review_date,
      reviewed_by,
      review_expiry,
      points_rewarded,
      task_id,
      participant_task_id
    } = detail;
    return (
      <ScrollView>
        <View style={{ ...styles.contentContainer, marginTop: 50 }}>
          <Text style={styles.contentTitleText}>{name}</Text>
          <Text style={styles.contentTitleDate}>{quest_start}</Text>
          <View style={styles.contentSubContainer}>
            <Text style={styles.contentSubText}>POINT REWARDS</Text>
            <View style={styles.contentSubPointContainer}>
              <Text style={styles.contentPointValueText}>{points}</Text>
              <Text style={styles.contentPointValueText}>POINTS</Text>
            </View>
          </View>
          <View style={styles.contentSubContainer}>
            <Text style={styles.contentSubText}>START DATE</Text>
            <Text style={styles.contentSubDateText}>{work_start}</Text>
          </View>
          <View style={{ ...styles.contentSubContainer, marginBottom: 30 }}>
            <Text style={styles.contentSubText}>DEADLINE</Text>
            <Text style={styles.contentSubDateText}>{quest_end}</Text>
          </View>
          {/* <View style={{...styles.contentSubContainer, marginBottom: 30}}>
            <Text style={styles.contentSubText}>TIME LIMIT</Text>
            <Text style={styles.contentSubDateText}>{durations}</Text>
          </View> */}


          {/* <View style={styles.contentDescriptionContainerTop}>
            
          </View> */}
          <View style={styles.contentDescriptionContainerBottom}>
            {/* {this.state.video_player ? ( */}
            {
              coach.coach_programme !== null ? (
                <View style={styles.ticketsContainer}>
                  {this.renderVideo(this.props.coach.coach_programme)}
                </View>
              ) :
                null
              // (
              //   <View style={styles.ticketComponent}>
              //     <Text style={styles.welcomeTextBoldBottom}>You don't have any video coaching programme right now!</Text>
              //   </View>
              // )
            }
            <HTML
              html={description}
              imagesMaxWidth={Dimensions.get("window").width}

              close={() => {
                this.setState({ text_description: false });
              }}
            />
            {/* ) : null} */}
          </View>
          <Text style={styles.contentInstructionText}>INSTRUCTION(S)</Text>
          <View style={styles.contentInstructionContainer}>
            <HTMLView style={styles.contentInstructionDetailText} value={requirements} />
            <Text style={styles.contentInstructionDetailText}>
              ({total_submits}/{total_preq})
            </Text>
          </View>
          {task_status === 0 ? (
            <View style={styles.actionContainer}>
              <RoundedButton
                text="Close Quest"
                onPress={() => {
                  this.setState({
                    confirm_popup: true
                  });
                }}
              />
              <Text style={{ marginBottom: Metrics.doubleBaseMargin }}>
                If you think the work is done, then you can close the Quest. The
                points will be rewarded after reviewed by the Quest giver.
              </Text>
            </View>
          ) : null}

          {task_status === 1 ? (
            <View style={styles.actionContainer}>
              <Text>Your quest is being reviewed.</Text>
            </View>
          ) : null}
          {task_status === 2 ? (
            <View>
              <View
                style={{
                  ...styles.actionContainer,
                  flex: 1,
                  flexDirection: "column"
                }}
              >
                <Text
                  style={{
                    flex: 3,
                    marginTop: Metrics.baseMargin,
                    fontWeight: "bold"
                  }}
                >
                  POINTS REWARDED :
                </Text>
                <Text
                  style={{ flex: 1, color: "#fCAA0B", ...Fonts.style.title.h3 }}
                >
                  {points_rewarded}
                </Text>
              </View>
              <View
                style={{
                  ...styles.actionContainer,
                  marginTop: Metrics.baseMargin * 1,
                  flex: 1,
                  flexDirection: "column"
                }}
              >
                <Text
                  style={{
                    flex: 1,
                    marginTop: Metrics.baseMargin,
                    fontWeight: "bold"
                  }}
                >
                  REVIEW:
                </Text>
                <Text
                  style={{ flex: 3, color: "#fCAA0B", ...Fonts.style.title.h3 }}
                >
                  {review_text}
                </Text>
              </View>
            </View>
          ) : null}
          {task_status === 3 ? (
            <View
              style={{
                ...styles.actionContainer,
                backgroundColor: "#cc0000",
                textAlign: "center",
                alignContent: "center",
                flexDirection: "row",
                justifyContent: "space-between",
                paddingBottom: 20,
                borderRadius: 20
              }}
            >
              <Text
                style={{
                  flex: 1,
                  textAlign: "center",
                  flexDirection: "row",
                  color: "#ffffff",
                  fontWeight: "bold",
                  ...Fonts.style.title.h2
                }}
              >
                EXPIRED
              </Text>
            </View>
          ) : null}
          {task_status === 4 ? (
            <View
              style={{
                ...styles.actionContainer,
                backgroundColor: "#cc0000",
                textAlign: "center",
                alignContent: "center",
                flexDirection: "row",
                justifyContent: "space-between",
                paddingBottom: 20,
                borderRadius: 20
              }}
            >
              <Text
                style={{
                  flex: 1,
                  textAlign: "center",
                  flexDirection: "row",
                  color: "#ffffff",
                  fontWeight: "bold",
                  ...Fonts.style.title.h2
                }}
              >
                REJECTED
              </Text>
            </View>
          ) : null}
        </View>
      </ScrollView>
    );
  }

  render() {
    const { coach } = this.props;
    if (__DEV__) console.tron.log('props:', this.props);

    return (
      <View style={styles.container}>
        <View style={styles.headerContainer}/>
				<HeaderMastery navigation={this.props.navigation} title={"INFO"} />
				{this.renderContent(this.props.quest.detail_payload)}
        <View style={styles.bottomBarContainer}>
          <TouchableOSResponsive
            style={styles.bottomBarImageContainer}
            onPress={() => { }}
          >
            <Image style={styles.bottomBarImage} source={Images.infoIcon} />
            <Text style={styles.iconTitle}>INFO</Text>
          </TouchableOSResponsive>

          {((typeof this.props.quest.detail_payload !== "undefined") &&
            this.props.quest.detail_payload !== null &&
            this.props.quest.detail_payload.task_status === 0) ? (
              <TouchableOSResponsive
                style={styles.bottomBarImageContainer}
                onPress={() => {
                  this.props.navigation.navigate("QuestWorkspaceScreen", { taskid: this.props.quest.detail_payload });
                }}
              >
                <Image
                  style={styles.bottomBarImageDeactivated}
                  source={Images.workspaceIcon}
                />
                <Text style={styles.iconTitleDeactivated}>WORKSPACE</Text>
              </TouchableOSResponsive>
            ) : null}

          <TouchableOSResponsive
            style={styles.bottomBarImageContainer}
            onPress={() => {
              this.props.navigation.navigate("MyQuestFilesScreen");
            }}
          >
            <Image
              style={styles.bottomBarImageDeactivated}
              source={Images.filesIcon}
            />
            <Text style={styles.iconTitleDeactivated}>EVIDENCE</Text>
          </TouchableOSResponsive>
        </View>

        <CloseQuestConfirmPopup
          visible={this.state.confirm_popup}
          data={this.props.quest}
          progress={this.state.close_progress}
          confirm={() => {
            const { participant_task_id } = this.props.quest.detail_payload;
            this.setState({
              close_progress: true
            });
            this.props.closeQuest(participant_task_id);
          }}
          cancel={() => {
            this.setState({ confirm_popup: false });
          }}
          styles={styles}
        />
      </View>
    );
  }
}

const mapStateToProps = state => {
  const { quest, close_quest, coach } = state;
  return {
    quest,
    close_quest,
    coach
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getCoachs: data => dispatch(CoachingProgrammeActions.coachingProgrammeRequest(data)),
    refresh: () => dispatch(QuestActions.questRequest()),
    getQuest: id => dispatch(QuestActions.questDetailRequest(id)),
    closeQuest: id => dispatch(CloseQuestActions.closeQuestRequest({ id }))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MyQuestInfoScreen);
