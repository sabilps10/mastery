// a library to wrap and simplify api calls
import apisauce from "apisauce";
import Secrets from "react-native-config";
import { Platform } from "react-native";
import * as mime from "react-native-mime-types";

// our "constructor"
const create = (baseURL = Secrets.API_URL) => {
  // ------
  // STEP 1
  // ------
  //
  // Create and configure an apisauce-based api object.
  //
  const api = apisauce.create({
    // base URL is read from the "constructor"
    baseURL,
    // here are some default headers
    headers: {
      "Cache-Control": "no-cache"
    },
    // 10 second timeout...
    timeout: 10000
  });
  const withTokenBearer = token => {
    const options = {
      headers: {
        Authorization: `Bearer ${token}`
      }
    };

    return options;
  };
  const withUploadFileHeader = token => {
    const options = {
      headers: {
        Authorization: `Bearer ${token}`,
        "Content-Type": "multipart/form-data"
      }
    };

    return options;
  };

  const createFormData = (photo, body) => {
   
    //a little hack to avoid using the debugger FormData
    //global.FormData = global.originalFormData;
    const data = new FormData();

    let { fileName, type, uri, path } = photo;
    if (typeof fileName === "undefined") {
      let chunks = path.split("/");
      if (__DEV__) console.tron.log(chunks, chunks.length, chunks[chunks.length - 1]);
      fileName = chunks[chunks.length - 1];
    }

    if (Platform.OS === "android") {
      uri = "file://" + path;
    } else {
      uri = uri.replace("file://", "");
    }
    if (typeof type === "undefined") type = mime.lookup(path);
    data.append("docs", {
      name: fileName,
      type: type !== null ? type : "video/mp4",
      uri: uri
    });

    Object.keys(body).forEach(key => {
      if (key === "description") body[key] = fileName;
      data.append(key, body[key]);
    });
    if (__DEV__) console.tron.log(data);

    return data;
  };

  const createProfilePhoto = (photo, body) => {

    //a little hack to avoid using the debugger FormData
    //global.FormData = global.originalFormData;
    const data = new FormData();

    let { fileName, type, uri, path } = photo;
    if (typeof fileName === "undefined") {
      let chunks = path.split("/");
      if (__DEV__) console.tron.log(chunks, chunks.length, chunks[chunks.length - 1]);
      fileName = chunks[chunks.length - 1];
    }

    if (Platform.OS === "android") {
      uri = "file://" + path;
    } else {
      uri = uri.replace("file://", "");
    }
    if (typeof type === "undefined") type = mime.lookup(path);
    data.append("file", {
      name: fileName,
      type: type !== null ? type : "video/mp4",
      uri: uri
    });

    Object.keys(body).forEach(key => {
      if (key === "description") body[key] = fileName;
      data.append(key, body[key]);
    });
    if (__DEV__) console.tron.log(data);

    return data;
  };



  const createFormDoc = (doc, body) => {
    const data = new FormData();
    let { name, type, uri } = doc;
    if (typeof name === "undefined") {
      let chunks = uri.split("/");
      if (__DEV__) console.tron.log(chunks, chunks.length, chunks[chunks.length - 1]);
      name = chunks[chunks.length - 1];
    }

    if (typeof type === "undefined") type = mime.lookup(uri);
    data.append("docs", {
      name: name,
      type: type !== null ? type : "application/pdf",
      uri: uri
    });

    Object.keys(body).forEach(key => {
      if (key === "description") body[key] = name;
      data.append(key, body[key]);
    });
    if (__DEV__) console.tron.log(data);
    return data;
  };

  const createFormAudio = (audio, body) => {
    const data = new FormData();
    let { name, type, uri } = audio;

    if (typeof name === "undefined") {
      let chunks = uri.split("/");
      if (__DEV__) console.tron.log(chunks, chunks.length, chunks[chunks.length - 1]);
      name = chunks[chunks.length - 1];
    }

    if (typeof type === "undefined") type = mime.lookup(uri);
    data.append("docs", {
      name: name,
      type: type !== null ? type : "audio/mp4",
      uri: uri
    });

    Object.keys(body).forEach(key => {
      if (key === "description") body[key] = name;
      data.append(key, body[key]);
    });
    if (__DEV__) console.tron.log(data);
    return data;
  };
  // ------
  // STEP 2
  // ------
  //
  // Define some functions that call the api.  The goal is to provide
  // a thin wrapper of the api layer providing nicer feeling functions
  // rather than "get", "post" and friends.
  //
  // I generally don't like wrapping the output at this level because
  // sometimes specific actions need to be take on `403` or `401`, etc.
  //
  // Since we can't hide from that, we embrace it by getting out of the
  // way at this level.
  //
  const getRoot = () => api.get("");
  const getRate = () => api.get("rate_limit");
  const getUser = username => api.get("search/users", { q: username });
  const login = input => {
    if (__DEV__) console.tron.log("api.login", input);
    return api.post("/login", input);
  };
  const register = input => {
    if (__DEV__) console.tron.log("api.register", input);
    return api.post("/register", input);
  };
  const forgetpassword = input => {
    if (__DEV__) console.tron.log("api.forgetpassword", input);
    return api.post("/reset/send_link", input);
  };

  const getProfileDetail = ({ id, access_token }) =>
    api.get("/users/" + id, {}, withTokenBearer(access_token));

  const editProfile = ({ data, access_token }) =>
    api.post(
      "/users/update/",
      data,
      withTokenBearer(access_token)
    )
  // const getCoach = data =>
  //   api.get("coach_programme/list?start=0&limit=10", data, withTokenBearer(access_token));

  const getCoachs = ({ data, access_token }) =>
    api.get("/coach_programme/list?start=0&limit=100", data, withTokenBearer(access_token));
  const getCoachDetail = ({ id, access_token }) =>
    api.get("/coach_programme/detail/" + id, {}, withTokenBearer(access_token));
  const pickCoach = ({ id, access_token }) =>
    api.post("/coach_programme/join/", { id: id }, withTokenBearer(access_token));
  const subscribeCoach = ({ id, access_token }) =>
    api.post("/subscribe/" + id + "/process", { product_type: "coaching" }, withTokenBearer(access_token));

  // const subscribeCoach = ({ id, access_token }) =>
  // api.post("/quest/" + id + "/subscribe", {}, withTokenBearer(access_token));

  //HOME STATS
  const getNewHome = data =>
    api.get("/home", null, withTokenBearer(data.access_token));
  const getHome = data =>
    api.get("/user/summaries", null, withTokenBearer(data.access_token));
  //QUESTS
  const getQuests = ({ data, access_token }) =>
    api.get("/quests", data, withTokenBearer(access_token));

  //ongoing quest detail
  //@params id participant_task_id
  const getQuestDetail = ({ id, access_token }) =>
    api.get("/quest/ongoing/" + id, {}, withTokenBearer(access_token));

  const pickQuest = ({ id, access_token }) =>
    api.post("/quest/" + id + "/pick", {}, withTokenBearer(access_token));

  //WORKSPACE
  const getPosts = ({ id, access_token }) =>
    api.get("/workspace/posts/" + id, {}, withTokenBearer(access_token));
  const getChats = ({ id, access_token }) =>
    api.get("/workspace/posts/:task_id?start=0&limit=20" + id, {}, withTokenBearer(access_token));

  const reply = ({ data, access_token }) =>
    api.post(
      "/workspace/posts/" + data.id + "/reply",
      data,
      withTokenBearer(access_token)
    );
  const getPhoto = ({ data, access_token }) => {
    let formData = createFormData(data.file, data.body);

    return api.post(
      "/workspace/" + data.id + "/upload",
      formData,
      withTokenBearer(access_token)
    );
  }
  const getProfilePhoto = ({ data, access_token }) => {
    let formData = createProfilePhoto(data.file, data.body);

    return api.post(
      "/media/upload",
      formData,
      withTokenBearer(access_token)
    );
  }
  const sendProof = ({ data, access_token }) => {
    if (__DEV__) console.tron.log("sendProof:", data);
    let formData = createFormData(data.file, data.body);
    if (__DEV__) console.tron.log("formdata", formData);

    return api.post(
      "/proof/" + data.id + "/upload",
      formData,
      withUploadFileHeader(access_token)
    );
  };

  const sendDoc = ({ data, access_token }) => {
    if (__DEV__) console.tron.log("sendDoc:", data);
    let formData = createFormDoc(data.file, data.body);
    if (__DEV__) console.tron.log('formData : ' + formData);

    return api.post(
      "/proof/" + data.id + "/upload",
      formData,
      withUploadFileHeader(access_token)
    );
  };

  const sendAudio = ({ data, access_token }) => {
    if (__DEV__) console.tron.log("sendAudio:", data);
    let formData = createFormAudio(data.file, data.body);
    if (__DEV__) console.tron.log(formData);

    return api.post(
      "/proof/" + data.id + "/upload",
      formData,
      withUploadFileHeader(access_token)
    );
  };

  const getevidence = ({ data, access_token }) =>
    api.get(
      "/workspace/submits/" + data.id,
      data,
      withTokenBearer(access_token)
    );
  const getreward = ({ data, access_token }) =>
    api.get(
      "/rewards/" + data.company_id,
      data,
      withTokenBearer(access_token)
    );
  const getredeem = ({ data, access_token }) =>
    api.get(
      "/redeems/list",
      data,
      withTokenBearer(access_token)
    );
  const pickReward = ({ id, access_token }) =>
    api.post("/redeems/rewards/" + id, {}, withTokenBearer(access_token));

  const getCloseQuest = ({ data, access_token }) =>
    api.post(
      "/quest/" + data.id + "/done",
      data,
      withTokenBearer(access_token)
    );

  const UpdateFCM = ({ data, access_token }) => {
    api.post(
      "/users/update",
      { "firebase_uid": data }, //masukan datanya
      withTokenBearer(access_token)
    );
  }
  // ------
  // STEP 3
  // ------
  //
  // Return back a collection of functions that we would consider our
  // interface.  Most of the time it'll be just the list of all the
  // methods in step 2.
  //
  // Notice we're not returning back the `api` created in step 1?  That's
  // because it is scoped privately.  This is one way to create truly
  // private scoped goodies in JavaScript.
  //
  return {
    // a list of the API functions from step 2
    UpdateFCM,
    getRoot,
    getRate,
    getUser,
    login,
    register,
    getCoachs,
    getCoachDetail,
    pickCoach,
    subscribeCoach,
    forgetpassword,
    getNewHome,
    getHome,
    getProfileDetail,
    editProfile,
    getQuests,
    getQuestDetail,
    pickQuest,
    getPosts,
    getChats,
    getPhoto,
    getProfilePhoto,
    reply,
    sendProof,
    sendDoc,
    sendAudio,
    getevidence,
    getCloseQuest,
    getreward,
    getredeem,
    pickReward
  };
};

// let's return back our create method as the default.
export default {
  create
};
