import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  homeRequest: ['data'],
  homeSuccess: ['payload'],
  homeFailure: ['status']
})

export const HomeTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  data: null,
  fetching: null,
  status:200,
  payload: {
    stats:{
      ponts_left:0,
      outstandings: 0,
      completed: 0,
      missed: 0,
      total_points: 0,
      total_redeemed: 0,
    }
  },
  error: null
})

/* ------------- Selectors ------------- */

export const HomeSelectors = {
  getData: state => state.data,
  getPayload: state => state.payload,
}

/* ------------- Reducers ------------- */

// request the data from an api
export const request = (state, { data }) =>
  state.merge({ fetching: true, data, status:200 })

// successful api lookup
export const success = (state, action) => {
  const { payload } = action
  return state.merge({ fetching: false, error: null, payload, status:200 })
}

// Something went wrong somewhere.
export const failure = (state, {status}) =>
  state.merge({ fetching: false, error: true, payload: null,status })

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.HOME_REQUEST]: request,
  [Types.HOME_SUCCESS]: success,
  [Types.HOME_FAILURE]: failure
})
