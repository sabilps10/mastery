import { createReducer, createActions } from "reduxsauce";
import Immutable from "seamless-immutable";

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  loginRequest: ["data"],
  loginSuccess: ["payload"],
  loginFailure: null,
  isLoggedIn: null,
  autoLogin: null,
  logout: null
});

export const LoginTypes = Types;
export default Creators;

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  data: null,
  fetching: null,
  payload: null,
  error: null,
  access_token: null,
  loggedInUser: null
});

/* ------------- Selectors ------------- */

export const LoginSelectors = {
  getData: state => state.data,
  getAccessToken: (state) => {
    return state.login.access_token
  }
};

/* ------------- Reducers ------------- */

// request the data from an api
export const request = (state, { data }) => {
  if(__DEV__) console.tron.log('loginRequest:', data);
  return state.merge({ fetching: true, data, payload: null });
}
  

// successful api lookup
export const success = (state, action) => {
  const { payload } = action;
  const { access_token, user } = payload;
  return state.merge({
    fetching: false,
    error: null,
    payload,
    access_token,
    loggedInUser: user
  });
};

// we've logged out
export const logout = state => INITIAL_STATE;

// startup saga invoked autoLogin
export const autoLogin = state => state;

// Something went wrong somewhere.
export const failure = state =>
  state.merge({ fetching: false, error: true, payload: null });

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.LOGIN_REQUEST]: request,
  [Types.LOGIN_SUCCESS]: success,
  [Types.LOGIN_FAILURE]: failure,
  [Types.LOGOUT]: logout,
  [Types.AUTO_LOGIN]: autoLogin
});
