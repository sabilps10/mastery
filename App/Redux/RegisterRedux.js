import { createReducer, createActions } from "reduxsauce";
import Immutable from "seamless-immutable";

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  registerRequest: ["data"],
  registerSuccess: ["payload"],
  registerFailure: null,
  isRegisteredIn: null,
});

export const RegisterTypes = Types;
export default Creators;

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  data: null,
  fetching: null,
  payload: null,
  error: null,
  access_token: null,
  RegisteredInUser: null
});

/* ------------- Selectors ------------- */

export const RegisterSelectors = {
  getData: state => state.data,
  getAccessToken: (state) => {
    return state.register.access_token
  }
};

/* ------------- Reducers ------------- */

// request the data from an api
export const request = (state, { data }) => {
  if(__DEV__) console.tron.log('registerRequest:', data);
  return state.merge({ fetching: true, data, payload: null });
}
  

// successful api lookup
export const success = (state, action) => {
  const { payload } = action;
  const { access_token, user } = payload;
  return state.merge({
    fetching: false,
    error: null,
    payload,
    access_token,
    RegisteredInUser: user
  });
};

// Something went wrong somewhere.
export const failure = state =>
  state.merge({ fetching: false, error: true, payload: null });

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.REGISTER_REQUEST]: request,
  [Types.REGISTER_SUCCESS]: success,
  [Types.REGISTER_FAILURE]: failure,
});
