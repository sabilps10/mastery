import { createReducer, createActions } from "reduxsauce";
import Immutable from "seamless-immutable";

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  questRequest: ["data"],
  questSuccess: ["payload"],
  questFailure: ["status"],
  questDetailId: ["id"],
  questDetailRequest: ["id"],
  questDetailSuccess: ["detail_payload"],
  pickQuest: ["id"],
  pickQuestSuccess: ["pick_result"],
  pickQuestFailure: ["status"]
});

export const QuestTypes = Types;
export default Creators;

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  data: null,
  id: 0,
  fetching: null,
  payload: null,
  detail_payload: null,
  error: null,
  status: 200,
  pick_result: null
});

/* ------------- Selectors ------------- */

export const QuestSelectors = {
  getData: state => state.data,
  getPayload: state => state.payload,
  getDetail: state => state.detail_payload,
  getId: state => state.id
};

/* ------------- Reducers ------------- */

// request the data from an api
export const request = (state, { data }) =>
  state.merge({ fetching: true, data, payload: null, pick_result:null, status: 200 });

// successful api lookup
export const success = (state, action) => {
  const { payload } = action;
  return state.merge({ fetching: false, error: null, payload, status: 200 });
};

export const detail_request = (state, {id}) =>
  state.merge({ fetching: true, id, detail_payload: null, status: 200 });

// successful api lookup
export const detail_success = (state, { detail_payload }) => {
  return state.merge({
    fetching: false,
    error: null,
    detail_payload,
    status: 200
  });
};
export const set_detail_id = (state, action) => {
  const { id } = action;

  return state.merge({ id, status: 200 });
};
// Something went wrong somewhere.
export const failure = (state, { status }) =>
  state.merge({ fetching: false, error: true, payload: null, status });

// request the data from an api
export const pick_quest = (state, id) =>
  state.merge({ fetching: true, id, pick_result: null, status: 200 });

// successful api lookup
export const pick_quest_success = (state, action) => {
  const { pick_result } = action;
  return state.merge({
    fetching: false,
    error: null,
    pick_result,
    status: 200
  });
};
export const pick_quest_failure = (state, { status }) =>
  state.merge({ fetching: false, error: true, pick_result: null, status });

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.QUEST_REQUEST]: request,
  [Types.QUEST_SUCCESS]: success,
  [Types.QUEST_FAILURE]: failure,
  [Types.QUEST_DETAIL_REQUEST]: detail_request,
  [Types.QUEST_DETAIL_SUCCESS]: detail_success,
  [Types.QUEST_DETAIL_ID]: set_detail_id,
  [Types.PICK_QUEST]: pick_quest,
  [Types.PICK_QUEST_SUCCESS]: pick_quest_success,
  [Types.PICK_QUEST_FAILURE]: pick_quest_failure
});
