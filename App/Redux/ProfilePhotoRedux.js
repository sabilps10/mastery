import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  profilePhotoRequest: ['data'],
  profilePhotoSuccess: ['payload', 'file_url'],
  profilePhotoFailure: null
})

export const ProfilePhotoTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  data: null,
  fetching: null,
  payload: null,
  error: null,
  file_url: null
})

/* ------------- Selectors ------------- */

export const SendPhotoSelectors = {
  getData: state => state.data
}

/* ------------- Reducers ------------- */

// request the data from an api
export const request = (state, { data }) =>
  state.merge({ fetching: true, data, payload: null, file_url: null })

// successful api lookup
export const success = (state, action) => {
  const { payload, file_url } = action
  return state.merge({ fetching: false, error: null, payload, file_url })
}

// Something went wrong somewhere.
export const failure = state =>
  state.merge({ fetching: false, error: true, payload: null, file_url: null })

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.PROFILE_PHOTO_REQUEST]: request,
  [Types.PROFILE_PHOTO_SUCCESS]: success,
  [Types.PROFILE_PHOTO_FAILURE]: failure
})
