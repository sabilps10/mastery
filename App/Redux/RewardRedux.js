import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  rewardRequest: ['data'],
  rewardSuccess: ['payload'],
  rewardFailure: null,
  pickReward: ["id"],
  pickRewardSuccess: ["pick_result"],
  pickRewardFailure: ["status"]
})

export const RewardTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  data: null,
  fetching: null,
  payload: null,
  error: null,
  id: 0,
  status: 200,
  pick_result: null
})

/* ------------- Selectors ------------- */

export const RewardSelectors = {
  getData: state => state.data,
  getId: state => state.id
}

/* ------------- Reducers ------------- */

// request the data from an api
export const request = (state, { data }) =>
  state.merge({ fetching: true, data, payload: null })

// successful api lookup
export const success = (state, action) => {
  const { payload } = action
  return state.merge({ fetching: false, error: null, payload })
}

// Something went wrong somewhere.
export const failure = state =>
  state.merge({ fetching: false, error: true, payload: null })

export const pick_reward = (state, id) =>
  state.merge({ fetching: true, id, pick_result: null, status: 200 });

// successful api lookup
export const pick_reward_success = (state, action) => {
  const { pick_result } = action;
  return state.merge({
    fetching: false,
    error: null,
    pick_result,
    status: 200
  });
};
export const pick_reward_failure = (state, { status }) =>
  state.merge({ fetching: false, error: true, pick_result: null, status });
/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.REWARD_REQUEST]: request,
  [Types.REWARD_SUCCESS]: success,
  [Types.REWARD_FAILURE]: failure,
  [Types.PICK_REWARD]: pick_reward,
  [Types.PICK_REWARD_SUCCESS]: pick_reward_success,
  [Types.PICK_REWARD_FAILURE]: pick_reward_failure
})
