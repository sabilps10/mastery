import { createReducer, createActions } from "reduxsauce";
import Immutable from "seamless-immutable";

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  coachingProgrammeRequest: ["data"],
  coachingProgrammeSuccess: ["payload"],
  coachingProgrammeFailure: ["status"],
  coachingProgrammeDetailId: ["id"],
  coachingProgrammeDetailRequest: ["id"],
  coachingProgrammeDetailSuccess: ["coach_programme", "programme_subscribers"],
  pickCoachingProgramme: ["id"],
  pickCoachingProgrammeSuccess: ["pick_result"],
  pickCoachingProgrammeFailure: ["status"],
  subscribeCoachingProgramme: ["id"],
  subscribeCoachingProgrammeSuccess: ["payment_url"],
  subscribeCoachingProgrammeFailure: ["status"]
});



export const CoachingProgrammeTypes = Types;
export default Creators;

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  data: null,
  id: 0,
  fetching: null,
  payload: null,
  coach_programme: null,
  error: null,
  status: 200,
  pick_result: null,
  payment_url: null,
  programme_subscribers: null,
});

/* ------------- Selectors ------------- */

export const CoachingProgrammeSelectors = {
  getData: state => state.data,
  getPayload: state => state.payload,
  getDetail: state => state.coach_programme,
  getId: state => state.id
};

/* ------------- Reducers ------------- */


export const request = (state, { data }) =>
  state.merge({ fetching: true, data, payload: null, pick_result:null, status: 200 });


export const success = (state, action) => {
  const { payload } = action;
  return state.merge({ fetching: false, error: null, payload, status: 200 });
};
  
export const detail_request = (state, {id}) =>
  state.merge({ fetching: true, id, coach_programme: null, programme_subscribers: null, status: 200 });


export const detail_success = (state, action ) => {
  const {coach_programme, programme_subscribers} = action;
  return state.merge({
    fetching: false,
    error: null,
    coach_programme,
    programme_subscribers,
    status: 200,
  });
};
export const set_detail_id = (state, action) => {
  const { id } = action;

  return state.merge({ id, status: 200 });
};
export const failure = (state, { status }) =>
  state.merge({ fetching: false, error: true, payload: null, status });


export const pick_coach = (state, id) =>
  state.merge({ fetching: true, id, pick_result: null, status: 200 });


export const pick_coach_success = (state, action) => {
  const { pick_result } = action;
  return state.merge({
    fetching: false,
    error: null,
    pick_result,
    status: 200,
  });
};
export const pick_coach_failure = (state, { status }) =>
  state.merge({ fetching: false, error: true, pick_result: null, status });


export const subscribe_coach = (state, id) =>
  state.merge({ fetching: true, id, status: 200, payment_url: null });


export const subscribe_coach_success = (state, action) => {
  const { payment_url } = action;
  return state.merge({
    fetching: false,
    error: null,
    status: 200,
    payment_url,
  });
};
export const subscribe_coach_failure = (state, { status }) =>
  state.merge({ fetching: false, error: true, status, payment_url: null });

/* ------------- Hookup Reducers To Types ------------- */
export const reducer = createReducer(INITIAL_STATE, {
  [Types.COACHING_PROGRAMME_REQUEST]: request,
  [Types.COACHING_PROGRAMME_SUCCESS]: success,
  [Types.COACHING_PROGRAMME_FAILURE]: failure,
  [Types.COACHING_PROGRAMME_DETAIL_REQUEST]: detail_request,
  [Types.COACHING_PROGRAMME_DETAIL_SUCCESS]: detail_success,
  [Types.COACHING_PROGRAMME_DETAIL_ID]: set_detail_id,
  [Types.PICK_COACHING_PROGRAMME]: pick_coach,
  [Types.PICK_COACHING_PROGRAMME_SUCCESS]: pick_coach_success,
  [Types.PICK_COACHING_PROGRAMME_FAILURE]: pick_coach_failure,
  [Types.SUBSCRIBE_COACHING_PROGRAMME]: subscribe_coach,
  [Types.SUBSCRIBE_COACHING_PROGRAMME_SUCCESS]: subscribe_coach_success,
  [Types.SUBSCRIBE_COACHING_PROGRAMME_FAILURE]: subscribe_coach_failure
});
