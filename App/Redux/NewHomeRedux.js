import { createReducer, createActions } from "reduxsauce";
import Immutable from "seamless-immutable";

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  newHomeRequest: ["data"],
  newHomeSuccess: ["quests", "programmes", "banners", "profile"],
  newHomeFailure: ["status"],
});



export const NewHomeTypes = Types;
export default Creators;

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  data: null,
  id: 0,
  fetching: null,
  quests: null,
  programmes: null,
  banners: null,
  profile: null,
  error: null,
  status: 200,
});

/* ------------- Selectors ------------- */

export const NewHomeSelectors = {
  getData: state => state.data,
  getQuests: state => state.quests,
  getProgrammes: state => state.programmes,
  getBanners: state => state.banners,
  getProfile: state => state.profile,
  getId: state => state.id
};

/* ------------- Rezsducers ------------- */

export const request = (state, { data }) =>
  state.merge({ fetching: true, data, quests: null, programmes: null, banners: null, profile: null, status: 200 });


export const success = (state, action) => {
  const { quests, programmes, banners, profile } = action;
  return state.merge({ fetching: false, error: null, quests, programmes, banners, profile, status: 200 });
};

export const failure = (state, { status }) =>
  state.merge({ fetching: false, error: true, quests: null, programmes: null, banners: null, profile: null, status });


/* ------------- Hookup Reducers To Types ------------- */
export const reducer = createReducer(INITIAL_STATE, {
  [Types.NEW_HOME_REQUEST]: request,
  [Types.NEW_HOME_SUCCESS]: success,
  [Types.NEW_HOME_FAILURE]: failure,
});
