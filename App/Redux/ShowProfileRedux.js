import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  profileDetailId: ["id"],
  profileDetailRequest: ["id"],
  profileDetailSuccess: ["detail_payload"],
})

export const ShowProfileTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  id: 0,
  fetching: null,
  detail_payload: null,
  error: null
})

/* ------------- Selectors ------------- */

export const ShowProfileSelectors = {
  getData: state => state.data,
  getPayload: state => state.payload,
  getId: state => state.id
}

/* ------------- Reducers ------------- */

export const detail_request = (state, { id }) =>
  state.merge({ fetching: true, id, payload: null, status: 200 });

// successful api lookup
export const detail_success = (state, { detail_payload }) => {
  return state.merge({
    fetching: false,
    error: null,
    detail_payload,
    status: 200
  });
};
export const set_detail_id = (state, action) => {
  const { id } = action;

  return state.merge({ id, status: 200 });
};


/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.PROFILE_DETAIL_REQUEST]: detail_request,
  [Types.PROFILE_DETAIL_SUCCESS]: detail_success,
  [Types.PROFILE_DETAIL_ID]: set_detail_id,
  
})
