import { combineReducers } from 'redux'
import { persistReducer } from 'redux-persist'
import configureStore from './CreateStore'
import rootSaga from '../Sagas/'
import ReduxPersist from '../Config/ReduxPersist'

/* ------------- Assemble The Reducers ------------- */
if(__DEV__) console.tron.log("masuk Index Redux");


export const reducers = combineReducers({
  nav: require('./NavigationRedux').reducer,
  github: require('./GithubRedux').reducer,
  search: require('./SearchRedux').reducer,
  login: require('./LoginRedux').reducer,
  register: require('./RegisterRedux').reducer,
  forgetpassword: require('./ForgetPasswordRedux').reducer,
  home: require('./HomeRedux').reducer,
  newhome: require('./NewHomeRedux').reducer,
  quest: require('./QuestRedux').reducer,
  coach: require('./CoachingProgrammeRedux').reducer,
  profile: require('./ShowProfileRedux').reducer,
  edit_profile: require('./EditProfileRedux').reducer,
  posts: require('./PostsRedux').reducer,
  chats: require('./ChatsRedux').reducer,
  send_posts: require('./SendPostsRedux').reducer,
  send_photo: require('./SendPhotoRedux').reducer,
  profile_photo: require('./ProfilePhotoRedux').reducer,
  send_proof: require('./SendProofRedux').reducer,
  send_doc: require('./SendDocRedux').reducer,
  send_audio: require('./SendAudioRedux').reducer,
  evidence: require('./EvidenceRedux').reducer,
  close_quest: require('./CloseQuestRedux').reducer,
  appreciation: require('./AppreciationRedux').reducer,
  newsletter: require('./NewsletterRedux').reducer,
  redeem: require('./RedeemRedux').reducer,
  reward: require('./RewardRedux').reducer,
  update_fcm: require('./UpdateFCMRedux').reducer, //reducer untuk update_profile
})

export default () => {
  let finalReducers = reducers
  // If rehydration is on use persistReducer otherwise default combineReducers
  if (ReduxPersist.active) {
    const persistConfig = ReduxPersist.storeConfig
    finalReducers = persistReducer(persistConfig, reducers)
  }

  let { store, sagasManager, sagaMiddleware } = configureStore(finalReducers, rootSaga)

  if (module.hot) {
    module.hot.accept(() => {
      const nextRootReducer = require('./').reducers
      store.replaceReducer(nextRootReducer)

      const newYieldedSagas = require('../Sagas').default
      sagasManager.cancel()
      sagasManager.done.then(() => {
        sagasManager = sagaMiddleware(newYieldedSagas)
      })
    })
  }

  return store
}
