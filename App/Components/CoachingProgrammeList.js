import React, { useState } from "react";
import {ActivityIndicator, Platform} from "react-native";
import { ScrollView, View, Text } from "react-native";
import CoachingProgrammeListItem from "./CoachingProgrammeListItem";

export default function CoachingProgrammeList(props) {
  if(__DEV__) console.tron.log(props.data);
  const { styles } = props;
  if(props.progress) return (
    <View style={styles.message}>
    <ActivityIndicator size={Platform.OS === "ios" ? 'large' : 30}></ActivityIndicator>
  </View>
  )
  if (typeof props.data === 'undefined' || props.data === 0 || props.data.length === 0) return (
    <View style={styles.message}>
      <Text style={styles.messageText}>
        Hooray, no more coaching programme at the moment !
      </Text>
    </View>
  )
  if(typeof props.data !== 'undefined'){
    return (
      <ScrollView style={styles.questsContainer}>
        {props.data.map((data, index) => {
          return (
            <CoachingProgrammeListItem
              key={index}
              {...props}
              data={data}
              clicked={id => {
                props.onclick(id)
              }}
            />
          );
        })}
      </ScrollView>
    );
  }
  else {
    <View></View>
  }
}
