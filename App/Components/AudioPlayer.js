import React, { Component } from "react";
import { Platform, StyleSheet, Text, View, BackHandler } from "react-native";
import SoundPlayer from 'react-native-sound-player';
import TouchableOSResponsive from '../Components/MaterialUi/TouchableOsResponsive'

class AudioPlayer extends Component {
  _onFinishedPlayingSubscription = null
  _onFinishedLoadingSubscription = null
  _onFinishedLoadingFileSubscription = null
  _onFinishedLoadingURLSubscription = null

  constructor(props) {
    super(props)
    this.state = {
      trigger: false,
    }
    this.handleBack = this.handleBack.bind(this);
  }

  componentWillMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBack);
    if(__DEV__) console.tron.log('selesai')
  }

  handleBack() {
    this.props.navigation.goBack(null);
    SoundPlayer.stop(this.props.uri)
      this.setState({trigger:false})
      return true;
  }
  

  // Subscribe to event(s) you want when component mounted
  componentDidMount() {
    _onFinishedPlayingSubscription = SoundPlayer.addEventListener('FinishedPlaying', ({ success }) => {
      if(__DEV__) console.tron.log('finished playing', success)
    })
    _onFinishedLoadingSubscription = SoundPlayer.addEventListener('FinishedLoading', ({ success }) => {
      if(__DEV__) console.tron.log('finished loading', success)
    })
    _onFinishedLoadingFileSubscription = SoundPlayer.addEventListener('FinishedLoadingFile', ({ success, name, type }) => {
      if(__DEV__) console.tron.log('finished loading file', success, name, type)
    })
    _onFinishedLoadingURLSubscription = SoundPlayer.addEventListener('FinishedLoadingURL', ({ success, url }) => {
      if(__DEV__) console.tron.log('finished loading url', success, url)
    })
    
  }

  // Remove all the subscriptions when component will unmount
  componentWillUnmount() {
    _onFinishedPlayingSubscription.remove()
    _onFinishedLoadingSubscription.remove()
    _onFinishedLoadingURLSubscription.remove()
    _onFinishedLoadingFileSubscription.remove()
    BackHandler.removeEventListener('hardwareBackPress', this.handleBack);
  }

  playSong() {
    try {
      SoundPlayer.playUrl(this.props.uri)
      this.setState({ trigger: true })
      if(__DEV__) console.tron.log("state triger=" + this.state.trigger);
      if(__DEV__) console.tron.log("props audio=" + this.props.uri);
    } catch (e) {
      alert('Cannot play the file')
      if(__DEV__) console.tron.log('cannot play the song file', e)
      if(__DEV__) console.tron.log("error=" + e);
    }
  }

  pauseSong() {
    try {
      SoundPlayer.pause(this.props.uri)
      this.setState({trigger:false})
      if(__DEV__) console.tron.log("props audio=" + this.props.uri);
    } catch (e) {
      alert('Cannot play the file')
      if(__DEV__) console.tron.log('cannot play the song file', e)
      if(__DEV__) console.tron.log("error=" + e);
    }
  }

  stopSong() {
    try {
      SoundPlayer.stop()
      this.setState({trigger:false})
      if(__DEV__) console.tron.log("props audio=" + this.props.uri);
    } catch (e) {
      alert('Cannot play the file')
      if(__DEV__) console.tron.log('cannot play the song file', e)
      if(__DEV__) console.tron.log("error=" + e);
    }
  }

  async getInfo() { // You need the keyword `async`
    try {
      const info = await SoundPlayer.getInfo() // Also, you need to await this because it is async
      if(__DEV__) console.tron.log('getInfo', info) // {duration: 12.416, currentTime: 7.691}
    } catch (e) {
      if(__DEV__) console.tron.log('There is no song playing', e)
    }
  }
  onPressPlayButton() {
    this.playSong()
    this.getInfo()
  }

  renderToolbar = () => (
    <View style={{ flex: 1, marginTop: 100 }}>
      <TouchableOsResponsive
        onPress={() => {
          this.setState({
            paused: true
          });
          this.props.close();
        }}
      >
        <View style={{
          alignItems: 'flex-end'
        }}>
          <Icon name="close" size={30} color="#ffffff" />
        </View>
      </TouchableOsResponsive>
    </View>
  );

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.text}>{this.getInfo}</Text>
        {!this.state.trigger ? (
          <TouchableOSResponsive style={styles.loginButton} onPress={() => this.playSong()}>
            <Text style={styles.loginText}>Play</Text>
          </TouchableOSResponsive>
        ) : (
            <View>
              <TouchableOSResponsive style={styles.loginButton} onPress={() => this.stopSong()}>
                <Text style={styles.loginText}>Stop</Text>
              </TouchableOSResponsive>
            </View>
          )}

      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    position: "absolute",
    width: "100%",
    height: "100%",
    backgroundColor: '#808080',
    justifyContent: 'center',
    alignItems: 'center'
  },
  toolbar: {
    marginTop: 30,
    backgroundColor: "white",
    padding: 10,
    borderRadius: 5
  },
  mediaPlayer: {
    position: "absolute",
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    backgroundColor: "black"
  },
  text: {
    color: 'black',
    fontSize: 20
  },
  loginButton: {
    width: 130,
    height: 70,
    borderWidth: 1,
    borderRadius: 15,
    alignSelf: 'center',
    justifyContent: 'center',
    marginTop: 30,
    marginBottom: 80,
    backgroundColor: 'blue'
  },
  loginText: {
    color: 'white',
    alignSelf: 'center',
    fontWeight: 'bold'
  },
});
export default AudioPlayer;
