import React, { Component } from "react";
import { Platform, StyleSheet, Text, View } from "react-native";
import Icon from "react-native-vector-icons/FontAwesome";
import { connect } from 'react-redux'
import Video from "react-native-video";
import MediaControls, { PLAYER_STATES } from "react-native-media-controls";
import { TouchableWithoutFeedback } from "react-native-gesture-handler";
import TouchableOsResponsive from "./MaterialUi/TouchableOsResponsive";
import RoundedButton from "./RoundedButton";
class VideoPlayerCoach extends Component {
  videoPlayer;
  constructor(props) {
    super(props);
    this.state = {
      currentTime: 0,
      duration: 0,
      isFullScreen: true,
      isLoading: true,
      paused: false,
      playerState: PLAYER_STATES.PLAYING,
      screenType: "cover"
    };
    if (__DEV__) console.tron.log("video_url ", this.props.coach.coach_programme.video_url);
    
  }
  onSeek = seek => {
    this.videoPlayer.seek(seek);
  };
  onPaused = playerState => {
    this.setState({
      paused: !this.state.paused,
      playerState
    });
  };
  onReplay = () => {
    this.setState({ playerState: PLAYER_STATES.PLAYING });
    this.videoPlayer.seek(0);
  };
  onProgress = data => {
    const { isLoading, playerState } = this.state;
    // Video Player will continue progress even if the video already ended
    if (!isLoading && playerState !== PLAYER_STATES.ENDED) {
      this.setState({ currentTime: data.currentTime });
    }
  };
  onLoad = data => this.setState({ duration: data.duration, isLoading: false });
  onLoadStart = data => this.setState({ isLoading: true });
  onEnd = () => this.setState({ playerState: PLAYER_STATES.ENDED });
  onError = () => alert("Oh! ", error);
  exitFullScreen = () => {
    alert("Exit full screen");
  };
  enterFullScreen = () => {};
  onFullScreen = () => {
    if (this.state.screenType == "content")
      this.setState({ screenType: "cover" });
    else this.setState({ screenType: "content" });
  };
  renderToolbar = () => (
    <View style={{ flex: 1, marginTop: 100 }}>
      
    </View>
  );
  onSeeking = currentTime => this.setState({ currentTime });

  render() {
    return (
      <View style={styles.container}>
        <Video
          onEnd={this.onEnd}
          onLoad={this.onLoad}
          onLoadStart={this.onLoadStart}
          onProgress={this.onProgress}
          paused={this.state.paused}
          ref={videoPlayer => (this.videoPlayer = videoPlayer)}
          resizeMode={this.state.screenType}
          onFullScreen={this.state.isFullScreen}
          fullscreenOrientation={"potrait"}
          source={{ uri: this.props.coach.coach_programme.video_url }}
          // source={{ uri: "https://www.youtube.com/watch?v=UAsTlnjvetI" }}
          style={styles.mediaPlayer}
          volume={10}
        />
        <MediaControls
          duration={this.state.duration}
          isLoading={this.state.isLoading}
          mainColor="#333"
          onFullScreen={this.onFullScreen}
          onPaused={this.onPaused}
          onReplay={this.onReplay}
          onSeek={this.onSeek}
          onSeeking={this.onSeeking}
          playerState={this.state.playerState}
          progress={this.state.currentTime}
          toolbar={this.renderToolbar()}
        />
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    position: "absolute",
    width: "100%",
    height: "100%",
    backgroundColor: "black"
  },
  toolbar: {
    marginTop: 30,
    backgroundColor: "white",
    padding: 10,
    borderRadius: 5
  },
  mediaPlayer: {
    position: 'absolute',
    top: 200,
    left: 10,
    bottom: 200,
    right: 10,
    backgroundColor: "black"
  }
});
const mapStateToProps = (state) => {
  const { coach } = state;
  return {
      coach
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(VideoPlayerCoach);
