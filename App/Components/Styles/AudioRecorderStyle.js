import { StyleSheet, Dimensions } from 'react-native'
import { Metrics, Fonts } from '../../Themes'

export default StyleSheet.create({
  container: {
    flex: 1
  },
  background: {
    flex: 1,
    backgroundColor: '#000000AA',
    justifyContent: 'center'
  },
  modalContainer: {
    width: Dimensions.get('screen').width * 0.6,
    height: Dimensions.get('screen').height * 0.35,
    backgroundColor: 'white',
    alignSelf: 'center',
    justifyContent: 'center',
    borderRadius: 20,
    elevation: 2
  },
  buttonContainer: {
    width: Dimensions.get('screen').width * 0.3,
    height: Dimensions.get('screen').width * 0.1,
    borderRadius: Dimensions.get('screen').width * 0.03,
    marginVertical: Metrics.baseMargin,
    backgroundColor: 'orange',
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    zIndex: 10
  },
  buttonContainerDisabled: {
    width: Dimensions.get('screen').width * 0.3,
    height: Dimensions.get('screen').width * 0.1,
    borderRadius: Dimensions.get('screen').width * 0.03,
    marginVertical: Metrics.baseMargin,
    backgroundColor: 'grey',
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    zIndex: 10
  },
  buttonCancelContainer: {
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    zIndex: 10,
    marginTop: Metrics.baseMargin,
    backgroundColor: 'grey',
    width: Dimensions.get('screen').width * 0.15 / 2,
    height: Dimensions.get('screen').width * 0.15 / 2,
    borderRadius: Dimensions.get('screen').width * 0.03 / 2
  },
  buttonText: {
    fontFamily: Fonts.type.helveticaNeueBold,
    fontSize: 16,
    color: 'white'
  },
  cancelText: {
    fontFamily: Fonts.type.helveticaNeueBold,
    fontWeight: 'bold',
    fontSize: 20,
    color: 'white'
  }
})
