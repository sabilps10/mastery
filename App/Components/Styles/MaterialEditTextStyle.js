import { Platform, StyleSheet } from 'react-native'
import { Metrics, Colors, Fonts } from '../../Themes'

export default StyleSheet.create({
  inputStyle: {
    ...Fonts.style.input.p1,
    color: Colors.white,
    marginLeft: Platform.OS === 'ios' ? Metrics.countWidthBased(Metrics.countWidthScala(10)) : Metrics.countWidthBased(Metrics.countWidthScala(7))
  },
  labelFocusStyle: {
    ...Fonts.style.input.p2,
    marginLeft: Metrics.countWidthBased(Metrics.countWidthScala(10))
  },
  labelDefaultStyle: {
    ...Fonts.style.input.p1,
    marginLeft: Metrics.countWidthBased(Metrics.countWidthScala(10))
  },
  inputContainerStyle: {
    marginTop: Metrics.countHeightBased(Metrics.countHeightScala(5)),
    marginHorizontal: Metrics.countWidthBased(Metrics.countWidthScala(Metrics.horizontalMargin))
  },
  errorTextStyle: {
    ...Fonts.style.body.p7,
    color: Colors.bloodOrange,
    left: Metrics.countWidthBased(Metrics.countWidthScala(10))
  }
})
