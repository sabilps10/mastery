import { StyleSheet, Dimensions } from 'react-native'
import { Colors, Metrics, Fonts} from '../../Themes'

export default StyleSheet.create({
  container: {
    backgroundColor: Colors.white,
    width: Dimensions.get('screen').width * 0.7,
    height: Dimensions.get('screen').height
  },
  userContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: Metrics.doubleBaseMargin * 2,
    marginLeft: Metrics.baseMargin,
    marginBottom: Metrics.doubleBaseMargin
  },
  avatarImage: {
    resizeMode: 'cover',
    width: Metrics.countWidthBased(Metrics.countWidthScala((170/3))),
    height: Metrics.countHeightBased(Metrics.countHeightScala((170/3))),
    borderRadius: Metrics.countHeightBased(Metrics.countHeightScala((170/3))) / 2
  },
  usernameContainer: {
    width: Dimensions.get('screen').width * 0.4,
    marginLeft: Metrics.baseMargin
  },
  nameText: {
    ...Fonts.style.subtitle.h3,
    color: Colors.coal,
    fontWeight: 'bold'
  },
  emailText: {
    ...Fonts.style.subtitle.h4
  },
  menuContainer: {
    marginTop: Metrics.doubleBaseMargin,
    marginLeft: Metrics.baseMargin
  },
  itemContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: Metrics.doubleBaseMargin
  },
  itemIconImage: {
    tintColor: Colors.masteryOrange,
    resizeMode: 'contain',
    width: Metrics.countWidthBased(Metrics.countWidthScala((50/2))),
    height: Metrics.countHeightBased(Metrics.countHeightScala((50/2))),
  },
  itemText: {
    ...Fonts.style.body.p3,
    color: Colors.coal,
    marginLeft: Metrics.baseMargin
  },
  lineBreak: {
    borderWidth: 0.35,
    borderColor: Colors.charcoal,
    width: Dimensions.get('screen').width * 0.6,
    marginBottom: Metrics.doubleBaseMargin
  }
})
