import { StyleSheet, Dimensions } from 'react-native'
import { Fonts, Colors, Metrics } from '../../Themes/'

export default StyleSheet.create({
  headerContainer: {
    zIndex: 1,
    backgroundColor: 'transparent'
  },
  headerImage: {
    ...StyleSheet.absoluteFillObject,
    backgroundColor: 'transparent',
    resizeMode: 'contain',
    width: Dimensions.get('screen').width,
    height: 261 * (Dimensions.get('screen').width / 768)
   
  },
  buttonBackContainer: {
    ...StyleSheet.absoluteFillObject,
    marginLeft: Metrics.baseMargin+10,
    marginTop: Metrics.baseMargin + 10,
    width: Metrics.countWidthBased(Metrics.countWidthScala(25)),
    height: Metrics.countHeightBased(Metrics.countHeightScala(25))
  },
  buttonBack: {
    resizeMode: 'contain',
    width: Metrics.countWidthBased(Metrics.countWidthScala(25)),
    marginTop: -Metrics.baseMargin / 2
  },
  headerTitleText: {
    ...Fonts.style.title.h3,
    color: Colors.white,
    alignSelf: 'center',
    fontWeight: 'bold',
    backgroundColor: 'transparent',
    marginTop: 261 * (Dimensions.get('screen').width / 768) / 7
  }
})
