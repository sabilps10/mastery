import React, { PureComponent } from 'react'
import { View, Text, Platform, TouchableOpacity, TouchableNativeFeedback } from 'react-native'

export default class TouchableOsResponsive extends PureComponent {
  render () {
    const { onPress, style, children } = this.props
    let { disabled } = this.props

    if (disabled === undefined) {
      disabled = false
    }

    if (disabled) {
      return (
        <View style={style} {...this.props}>
          {children}
        </View>
      )
    } else if (Platform.OS === 'android') {
      return (
        <TouchableNativeFeedback 
          onPress={() => setTimeout(() => { onPress() }, 0)} 
          {...this.props}
          useForeground
          >
          <View style={style}>
            {children}
          </View>
        </TouchableNativeFeedback>
      )
    } else if (Platform.OS === 'ios') {
      return (
        <TouchableOpacity onPress={onPress} style={style} {...this.props}>
          {children}
        </TouchableOpacity>
      )
    }
  }
}
