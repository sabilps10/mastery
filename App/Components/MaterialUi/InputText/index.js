import React, { PureComponent } from 'react'
import { View, TextInput, Animated, Text, Image, TouchableNativeFeedback } from 'react-native'
import styles from './styles.js'
import TouchableOsResponsive from '../TouchableOsResponsive'

import { Images } from '../../../Themes'

export const TYPE = {
  password: 'password'
} 

export default class InputText extends PureComponent {
  constructor (props) {
    super(props)
    this.state = {
      focus: props.isFocused && props.isFocused === true ? true : false,
      focusColor: props.focusColor ? props.focusColor : '#6bfdff',
      blurColor: props.blurColor ? props.blurColor : '#e0e0e0',
      text: props.propsValue ? props.propsValue : '',
      duration: 300,
      isVisiblePassword: true
    }

    this.onPressPasswordType = this.onPressPasswordType.bind(this)
    this.onBlur = this.onBlur.bind(this)
    this.onFocus = this.onFocus.bind(this)
    this.onChangeText = this.onChangeText.bind(this)
    this.placeHolderPosition = new Animated.Value(props.isFocused && props.isFocused === true ? 0 : 1)
  }

  onFocus () {
    this.setState({
      focus: true
    })
  }

  onBlur () {
    this.setState({
      focus: false
    })
  }

  onChangeText (text) {
    this.setState({
      text
    })
    this.props.onChangeText(text)
  }

  componentDidUpdate (prevProps, prevState) {
    const { focus, duration, text } = this.state
    const { label } = this.props
    if (label !== undefined && label !== null &&
      focus  ^ prevState.focus && text.length == 0) {
      let toValue = this.getToValue(focus)
      Animated
        .timing(this.placeHolderPosition,
          { 
            toValue,
            duration
          })
        .start()
    }
  }

  getToValue (focus) {
    return focus ? 0 : 1
  }

  onPressPasswordType () {
    this.setState((oldState) => {
      return {
        isVisiblePassword: !oldState.isVisiblePassword
      } 
    })
  }

  renderRightIcon (type, rightIconSource) {
    if (type !== undefined && type === TYPE.password) {
      const { isVisiblePassword } = this.state
      return (
        <TouchableOsResponsive style={styles.containerButton} onPress={this.onPressPasswordType}>
          <Image
            style={{ ...styles.rightIconStyle }}
            source={isVisiblePassword ? Images.hidePassword : Images.showPassword } />
        </TouchableOsResponsive>
      )
    } else if (rightIconSource !== undefined && rightIconSource !== null) {
      return (
        <Image
          style={{ ...styles.rightIconStyle }} />
      )
    } else return null
  }

  getLabelStyle (labelFocusStyle, labelStyle) {

  }

  render () {
    const {
      containerInputStyle,
      inputTextStyle,
      lineStyle,
      placeholderStyle,
      type,
      onPressRightIcon,
      rightIconSource,
      propsValue,
      label,
      labelFocusStyle,
      labelStyle,
      error,
      require,
      containerBottomLine,
      errorTextStyle,
      requireTextStyle,
      ...props
    } = this.props
    const {
      focus,
      focusColor,
      blurColor,
      text,
      isVisiblePassword
    } = this.state

    const bottom = this.placeHolderPosition.interpolate({
      inputRange: [0, 1],
      outputRange: [39, 10]
    })    

    return (
      <View style = {{ ...styles.container, ...containerInputStyle }}>
        <View style = {styles.containerInput}>
          <View style={{ flex: 1, flexDirection: 'row' }}>
            <TextInput
              secureTextEntry={type !== undefined && isVisiblePassword && type === TYPE.password}
              { ...props }
              style = {{ ...styles.inputTextStyle, ...inputTextStyle }}
              value = {propsValue ? propsValue : text}
              onChangeText = { this.onChangeText }
              onFocus = { this.onFocus }
              onBlur = { this.onBlur } />
            <Animated.View style={{ bottom, ...styles.placeholderStyle }}>
              <Text style={{ ...focus ? labelFocusStyle : labelStyle, color: focus ? focusColor : blurColor }}>{label}</Text>
            </Animated.View>
          </View>
          {
            this.renderRightIcon(type, rightIconSource)
          }
        </View>
        <View style = {{ ...styles.lineStyle, ...lineStyle, backgroundColor: focus ? focusColor : blurColor }}/>
        { (error || require) &&
          <Text style={error ? { ...styles.errorTextStyle, ...errorTextStyle } : { ...styles.requireTextStyle, ...requireTextStyle }}>{ error ? error : require }</Text>
        }
      </View>
    )
  }
}
