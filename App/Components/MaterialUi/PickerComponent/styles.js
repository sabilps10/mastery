import { StyleSheet, Platform } from 'react-native'

export default StyleSheet.create({
  container: {
    flex: 1,
    height: 80
  },
  containerInput: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'flex-end',
    flexDirection: 'row',
    position: 'absolute',
    right: 0,
    left: 0,
    bottom: 20,
    zIndex: 2
  },
  inputTextStyle: {
    flex: 1,
    flexDirection: 'row',
    zIndex: 3,
    marginLeft: Platform.OS === 'android' ? -3 : 0,
    height: 39
  },
  lineStyle: {
    position: 'absolute',
    bottom: 20,
    right: 0,
    left: 0,
    height: 1.5
  },
  leftIconStyle: {
    resizeMode: 'contain',
    width: 20,
    height: 20
  },
  rightIconStyle: {
    resizeMode: 'contain',
    width: 20,
    height: 20
  },
  placeholderStyle: {
    position: 'absolute',
    zIndex: 1
  },
  containerButton: {
    padding: 10
  },
  errorTextStyle: {
    color: 'red',
    position: 'absolute',
    height: 20,
    bottom: 0
  },
  requireTextStyle: {
    position: 'absolute',
    height: 20,
    bottom: 0
  }
})
