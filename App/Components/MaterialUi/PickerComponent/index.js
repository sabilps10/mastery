import React, { PureComponent } from 'react'
import { View, Animated, Text, Picker, Dimensions } from 'react-native'
import styles from './styles.js'
import TouchableOsResponsive from '../TouchableOsResponsive'

import DatePicker from 'react-native-datepicker'

export default class PickerComponent extends PureComponent {
  constructor (props) {
    super(props)
    this.state = {
      focus: props.isFocused && props.isFocused === true ? true : false,
      blurColor: props.blurColor ? props.blurColor : '#e0e0e0',
      focusColor: props.focusColor ? props.focusColor : '#6bfdff',
      text: '',
      duration: 300,
      dateChildren: null,
      pickerValue: ''
    }

    this.onBlur = this.onBlur.bind(this)
    this.onFocus = this.onFocus.bind(this)
    this.placeHolderPosition = new Animated.Value((props.isFocused && props.isFocused === true) || props.children != null ? 0 : 1)
  }

  onFocus () {
    this.setState({
      focus: true
    })
  }

  onBlur () {
    this.setState({
      focus: false
    })
  }

  componentDidUpdate (prevProps, prevState) {
    const { focus, duration, text } = this.state
    const { label } = this.props
    if (label !== undefined && label !== null &&
      focus  ^ prevState.focus && text.length == 0) {
      let toValue = this.getToValue(focus)
      Animated
        .timing(this.placeHolderPosition,
          {
            toValue,
            duration
          })
        .start()
    }
  }

  getToValue (focus) {
    return focus ? 0 : 1
  }

  render () {
    const {
      containerInputStyle,
      inputTextStyle,
      lineStyle,
      placeholderStyle,
      label,
      labelFocusStyle,
      labelStyle,
      children,
      mode,
      ...props
    } = this.props
    const {
      focus,
      focusColor,
      blurColor
    } = this.state

    const bottom = this.placeHolderPosition.interpolate({
      inputRange: [0, 1],
      outputRange: [39, 10]
    })
    
    const d = new Date()

    return (
      <View style = {{ ...styles.container, ...containerInputStyle }}>
        <View style = {styles.containerInput}>
          <View style={{ flex: 1, flexDirection: 'row' }}>
            <TouchableOsResponsive 
              style={{ flex: 1, flexDirection: 'row', zIndex: 2 }}
              disabled={ mode !== 'Date' }
              onPress={() => {
                this.refs.DatePicker.onPressDate()
              }}>
              <Picker
                selectedValue={this.state.pickerValue}
                enabled={ mode !== 'Date' }
                style = {{ ...styles.inputTextStyle, ...inputTextStyle, marginLeft: 5, color: '#000000' }}
                onValueChange={(itemValue, itemIndex) => {
                  this.onFocus()
                  this.setState({pickerValue: itemValue})
                }}>
                { mode !== 'Date' ?
                  children : this.state.dateChildren
                }
              </Picker>
              { mode === 'Date' ?
              <DatePicker
                ref={'DatePicker'}
                style={{ position: 'absolute', marginLeft: Dimensions.get('screen').width / 2 }}
                date={d.getDate() + '-' + d.getMonth() + '-' + '1995'}
                mode="date"
                // showIcon={false}
                hideText={true}
                format="DD-MM-YYYY"
                minDate="1-1-1900"
                maxDate="31-12-2010"
                confirmBtnText="Konfirmasi"
                cancelBtnText="Batal"
                androidMode="spinner"
                onDateChange={(date) => {this.setState({
                  dateChildren: <Picker.Item style = {{ ...styles.inputTextStyle, ...inputTextStyle }} label={date} value={date} />
                })}}
              />
              : null
              }
            </TouchableOsResponsive>
            <Animated.View style={{ bottom, ...styles.placeholderStyle }}>
              <Text style={{ ...focus ? labelFocusStyle : labelStyle, color: blurColor }}>{label}</Text>
            </Animated.View>
          </View>
        </View>
        <View style = {{ ...styles.lineStyle, ...lineStyle, backgroundColor: blurColor }}/>
      </View>
    )
  }
}
