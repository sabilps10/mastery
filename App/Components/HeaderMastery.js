import React, { Component } from 'react'
import { View, Text, Image } from 'react-native'
import styles from './Styles/HeaderMasteryStyle'

import TouchableOSResponsive from './MaterialUi/TouchableOsResponsive'
import { Images } from '../Themes'

export default class HeaderMastery extends Component {
  render () {
    const { navigation, title, children } = this.props

    return (
      <View style={styles.headerContainer}>
        <Image style={{...styles.headerImage, top: children ? 0 : -10}} source={Images.header}></Image>
          <TouchableOSResponsive style={styles.buttonBackContainer} onPress={() => { navigation ? navigation.goBack() : {} }}>
            <Image style={styles.buttonBack} source={Images.buttonBack}/>
          </TouchableOSResponsive>
        <Text style={styles.headerTitleText}>{title ? title : 'TITLE'}</Text>
        { children }
      </View>
    )
  }
}
