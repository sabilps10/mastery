import React, { Component } from "react";
import { View, Text, Image } from "react-native";
import styles from "./Styles/SideBarStyle";
import { Images } from "../Themes";
import { connect } from "react-redux";
import NewHomeActions from "../Redux/NewHomeRedux"
import TouchableOsResponsive from "./MaterialUi/TouchableOsResponsive";

export class SideBar extends Component {
  render() {
    if (__DEV__) console.tron.log("isi sidebar", this.props);
    const { navigation } = this.props; 
    const manage = false;
    if (typeof this.props.newhome.profile === "undefined") {
      return <View></View>;
    }
    if (this.props.newhome.profile === null) {
      return <View></View>;
    }
    return (
      <View style={styles.container}>
        <View style={styles.userContainer}>
          <TouchableOsResponsive onPress={() => {
            navigation.navigate("ShowProfileScreen");
          }}>
            <Image
              style={styles.avatarImage}
              source={{ uri: this.props.newhome.profile.avatar }}
            />
            <View style={styles.usernameContainer}>
              <Text style={styles.nameText}>
                {this.props.newhome.profile.name}
              </Text>
              <Text style={styles.emailText}>
                {this.props.newhome.profile.email}
              </Text>
            </View>
          </TouchableOsResponsive>
        </View>
        <View style={styles.menuContainer}>
          <TouchableOsResponsive
            onPress={() => {
              this.props.onPressSummary();
            }}
          >
            <View style={styles.itemContainer}>
              <Image style={styles.itemIconImage} source={Images.homeIcon} />
              <Text style={styles.itemText}>Summary</Text>
            </View>
          </TouchableOsResponsive>
          <TouchableOsResponsive
            onPress={() => {
              this.props.navigation.navigate("MyPerformanceScreen");
            }}
          >
            <View style={styles.itemContainer}>
              <Image style={styles.itemIconImage} source={Images.summaryIcon} />
              <Text style={styles.itemText}>My Performance</Text>
            </View>
          </TouchableOsResponsive>
          <TouchableOsResponsive
            onPress={() => {
              this.props.navigation.navigate("QuestNewScreen");
            }}
          >
            <View style={styles.itemContainer}>
              <Image
                style={styles.itemIconImage}
                source={Images.questMenuIcon}
              />
              <Text style={styles.itemText}>Quest</Text>
            </View>
          </TouchableOsResponsive>
          <TouchableOsResponsive
            onPress={() => {
              this.props.navigation.navigate("AppreciationsScreen");
            }}
          >
            <View style={styles.itemContainer}>
              <Image
                style={styles.itemIconImage}
                source={Images.appreciationsIcon}
              />
              <Text style={styles.itemText}>Appreciations</Text>
            </View>
          </TouchableOsResponsive>
          
          { <TouchableOsResponsive
            onPress={() => {
              this.props.navigation.navigate("CoachingProgrammeListScreen");
            }}
          >
            <View style={styles.itemContainer}>
              <Image
                style={styles.itemIconImage}
                source={Images.coachingProgrammeIcon}
              />
              <Text style={styles.itemText}>Coaching Programme</Text>
            </View>
          </TouchableOsResponsive> }
          
          <TouchableOsResponsive
            onPress={() => {
              this.props.navigation.navigate("RewardsScreen");
            }}
          >
            <View style={styles.itemContainer}>
              <Image style={styles.itemIconImage} source={Images.rewardsIcon} />
              <Text style={styles.itemText}>My Rewards</Text>
            </View>
          </TouchableOsResponsive>
          {manage ? (
            <TouchableOsResponsive
              onPress={() => {
                this.props.navigation.navigate("NewsletterScreen");
              }}
            >
              <View style={styles.itemContainer}>
                <Image
                  style={styles.itemIconImage}
                  source={Images.newsletterIcon}
                />
                <Text style={styles.itemText}>Newsletter</Text>
              </View>
            </TouchableOsResponsive>
          ) : null}
          {manage ? (
            <TouchableOsResponsive onPress={() => {}}>
              <View style={styles.itemContainer}>
                <Image
                  style={styles.itemIconImage}
                  source={Images.manageIcon}
                />
                <Text style={styles.itemText}>Manage</Text>
              </View>
            </TouchableOsResponsive>
          ) : null}
          <View style={styles.lineBreak} />
          <TouchableOsResponsive
            onPress={() => {
              this.props.onLogout();
            }}
          >
            <View style={styles.itemContainer}>
              <Image style={styles.itemIconImage} source={Images.logoutIcon} />
              <Text style={styles.itemText}>Log Out</Text>
            </View>
          </TouchableOsResponsive>
        </View>
      </View>
    );
  }
}
const mapStateToProps = (state) => {
  const {newhome } = state;
  return {
    newhome
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getNewHome: () => dispatch(NewHomeActions.newHomeRequest()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SideBar);
