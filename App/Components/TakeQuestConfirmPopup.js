import React, { useState } from "react";
import { View, Text, TouchableOpacity, TouchableHighlight,ActivityIndicator } from "react-native";
import TouchableOSResponsive from "../Components/MaterialUi/TouchableOsResponsive";

import Modal from "react-native-simple-modal";
import { ApplicationStyles, Fonts, Colors, Metrics } from '../Themes'

export default function TakeQuestConfirmPopup(props) {
  const { styles } = props;
  const [visible, setVisible] = useState(false);
  return (
    <Modal
      offset={10}
      open={props.visible}
      style={{borderRadius:20}}
      style={{ alignItems: "center" }}
    >
      <View style={{ marginTop: 22,paddingLeft:20,paddingRight:20 }}>
        <View style={{alignContent:"center"}}>
          <Text>Are you sure to take these quest ? </Text>
          <View>
            <Text style={{...Fonts.style.h4,fontWeight:'bold',marginTop:Metrics.baseMargin}}>{props.data.name}</Text>
          </View>
          <View>
            <Text style={{fontWeight:'bold',marginTop:Metrics.baseMargin}}>Deadline</Text>
          </View>
          <View>
            <Text>{props.data.end_time}</Text>
          </View>
          <View>
            <Text style={{fontWeight:'bold',marginTop:Metrics.baseMargin}}>Points</Text>
          </View>
          <View>
            <Text>{props.data.points}</Text>
          </View>
         
            {props.progress ? (<View style={{
                flexDirection: 'row',
                justifyContent: 'center',
                marginTop: Metrics.baseMargin
            }}>
              <ActivityIndicator color="#cccccc"/>
              </View>) : (
               <View style={{
                flexDirection: 'row',
                justifyContent: 'center',
                marginTop: Metrics.baseMargin
            }}>
            <TouchableOSResponsive
              style={styles.questDetailButton}
              onPress={() => {
                props.confirm()
              }}
            >
              <Text style={styles.questDetailButtonText}>CONFIRM</Text>
            </TouchableOSResponsive>
            <TouchableOSResponsive
              style={{
                ...styles.questDetailButton,
                marginLeft: Metrics.baseMargin
              }}
              onPress={() => {
                  props.cancel()
              }}
            >
              <Text style={styles.questDetailButtonText}>CANCEL</Text>
            </TouchableOSResponsive>
          </View>
            )}
            
        </View>
      </View>
      
    </Modal>
  );
}
