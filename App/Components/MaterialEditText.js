import React, { Component } from 'react'
import styles from './Styles/MaterialEditTextStyle'
import InputText, { TYPE } from './MaterialUi/InputText'

import { Colors } from '../Themes'

export default class MaterialEditText extends Component {
  constructor (props) {
    super(props)
  }

  render () {
    return (
      <InputText
        inputTextStyle={styles.inputStyle}
        labelFocusStyle={styles.labelFocusStyle}
        labelStyle={styles.labelDefaultStyle}
        containerInputStyle={{...styles.inputContainerStyle, ...this.props.containerInputStyle}}
        focusColor={Colors.masteryOrange}
        errorTextStyle={styles.errorTextStyle}
        {...this.props} />
    )
  }
}
