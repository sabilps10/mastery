import React, { Component } from 'react';
import { StyleSheet, View, Dimensions } from 'react-native';
import Pdf from 'react-native-pdf';


export default class DownloadFile extends Component {
    renderToolbar = () => (
        <View style={{ flex: 1, marginTop: 100 }}>
          <TouchableOsResponsive
            onPress={() => {
              this.setState({
                paused: true
              });
              this.props.close();
            }}
          >
            <View style={{
              alignItems: 'flex-end'
            }}>
              <Icon name="close" size={30} color="#ffffff" />
            </View>
          </TouchableOsResponsive>
        </View>
      );
    render() {
        const source = {uri: this.props.navigation.state.params.url,cache:true};
        //const source = require('./test.pdf');  // ios only
        //const source = {uri:'bundle-assets://test.pdf'};
 
        //const source = {uri:'file:///sdcard/test.pdf'};
        //const source = {uri:"data:application/pdf;base64,JVBERi0xLjcKJc..."};
 
        return (
            <View style={styles.container}>
                <Pdf
                    source={source}
                    onLoadComplete={(numberOfPages,filePath)=>{
                        if(__DEV__) console.tron.log(`number of pages: ${numberOfPages}`);
                    }}
                    onPageChanged={(page,numberOfPages)=>{
                        if(__DEV__) console.tron.log(`current page: ${page}`);
                    }}
                    onError={(error)=>{
                        if(__DEV__) console.tron.log(error);
                    }}
                    onPressLink={(uri)=>{
                        if(__DEV__) console.tron.log(`Link presse: ${uri}`)
                    }}
                    style={styles.pdf}/>
            </View>
        )
  }
}
 
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
        marginTop: 25,
    },
    pdf: {
        flex:1,
        width:Dimensions.get('window').width,
        height:Dimensions.get('window').height,
    }
});