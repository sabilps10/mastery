import React, { useState } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  TouchableHighlight,
  ActivityIndicator
} from "react-native";
import TouchableOSResponsive from "../Components/MaterialUi/TouchableOsResponsive";

import Modal from "react-native-simple-modal";
import { ApplicationStyles, Fonts, Colors, Metrics } from "../Themes";

export default function CloseQuestConfirmPopup(props) {
  const { styles } = props;
  const [visible, setVisible] = useState(false);
  return (
    <Modal
      offset={10}
      open={props.visible}
      closeOnTouchOutside={false}
      style={{ borderRadius: 20 }}
      style={{ alignItems: "center" }}
    >
      <View style={{ marginTop: 22, paddingLeft: 20, paddingRight: 20 }}>
        <View style={{ alignContent: "center" }}>
          <Text
            style={{
              alignContent: "center",
              justifyContent: "center",
              textAlign: "center"
            }}
          >
            Are you sure to close these quest ?{" "}
          </Text>

          {props.progress ? (
            <View
              style={{
                flexDirection: "row",
                justifyContent: "center",
                marginTop: Metrics.baseMargin
              }}
            >
              <ActivityIndicator color="#cccccc" />
            </View>
          ) : (
            <View
              style={{
                flex: 1,
                height: 200,
                padding: 20,
                paddingBottom: 70,
                flexDirection: "row",
                justifyContent: "center",
                marginTop: Metrics.baseMargin
              }}
            >
              <TouchableOpacity
               style={{
                height: 45,
                borderRadius: 5,
                marginHorizontal: Metrics.section,
                marginVertical: Metrics.baseMargin,
                backgroundColor: Colors.fire,
                paddingLeft: 10,
                paddingRight: 10,
                justifyContent: "center"
              }}
                onPress={() => {
                  props.confirm();
                }}
              >
                <View
                  
                >
                  <Text
                    style={{
                      ...styles.questDetailButtonText,
                      fontWeight: "bold",

                      color: "#ffffff"
                    }}
                  >
                    CONFIRM
                  </Text>
                </View>
              </TouchableOpacity>
              <TouchableOSResponsive
                style={{
                    height: 45,
                    borderRadius: 5,
                    marginHorizontal: Metrics.section,
                    marginVertical: Metrics.baseMargin,
                    backgroundColor: Colors.fire,
                    paddingLeft: 10,
                    paddingRight: 10,
                    justifyContent: "center"
                  }}
                onPress={() => {
                    if(__DEV__) console.tron.log("cancel");
                  props.cancel();
                }}
              >
                <View
                  
                >
                  <Text
                    style={{
                      ...styles.questDetailButtonText,

                      fontWeight: "bold",
                      color: "#ffffff"
                    }}
                  >
                    CANCEL
                  </Text>
                </View>
              </TouchableOSResponsive>
            </View>
          )}
        </View>
      </View>
    </Modal>
  );
}
