import React, { useState } from "react";
import { Images } from "../Themes";
import { Metrics } from "../Themes/";
import { View, Text, Image } from "react-native";
import TouchableOSResponsive from "../Components/MaterialUi/TouchableOsResponsive";

export default function FinishedQuestListItem(props) {
  const { styles, data } = props;
 
  const { name, start_time, id, task_status, review_score, points, participant_task_id } = data;

  return (
    <TouchableOSResponsive
      style={styles.questComponentContainer}
      key={id}
      onPress={() => {
        if(__DEV__) console.tron.log("finished click", participant_task_id, props);
        props.clicked(participant_task_id);
      }}
    >
      <View style={styles.questComponentContainer} key={id}>
        <View style={styles.questIconContainer}>
          <Image style={styles.questIcon} source={Images.questIcon} />
        </View>
        <View style={styles.questDescriptionContainer}>
          <Text style={styles.questTitleText}>{name}</Text>
          <Text style={styles.questDateText}>{start_time}</Text>
        </View>
        {(task_status === 1) ? (
           <Text style={styles.questPointsText}>-</Text>
        ) : null}

        {(task_status === 2) ? (
            <Text style={styles.questPointsText}>{points.toString()}</Text>
        ) : null}
       
        
      </View>
    </TouchableOSResponsive>
  );
}
