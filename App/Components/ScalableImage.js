import React, { Component } from "react";
import {Image} from 'react-native'
import PropTypes from 'prop-types'
import styles from './Styles/ScalableImageStyle'

export default class ScalableImage extends Component {
    constructor(props) {
        super(props)
        this.state = {
            source: {uri: this.props.uri},
            width: 0,
            height: 0,
        }
    }

    componentWillMount() {
        Image.getSize(this.props.uri, (width, height) => {
            if (this.props.width && !this.props.height) {
                this.setState({width: this.props.width, height: height * (this.props.width / width)})
            } else if (!this.props.width && this.props.height) {
                this.setState({width: width * (this.props.height / height), height: this.props.height})
            } else {
                this.setState({width: width, height: height})
            }

            if (this.props.onLoaded) {
                this.props.onLoaded(this.props.index ? this.props.index : 0)
            }
        }, (error) => {
          if (__DEV__) console.tron.log("ScalableImage:componentWillMount:Image.getSize failed with error: ", error)
        })
    }

    render() {
        return <Image source={this.state.source} style={[this.props.style, {height: this.state.height, width: this.state.width}]}/>
    }
}
