import React, { useState } from "react";
import {ActivityIndicator, Platform} from "react-native";
import { ScrollView, View, Text } from "react-native";
import OngoingQuestListItem from "./OngoingQuestListItem";

export default function OngoingQuestList(props) {
  if(__DEV__) console.tron.log(props.data);
  const { styles } = props;
  if(props.progress) return (
    <View style={styles.message}>
    <ActivityIndicator size={Platform.OS === "ios" ? 'large' : 30}></ActivityIndicator>
  </View>
  )
  if (typeof props.data === 'undefined' || props.data === 0 || props.data.length === 0) return (
    <View style={styles.message}>
      <Text style={styles.messageText}>
        Hooray, no more quest(s) at the moment !
      </Text>
    </View>
  )
  if(typeof props.data !== 'undefined'){
    return (
      <ScrollView style={styles.questsContainer}>
        {props.data.map((data, index) => {
          return (
            <OngoingQuestListItem
              key={index}
              {...props}
              data={data}
              clicked={id => {
                props.onclick(id)
              }}
            />
          );
        })}
      </ScrollView>
    );
  }
  else {
    <View></View>
  }
}
