import React, { useState } from "react";
import { Images } from "../Themes";
import { Metrics } from "../Themes";
import { View, Text, Image } from "react-native";
import TouchableOSResponsive from "./MaterialUi/TouchableOsResponsive";

export default function CoachingProgrammeListItem(props) {
  
  const { styles, data } = props;
  const { name, start_time, id, participant_tasks_id } = data;

  return (
    <TouchableOSResponsive
      style={styles.questComponentContainer}
      key={id}
      onPress={() => {
        props.clicked(participant_tasks_id);
      }}
    >
      <View style={styles.questIconContainer}>
        <Image style={styles.questIcon} source={Images.questIcon} />
      </View>
      <View style={styles.questDescriptionContainer}>
        <Text style={styles.questTitleText}>{name}</Text>
        <Text style={styles.questDateText}>{start_time}</Text>
      </View>
    </TouchableOSResponsive>
  );
}
