import React, { useState } from "react";
import { ScrollView,View, Text} from "react-native";
import QuestListItem from './QuestListItem';

export default function QuestList(props) {
  const {styles} = props

  if(props.data.length === 0) return (
    <View style={styles.message}>
      <Text style={styles.messageText}>
        Hooray, no more quest(s) at the moment !
      </Text>
    </View>
  )

  return (
    <ScrollView style={styles.questsContainer}>
    {
      props.data.map((data, index) => {
       
        return (
          <QuestListItem key={index} {...props} data={data} />
        )
      })
    }
    </ScrollView>
  )
}

