import React, { useState } from "react";
import { Images } from "../Themes";
import { Metrics } from "../Themes/";
import { View, Text, Image, Dimensions } from "react-native";
import TouchableOSResponsive from "../Components/MaterialUi/TouchableOsResponsive";
import HTMLView from 'react-native-htmlview';

export default function QuestListItem(props) {
  if(__DEV__) console.tron.log("props");
  const [isOpen, setOpen] = useState(false);
  const { styles, data } = props;
  const { name, start_time, points, id, description } = data;
  if(__DEV__) console.tron.log("quest:", data);
  return (
    <View key={id}>
      <View style={styles.questComponentContainer}>
        <TouchableOSResponsive
          style={styles.expandPanelContainer}
          onPress={() => setOpen(!isOpen)}
        >
          <View style={styles.questIconContainer}>
            <Image style={styles.questIcon} source={Images.questIcon} />
          </View>
          <View style={styles.questDescriptionContainer}>
            <Text style={styles.questTitleText}>{name}</Text>
            <Text style={styles.questDateText}>{start_time}</Text>
          </View>
        </TouchableOSResponsive>
        <TouchableOSResponsive
          style={styles.expandButtonContainer}
          onPress={() => setOpen(!isOpen)}
        >
          <Image
            style={styles.expandButtonImage}
            source={!isOpen ? Images.expandIcon : Images.shrinkIcon}
          />
        </TouchableOSResponsive>
      </View>
      {isOpen ? (
        <View style={styles.questDetailContainer}>
          <View style={styles.questDetailPointContainer}>
            <Text style={styles.questDetailPointTitleText}>POINT REWARD: </Text>
            <Text style={styles.questDetailPointValueText}>
              {points.toString()}
            </Text>
          </View>
          {/* <Text style={styles.questDetailDescriptionText}>{description}</Text> */}
          <HTMLView style={styles.questDetailDescriptionText} value={description} />
          <Text style={styles.questDetailTakeQuestText}>TAKE QUEST?</Text>
          <View style={styles.questDetailButtonContainer}>
            <TouchableOSResponsive
              style={styles.questDetailButton}
              onPress={() => {
                props.takeQuest(data);
              }}
            >
              <Text style={styles.questDetailButtonText}>YES</Text>
            </TouchableOSResponsive>
            <TouchableOSResponsive
              style={{
                ...styles.questDetailButton,
                marginLeft: Metrics.baseMargin
              }}
              onPress={() => {}}
            >
              <Text style={styles.questDetailButtonText}>NO</Text>
            </TouchableOSResponsive>
          </View>
        </View>
      ) : null}
      <View style={{
        borderWidth: 0.75,
        opacity: 0.1,
        width: Dimensions.get('screen').width * 0.8,
        alignSelf: 'center',
        marginBottom: Metrics.doubleBaseMargin
      }}/>
    </View>
  );
}
