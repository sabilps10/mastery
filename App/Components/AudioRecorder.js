import React, { Component } from 'react'
import { View, Text, Modal, TouchableOpacity } from 'react-native'
import styles from './Styles/AudioRecorderStyle'
import SoundRecorder from 'react-native-sound-recorder'
import Sound from 'react-native-sound'

var s = null
export default class AudioRecorder extends Component {
  constructor(props) {
    super(props);

    this.state = {
      is_recording: false,
      is_playing: false,
      path: null
    };

    this.onRecord = this.onRecord.bind(this)
    this.onPlay = this.onPlay.bind(this)
    this.playRecord = this.playRecord.bind(this)
    this.stopPlayRecord = this.stopPlayRecord.bind(this)
  }

  playRecord (path) {
    s = new Sound(path, Sound.CACHES, (e) => {
      s.play((r) => {
        this.setState({
          is_playing: false
        })
      })
    })
  }

  stopPlayRecord () {
    if (s !== null) {
      s.stop()
      this.setState({
        is_playing: false
      })
    }
  }

  onPlay () {
    if (this.state.is_playing) {
      this.stopPlayRecord()
    } else {
      this.playRecord(this.state.path)
    }

    this.setState({
      is_playing: !this.state.is_playing
    })
  }

  onRecord () {
    if (!this.state.is_recording) {
      SoundRecorder.start(SoundRecorder.PATH_CACHE + '/test.mp4')
      .then(function() {
          if (__DEV__) console.tron.log('started recording');
      });
    } else {
      SoundRecorder.stop()
      .then((result) => {
        this.setState({
          path: result.path
        })
      });
    }

    this.setState({
      is_recording: !this.state.is_recording
    })
  }

  render () {
    const  { is_recording, is_playing, path } = this.state
    const { onCancelAudioRecord, onPressUploadAudio, visible } = this.props

    return (
      <Modal
      visible={visible}
      animated={true}
      animationType={'fade'}
      transparent={true}
      style={styles.container}>
        <View style={styles.background}>
          <View style={styles.modalContainer}>
            <TouchableOpacity disabled={is_playing} style={is_playing ? styles.buttonContainerDisabled : styles.buttonContainer} onPress={this.onRecord}>
              <Text style={styles.buttonText}>{is_recording ? "STOP" : "RECORD"}</Text>
            </TouchableOpacity>
            <TouchableOpacity disabled={path === null || is_recording} style={path === null || is_recording ? styles.buttonContainerDisabled : styles.buttonContainer} onPress={this.onPlay}>
              <Text style={styles.buttonText}>{is_playing ? "STOP" : "PLAY"}</Text>
            </TouchableOpacity>
            <TouchableOpacity disabled={path === null || is_recording} style={path === null || is_recording ? styles.buttonContainerDisabled : styles.buttonContainer} onPress={() => { 
                onPressUploadAudio(path)
                onCancelAudioRecord()
                setTimeout(() => {
                  this.setState({
                    is_recording: false,
                    is_playing: false,
                    path: null
                  })
                }, 100)
              }}>
              <Text style={styles.buttonText}>{"UPLOAD"}</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.buttonCancelContainer} onPress={() => onCancelAudioRecord() }>
              <Text style={styles.cancelText}>{"X"}</Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    )
  }
}
