import { createStackNavigator, createAppContainer } from 'react-navigation'
import MyWebScreen from '../Containers/MyWebScreen'
import MyQuestFilesScreen from '../Containers/MyQuestFilesScreen'
import MyQuestWorkspaceScreen from '../Containers/MyQuestWorkspaceScreen'
import QuestWorkspaceScreen from '../Containers/QuestWorkspaceScreen'
import MyQuestInfoScreen from '../Containers/MyQuestInfoScreen'
import QuestCompletedScreen from '../Containers/QuestCompletedScreen'
import QuestOngoingScreen from '../Containers/QuestOngoingScreen'
import QuestNewScreen from '../Containers/QuestNewScreen'
import NewsletterScreen from '../Containers/NewsletterScreen'
import AppreciationsScreen from '../Containers/AppreciationsScreen'
import RewardsScreen from '../Containers/RewardsScreen'
import HomeScreen from '../Containers/HomeScreen'
import MyPerformanceScreen from '../Containers/MyPerformanceScreen'
import LoginScreen from '../Containers/LoginScreen'
import ForgetPasswordScreen from '../Containers/ForgetPasswordScreen'
import RegisterScreen from '../Containers/RegisterScreen'
import ShowProfileScreen from '../Containers/ShowProfileScreen'
// import UpdateProfileScreen from '../Containers/UpdateProfileScreen'
// import ProfileScreen from '../Containers/ProfileScreen'
import EditProfileScreen from '../Containers/EditProfileScreen'
import EditPasswordScreen from '../Containers/EditPasswordScreen'
import SuccessRegisterScreen from '../Containers/SuccessRegisterScreen'
import SuccessEditPasswordScreen from '../Containers/SuccessEditPasswordScreen'
import SuccessEditProfileScreen from '../Containers/SuccessEditProfileScreen'
import SuccessResetPassScreen from '../Containers/SuccessResetPassScreen'
import SuccessRedeemScreen from '../Containers/SuccessRedeemScreen'
import CoachingProgrammeListScreen from '../Containers/CoachingProgrammeListScreen'
import CoachingProgrammeMainTitleScreen from '../Containers/CoachingProgrammeMainTitleScreen'
import SuccessCoachingScreen from '../Containers/SuccessCoachingScreen'
import ReturnCoachingScreen from '../Containers/ReturnCoachingScreen'
import DownloadFile from '../Components/DownloadFile'
import VideoPlayerCoach from '../Components/VideoPlayerCoach'
import styles from './Styles/NavigationStyles'
import PlayerScreen from '../Containers/AudioPlayer'

// Manifest of possible screens
const PrimaryNav = createStackNavigator({
  MyWebScreen: { screen: MyWebScreen },
  MyQuestFilesScreen: { screen: MyQuestFilesScreen },
  MyQuestWorkspaceScreen: { screen: MyQuestWorkspaceScreen },
  QuestWorkspaceScreen: { screen: QuestWorkspaceScreen },
  MyQuestInfoScreen: { screen: MyQuestInfoScreen },
  QuestCompletedScreen: { screen: QuestCompletedScreen },
  QuestOngoingScreen: { screen: QuestOngoingScreen },
  QuestNewScreen: { screen: QuestNewScreen },
  NewsletterScreen: { screen: NewsletterScreen },
  AppreciationsScreen: { screen: AppreciationsScreen },
  RewardsScreen: { screen: RewardsScreen },
  HomeScreen: { screen: HomeScreen },
  MyPerformanceScreen: { screen: MyPerformanceScreen },
  LoginScreen: { screen: LoginScreen },
  ShowProfileScreen: { screen: ShowProfileScreen },
  DownloadFile: { screen: DownloadFile },
  VideoPlayerCoach: {screen: VideoPlayerCoach},
  PlayerScreen: {screen: PlayerScreen},
  // ProfileScreen: {screen: ProfileScreen },
  // UpdateProfileScreen: { screen: UpdateProfileScreen },
  EditProfileScreen: { screen: EditProfileScreen },
  SuccessEditProfileScreen : { screen: SuccessEditProfileScreen },
  EditPasswordScreen: { screen: EditPasswordScreen },
  ForgetPasswordScreen: { screen: ForgetPasswordScreen },
  RegisterScreen: { screen: RegisterScreen },
  SuccessRegisterScreen: { screen: SuccessRegisterScreen },
  SuccessResetPassScreen: { screen: SuccessResetPassScreen },
  SuccessEditPasswordScreen: {screen: SuccessEditPasswordScreen},
  SuccessRedeemScreen: {screen: SuccessRedeemScreen},
  CoachingProgrammeListScreen: { screen: CoachingProgrammeListScreen },
  CoachingProgrammeMainTitleScreen: { screen: CoachingProgrammeMainTitleScreen },
  SuccessCoachingScreen: { screen: SuccessCoachingScreen },
  ReturnCoachingScreen: {screen: ReturnCoachingScreen},
}, {
  // Default config for all screens
  headerMode: 'none',
  initialRouteName: 'LoginScreen',
  navigationOptions: {
    headerStyle: styles.header
  }
})

export default createAppContainer(PrimaryNav)
