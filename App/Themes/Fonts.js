const type = {
  base: 'Avenir-Book',
  bold: 'Avenir-Black',
  emphasis: 'HelveticaNeue-Italic',
  helveticaNeue: 'HelveticaNeue',
  helveticaNeueLight: 'HelveticaNeue-Light',
  helveticaNeueThin: 'HelveticaNeue-Thin',
  helveticaNeueMedium: 'HelveticaNeue-Medium',
  helveticaNeueBold: 'HelveticaNeue-Bold',
}

const size = {
  title: {
    h1: 27,
    h2: 24,
    h3: 24,
    h4: 20,
    h5: 18,
    h6: 10
  },
  subtitle: {
    h1: 18,
    h2: 14,
    h3: 14,
    h4: 10
  },
  body: {
    p1: 18,
    p2: 14,
    p3: 14,
    p4: 12,
    p5: 12,
    p6: 10,
    p7: 10,
    p10: 8
  },
  h1: 38,
  h2: 34,
  h3: 30,
  h4: 26,
  h5: 20,
  h6: 19,
  input: 18,
  regular: 17,
  medium: 14,
  small: 12,
  tiny: 8.5
}

const style = {
  title:{
    h1: {
      fontSize: size.title.h1,
      fontFamily: type.helveticaNeueBold
    },
    h2: {
      fontSize: size.title.h2,
      fontFamily: type.helveticaNeueBold
    },
    h3: {
      fontSize: size.title.h3,
      fontFamily: type.helveticaNeueLight
    },
    h4: {
      fontSize: size.title.h4,
      fontFamily: type.helveticaNeueBold
    },
    h5: {
      fontSize: size.title.h5,
      fontFamily: type.helveticaNeueLight
    },
    h6: {
      fontSize: size.title.h6,
      fontFamily: type.helveticaNeueBold
    },
    h7: {
      fontSize: 14,
      fontFamily: type.helveticaNeueBold
    },
    h8: {
      fontSize: 12,
      fontFamily: type.helveticaNeueBold
    }
  },
  subtitle: {
    h1: {
      fontSize: size.subtitle.h1,
      fontFamily: type.helveticaNeueMedium
    },
    h2: {
      fontSize: size.subtitle.h2,
      fontFamily: type.helveticaNeueBold
    },
    h3: {
      fontSize: size.subtitle.h3,
      fontFamily: type.helveticaNeueMedium
    },
    h4: {
      fontSize: size.subtitle.h4,
      fontFamily: type.helveticaNeueMedium
    }
  },
  input: {
    p1: {
      fontSize: 14,
      fontFamily: type.helveticaNeueMedium
    },
    p2: {
      fontSize: 14,
      fontFamily: type.helveticaNeueLight
    },
    search: {
      fontSize: 14,
      fontFamily: type.helveticaNeue
    },
    otp: {
      fontSize: 41,
      fontFamily: type.helveticaNeueBold
    }
  },
  body: {
    p1: {
      fontSize: size.body.p1,
      fontFamily: type.helveticaNeueLight
    },
    p2: {
      fontSize: size.body.p2,
      fontFamily: type.helveticaNeueLight
    },
    p3: {
      fontSize: size.body.p3,
      fontFamily: type.helveticaNeueThin
    },
    p4: {
      fontSize: size.body.p4,
      fontFamily: type.helveticaNeueMedium
    },
    p5: {
      fontSize: size.body.p5,
      fontFamily: type.helveticaNeueLight
    },
    p6: {
      fontSize: size.body.p6,
      fontFamily: type.helveticaNeueBold
    },
    p7: {
      fontSize: size.body.p7,
      fontFamily: type.helveticaNeueLight
    },
    p8: {
      fontSize: 13,
      fontFamily: type.helveticaNeueThin
    },
    p10: {
      fontSize: size.body.p10,
      fontFamily: type.helveticaNeueLight
    },
    number: {
      fontSize: 26,
      fontFamily: type.helveticaNeueMedium
    },
    quotes: {
      fontSize: 120,
      fontFamily: type.helveticaNeueMedium
    }
  },
  button: {
    p1: {
      fontSize: 14,
      fontFamily: type.helveticaNeueBold
    },
    p2: {
      fontSize: 14,
      fontFamily: type.helveticaNeueMedium
    }
  },
  h1: {
    fontFamily: type.base,
    fontSize: size.h1
  },
  h2: {
    fontWeight: 'bold',
    fontSize: size.h2
  },
  h3: {
    fontFamily: type.emphasis,
    fontSize: size.h3
  },
  h4: {
    fontFamily: type.base,
    fontSize: size.h4
  },
  h5: {
    fontFamily: type.base,
    fontSize: size.h5
  },
  h6: {
    fontFamily: type.emphasis,
    fontSize: size.h6
  },
  normal: {
    fontFamily: type.base,
    fontSize: size.regular
  },
  description: {
    fontFamily: type.base,
    fontSize: size.medium
  }
}

export default {
  type,
  size,
  style
}
