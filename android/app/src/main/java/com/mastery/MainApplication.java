package com.mastery;

import android.app.Application;
import java.util.Arrays;
import java.util.List;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;
import com.facebook.react.ReactApplication;
import io.invertase.firebase.RNFirebasePackage;
import com.kevinresol.react_native_sound_recorder.RNSoundRecorderPackage;
import com.inprogress.reactnativeyoutube.ReactNativeYouTube;
import com.zmxv.RNSound.RNSoundPackage;
import com.RNFetchBlob.RNFetchBlobPackage;
import com.johnsonsu.rnsoundplayer.RNSoundPlayerPackage;
import org.wonday.pdf.RCTPdfView;
import com.brentvatne.react.ReactVideoPackage;
import com.oblador.vectoricons.VectorIconsPackage;
import com.swmansion.reanimated.ReanimatedPackage;
import com.imagepicker.ImagePickerPackage;
import com.AlexanderZaytsev.RNI18n.RNI18nPackage;
import com.swmansion.gesturehandler.react.RNGestureHandlerPackage;
import com.rnfs.RNFSPackage;
import com.filepicker.FilePickerPackage;
import io.github.elyx0.reactnativedocumentpicker.DocumentPickerPackage;
import com.learnium.RNDeviceInfo.RNDeviceInfo;
import com.lugg.ReactNativeConfig.ReactNativeConfigPackage;
import org.reactnative.camera.RNCameraPackage;
import io.invertase.firebase.notifications.RNFirebaseNotificationsPackage; // <-- Add this line
import io.invertase.firebase.messaging.RNFirebaseMessagingPackage; // <-- Add this line
import com.reactnativecommunity.asyncstorage.AsyncStoragePackage;

//import com.bugfender.react.RNBugfenderPackage;



public class MainApplication extends Application implements ReactApplication {

  private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {
    @Override
    public boolean getUseDeveloperSupport() {
      return BuildConfig.DEBUG;
    }

    @Override
    protected List<ReactPackage> getPackages() {
      return Arrays.<ReactPackage>asList(
          new MainReactPackage(),
            new RNFirebasePackage(),
            new RNSoundRecorderPackage(),
            new ReactNativeYouTube(),
            new RNSoundPackage(),
            new RNFetchBlobPackage(),
            new RNSoundPlayerPackage(),
            new RCTPdfView(),
            new RNFirebaseNotificationsPackage(), // <-- Add this line
            new RNFirebaseMessagingPackage(),
            new ReactVideoPackage(),
            new VectorIconsPackage(),
            new ReanimatedPackage(),
            new ImagePickerPackage(),
            new RNI18nPackage(),
            new RNGestureHandlerPackage(),
            new RNFSPackage(),
            new FilePickerPackage(),
            new DocumentPickerPackage(),
            new RNDeviceInfo(),
            new ReactNativeConfigPackage(),
            new RNCameraPackage(),
            new AsyncStoragePackage()
           
           
            //new RNBugfenderPackage()
      );
    }

    @Override
    protected String getJSMainModuleName() {
      return "index";
    }
  };

  @Override
  public ReactNativeHost getReactNativeHost() {
    return mReactNativeHost;
  }

  @Override
  public void onCreate() {
    super.onCreate();
    SoLoader.init(this, /* native exopackage */ false);
  }
}
